@extends('layouts.app')

@section('content')
<style>
    .check__out_content {
        padding-top: 70px;
        padding-bottom: 70px;

    }
    h3.panel-title {
    text-align: center;
    font-weight: 700;
    color: #6464dd;
}
</style>
<script src="https://js.stripe.com/v2/"></script>

<div class="panel">
    <div class="container check__out_content">
        <div class="row">
            <div class="col-lg-3 col-md-2"></div>
            <div class="col-lg-6 col-md-8 col-12">
                <div class="panel-heading">
                    <h3 class="panel-title">Checkout</h3>
                </div>

                <div class="panel-body">
                    <div class="payment-status"></div>
                    <form action="{{route('payNow')}}" method="POST" id="paymentFrm">
                        @csrf
                        <input type="hidden" name="total_amount" value="{{ $total_amount }}">
                        <input type="hidden" name="order_id" value="{{ $order_id }}">
                        <p><strong>Amount to Pay</strong> : ${{ $total_amount }}</p>
                        <p><strong>Order ID</strong> : {{ $order_id }}</p>
                        <p><strong>Paying to</strong> - GrabThat Inc.</p>

                        <div class="form-group">
                            <label>NAME</label>
                            <input type="text" name="name" id="name" placeholder="Enter name"  autofocus>
                        </div>

                        <div class="form-group">
                            <label>EMAIL</label>
                            <input type="email" name="email" id="email" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label>CARD NUMBER</label>
                            <input type="text" name="card_number" id="card_number" placeholder autocomplete="off">
                        </div>

                        <div class="row">
                            <div class="left">
                                <div class="form-group">
                                    <label>EXPIRY DATE</label>
                                    <div class="col-1">
                                        <input type="text" name="card_exp_month" id="card_exp_month" placeholder="MM">
                                    </div>
                                    <div class="col-2">
                                        <input type="text" name="card_exp_year" id="card_exp_year" placeholder="YYYY">
                                    </div>
                                </div>
                            </div>

                            <div class="right">
                                <div class="form-group">
                                    <label>CVC CODE</label>
                                    <input type="text" name="card_cvc" id="card_cvc" placeholder="CVC" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success" id="payBtn">Submit Payment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
 
<script>
    Stripe.setPublishableKey("{{ env('STRIPE_PUBLISHABLE_KEY') }}");
</script>
@endsection