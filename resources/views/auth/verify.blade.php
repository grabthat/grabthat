@extends('layouts.guest')

@section('content')
<div class="lock-screen" style="background: url('/images/bg-img.jpg');">
    <div class="login-form credentials-form">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="lock-form-head">
                <h4>Verify Your Email Address</h4>
                <p></p>
            </div>

            <div class="login-form-body">
                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif

                <p>Before proceeding, please check your email for a verification link.</p>
                <p>If you did not receive the email, <a href="{{ route('verification.resend') }}">click here to request another</a></p>
            </div>
        </form>
    </div>
</div>
@endsection
