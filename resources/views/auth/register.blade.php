@extends('layouts.guest')

@section('content')
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="register-bg">
                <div class="login-overlay"></div>
                <div class="login-left">
                    <img src="/images/logo.png" alt="Logo">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus elit.</p>
                    <a href="javascript:void(0);" class="btn btn-primary">Learn More</a>
                </div>
            </div>

            <div class="login-form">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="login-form-body">
                        @error('first_name')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        @error('last_name')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        @error('username')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        @error('email')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        @error('password')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        <div class="form-gp">
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" placeholder="First Name" required>
                            <i class="fa fa-user"></i>
                        </div>

                        <div class="form-gp">
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name" required>
                            <i class="fa fa-user"></i>
                        </div>

                        <div class="form-gp">
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Username" required>
                            <i class="fa fa-user"></i>
                        </div>

                        <div class="form-gp">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email Address" required>
                            <i class="fa fa-user"></i>
                        </div>

                        <div class="form-gp">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required>
                            <i class="fa fa-lock"></i>
                        </div>

                        <div class="form-gp">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                            <i class="fa fa-lock"></i>
                        </div>

                        <div class="submit-btn-area">
                            <button class="btn btn-primary" type="submit">Submit</button>

                            <div class="login-other row mt-4">
                                <div class="col-6">
                                    <a class="fb-login" href="/login/facebook">
                                        <span class="login_txt">Sign up with</span> <i class="fa fa-facebook"></i>
                                    </a>
                                </div>

                                <div class="col-6">
                                    <a class="google-login" href="/login/google">
                                        <span class="login_txt">Sign up with</span> <i class="fa fa-google"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-footer text-center mt-5">
                            <p class="text-muted">Don't have an account? <a href="{{ route('login') }}">Sign in</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
