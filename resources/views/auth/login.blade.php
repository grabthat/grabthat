@extends('layouts.guest')

@section('content')
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="login-bg">
                <div class="login-overlay"></div>
                <div class="login-left">
                    <img src="/images/logo.png" alt="Logo">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus elit.</p>
                    <a href="javascript:void(0);" class="btn btn-primary">Learn More</a>
                </div>
            </div>

            <div class="login-form">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="login-form-body">
                        @error('email')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        @error('password')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('warning'))
                            <div class="alert alert-warning">
                                {{ session('warning') }}
                            </div>
                        @endif

                        <div class="form-gp">
                            <input type="text" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email address" required >
                            <i class="fa fa-user"></i>
                        </div>

                        <div class="form-gp">
                            <input type="password" class="@error('password') is-invalid @enderror" name="password" placeholder="Password" required>
                            <i class="fa fa-lock"></i>
                        </div>

                        <div class="row mb-4 rmber-area">
                            <div class="col-6">
                                <div class="custom-control custom-checkbox primary-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label">Remember Me</label>
                                </div>
                            </div>

                            <div class="col-6 text-right">
                                <a href="{{ route('password.request') }}" class="text-primary">Forgot Password?</a>
                            </div>
                        </div>

                        <div class="submit-btn-area">
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <div class="login-other row mt-4">
                                <div class="col-6">
                                    <a class="fb-login" href="/login/facebook">
                                        <span class="login_txt">Log in with</span> <i class="fa fa-facebook"></i>
                                    </a>
                                </div>

                                <div class="col-6">
                                    <a class="google-login" href="/login/google">
                                        <span class="login_txt">Log in with</span> <i class="fa fa-google"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-footer text-center mt-5">
                            <p class="text-muted">Don't have an account? <a href="{{ route('register') }}" class="text-primary">Sign up</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
