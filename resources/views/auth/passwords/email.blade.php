@extends('layouts.guest')

@section('content')
<div class="lock-screen" style="background: url('/images/bg-img.jpg');">
    <div class="login-form credentials-form">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="lock-form-head">
                <h4>Forgot Password</h4>
                <p>Please Enter your Email Below</p>
            </div>

            <div class="login-form-body">
                @error('email')
                <p class="text-danger">{{ $message }}</p>
                @enderror

                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif

                <div class="form-gp">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email address" required>
                    <i class="fa fa-lock"></i>
                </div>

                <div class="submit-btn-area">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
