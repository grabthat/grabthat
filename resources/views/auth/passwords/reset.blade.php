@extends('layouts.guest')

@section('content')
<div class="lock-screen" style="background: url('/images/bg-img.jpg');">
    <div class="login-form credentials-form">
        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="lock-form-head">
                <h4>Reset Password</h4>
                <p>Please Reset Password</p>
            </div>

            <div class="login-form-body">
                @error('email')
                <p class="text-danger">{{ $message }}</p>
                @enderror

                @error('password')
                <p class="text-danger">{{ $message }}</p>
                @enderror

                <div class="form-gp">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" placeholder="Email address" required>
                    <i class="fa fa-user"></i>
                </div>

                <div class="form-gp">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required>
                    <i class="fa fa-lock"></i>
                </div>

                <div class="form-gp">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                    <i class="fa fa-lock"></i>
                </div>

                <div class="submit-btn-area">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
