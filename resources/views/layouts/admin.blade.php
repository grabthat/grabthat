@include('admin.header')

<div class="page_content">
    @include('admin.menu')

    <div class="main-content">
        <div class="main-content-inner">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>

    <footer>
        <div class="footer-area">
            <p>&copy; Copyright 2019. All right reserved.</p>
        </div>
    </footer>
</div>

@include('admin.footer')
