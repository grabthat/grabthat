<!doctype html>
<html lang="en">
<head>
<title>Grab That</title>
<link rel="stylesheet" type="text/css" href="/css/reset.css">
<link rel="stylesheet" type="text/css" href="/css/styles.css">
<link href="{{url('css/font-awesome.min.css')}}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet"><link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap">
@yield('styless')
</head>
<body>
@include('front.header')

@yield('content')

@include('front.footer')

@yield('script')
</body>
</html>
