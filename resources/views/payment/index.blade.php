@extends('layouts.app')

@section('content')
<div class="container">
    <div class="status">
        <?php if(!empty($order_id)) { ?>
        <h1 class="<?php echo $ordStatus; ?>"><?php echo $statusMsg; ?></h1>

        <h4>Payment Information</h4>
        <p><b>Reference Number:</b> <?php echo $order_id; ?></p>
        <p><b>Transaction ID:</b> <?php echo $transactionID; ?></p>
        <p><b>Paid Amount:</b> <?php echo $paidAmount . ' ' . $paidCurrency; ?></p>
        <p><b>Payment Status:</b> <?php echo $payment_status; ?></p>

        <h4>Product Information</h4>
        <p><b>Name:</b> <?php echo $itemName; ?></p>
        <p><b>Price:</b> <?php echo $itemPrice . ' ' . $currency; ?></p>
        <?php }else{ ?>
        <h1 class="error">Your Payment has Failed</h1>
        <?php } ?>
    </div>
</div>
@endsection
