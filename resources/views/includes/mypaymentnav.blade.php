<div class="payment-left">
    <div class="balance-box">
        Balance <span>$450.85</span>
    </div>
    <ul>
        <li><a href="{{url('transaction/summary')}}" class="active">Transactions Summary</a></li>
        <li><a href="{{url('withdraw/funds')}}">Withdraw Funds</a></li>
        <li><a href="{{url('bank-details')}}">Bank Details</a></li>
    </ul>
</div>