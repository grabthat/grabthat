@extends('layouts.app')

@section('content')
@if(!Auth::check())
@include('front.visit-index')
@else 
@include('front.home')
@endif

<!--<section class="home-bottom">
    <div class="container">
        <ul>
            <li>
                <img src="assets/images/paperworks-img.png" alt="">
                <h3>No paperworks</h3>
                <p>All documents and forms have to be uploaded online once.</p>
            </li>
            <li>
                <img src="assets/images/free-img.png" alt="">
                <h3>Free!</h3>
                <p>It's free of charge for tenants. Premium users are charged for credit checks.</p>
            </li>
            <li>
                <img src="assets/images/real-deals-img.png" alt="">
                <h3>Verified &amp; Real Deals</h3>
                <p>Our apartments and landlords are verified. We’re proud of being scam-free!</p>
            </li>
        </ul>
    </div>
</section>-->
@endsection
