<footer class="footer-sec">
    
    <div class="footer-bottom">
        <div class="container flexbox">
            <div class="fot-copy">
                &copy; 2019. All rights reserved, Grabthat Inc.
                <ul>
                    <li><a href="#">Term &amp; Condtions</a> &nbsp;|&nbsp;&nbsp;</li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
            <ul class="footer-social">
                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            </ul>
        </div>
    </div>
</footer>

<script src="/script/jquery.min.js"></script>
<script src="/script/jquery.stylish-select.min.js"></script>
<script src="/script/date-time-picker.min.js"></script>
<script src="/script/custom.js"></script>

<script type="text/javascript">
$('.my-dropdown').sSelect();

$('#filterForm input').change(function () {
    $('#filterForm').submit();
});

$('select[name=sort]').change( function(){
    var _val = $(this).val();
    $('input[name=sort]').val(_val);
    $('#filterForm').submit();
});

$('input.date1').dateTimePicker();
$('input.date2').dateTimePicker();




function stripeResponseHandler(status, response) {
    if (response.error) {
        $('#payBtn').removeAttr("disabled");
        $(".payment-status").html('<p>' + response.error.message + '</p>');
    } else {
        var form$ = $("#paymentFrm");
        var token = response.id;
        var item_data = JSON.parse(localStorage.getItem("bookinginfo"));
        for(value in item_data){
            console.log(item_data[value]);
            form$.append("<input type='hidden' name='"+value+"' value='" + item_data[value] + "' />");
        }
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        form$.get(0).submit();
    }
}

$("#paymentFrm").submit(function () {
    $('#payBtn').attr("disabled", "disabled");
    
    // var item_data = JSON.parse(localStorage.getItem("bookinginfo"));
    // // console.log(item_data);
    // form$ = $("#paymentFrm");
    //     for(value in item_data){
    //         console.log(item_data[value]);
    //         form$.append("<input type='hidden' name='"+value+"' value='" + item_data[value] + "' />");
    //     }
        // return false;
    // form$.append(item_data);
    // form$.get(0).submit();
    Stripe.createToken({
        number: $('#card_number').val(),
        exp_month: $('#card_exp_month').val(),
        exp_year: $('#card_exp_year').val(),
        cvc: $('#card_cvc').val()
    }, stripeResponseHandler);
    return false;
    
});
</script>

