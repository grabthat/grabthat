 
<section class="home-content home-afterlogin-content">
    <div class="container">
        <h3>What can we help you Grab today, {{ Auth::user()->display_name }}?</h3>

        <div class="explore-categories">
            <ul>
                <?php foreach ($categories as $category) { ?>
                    <li>
                        <div class="explore-categories-img">
                            <a href="/category/<?php echo $category->slug; ?>">
                                <img src="<?php echo $category->image; ?>" alt="">
                            </a>
                        </div>

                        <h4><?php echo $category->name; ?></h4>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <h3>Today in <?php echo $city; ?></h3>

        <?php if (empty($today)) echo 'We have not found any rental for your location'; ?>

        <div class="featured-rentals-sec">
            <ul>
                <?php foreach ($today as $rental) { ?>
                    <li>
                        <div class="featured-img">
                            <a href="/rental/<?php echo $rental->slug; ?>">
                                <img src="<?php echo $rental->image; ?>" alt="">
                            </a>
                        </div>

                        <div class="featured-content">
                            <h4><?php echo $rental->city; ?></h4>

                            <div class="stars flexbox">
                                
                                <div class="star-rating">
                               
                                    @if(isset($rental->rentReview))
                                     
                                   <?php  $rating =  count(@$rental->rentReview); ?>
                                    @foreach(range(1,5) as $i)
                                    @if($rating >0)
                                    @if($rating >0.5)
                                    <i class="fa fa-star star-color"></i>
                                    @else
                                    <i class="fa fa-star-half-o star-color"></i>
                                    @endif
                                    @else
                                    <i class="fa  fa-star-o star-color"></i>
                                    @endif
                                    <?php $rating--; ?>
                                    @endforeach
                                    @else
                                    @endif
                                    <!-- Rating in numbers -->
                                    @if(@$rental->rating[0] != '' )
                                    @php echo $rental->rating[0].'/5.0'; @endphp
                                    @else
                                    @php echo '0/5.0'; @endphp
                                    @endif
                                    <!-- Reviews -->
                                    @if(@$rental->review != '')
                                    @php echo '('.$rental->review.' reviews )'; @endphp
                                    @else

                                    @endif
                                </div>
                            </div>

                            <h3><?php echo $rental->name; ?></h3>
                            <p>From : <?php echo $rental->price; ?></p>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <h3>Popular in <?php echo $city; ?></h3>

        <?php if (empty($popular)) echo 'We have not found any rental for your location'; ?>

        <div class="featured-rentals-sec">
            <ul>
                <?php foreach ($popular as $rental) { ?>
                    <li>
                        <div class="featured-img">
                            <a href="/rental/<?php echo $rental->slug; ?>">
                                <img src="<?php echo $rental->image; ?>" alt="">
                            </a>
                        </div>

                        <div class="featured-content">
                            <h4><?php echo $rental->city; ?></h4>

                            <div class="stars flexbox">
                                <!-- <img src="/images/star.png" alt=""> -->
                                <div class="star-rating">
                                @if(isset($rental->rentReview))
                                      
                                     <?php  $rating = count(@$rental->rentReview); ?>
                                      @foreach(range(1,5) as $i)
                                      @if($rating >0)
                                      @if($rating >0.5)
                                      <i class="fa fa-star star-color"></i>
                                      @else
                                      <i class="fa fa-star-half-o star-color"></i>
                                      @endif
                                      @else
                                      <i class="fa  fa-star-o star-color"></i>
                                      @endif
                                      <?php $rating--; ?>
                                      @endforeach
                                      @else
                                      @endif
                                      <!-- Rating in numbers -->
                                      @if(@$rental->rating[0] != '' )
                                      @php echo $rental->rating[0].'/5.0'; @endphp
                                      @else
                                      @php echo '0/5.0'; @endphp
                                      @endif
                                      <!-- Reviews -->
                                      @if(@$rental->review != '')
                                      @php echo '('.$rental->review.' reviews )'; @endphp
                                      @else
  
                                      @endif
                                </div>
                            </div>

                            <h3><?php echo $rental->name; ?></h3>
                            <p>From : <?php echo $rental->price; ?></p>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <h3>Tomorrow in <?php echo $city; ?></h3>

        <?php if (empty($tomorrow)) echo 'We have not found any rental for your location'; ?>

        <div class="featured-rentals-sec">
            <ul>
                <?php foreach ($tomorrow as $rental) { ?>
                    <li>
                        <div class="featured-img">
                            <a href="/rental/<?php echo $rental->slug; ?>">
                                <img src="<?php echo $rental->image; ?>" alt="">
                            </a>
                        </div>

                        <div class="featured-content">
                            <h4><?php echo $rental->city; ?></h4>

                            <div class="stars flexbox">
                                <!-- <img src="/images/star.png" alt=""> -->
                                <div class="star-rating">
                                @if(isset($rental->rentReview))
                                     
                                     <?php  $rating =  count(@$rental->rentReview); ?>
                                      @foreach(range(1,5) as $i)
                                      @if($rating >0)
                                      @if($rating >0.5)
                                      <i class="fa fa-star star-color"></i>
                                      @else
                                      <i class="fa fa-star-half-o star-color"></i>
                                      @endif
                                      @else
                                      <i class="fa  fa-star-o star-color"></i>
                                      @endif
                                      <?php $rating--; ?>
                                      @endforeach
                                      @else
                                      @endif
                                      <!-- Rating in numbers -->
                                      @if(@$rental->rating[0] != '' )
                                      @php echo $rental->rating[0].'/5.0'; @endphp
                                      @else
                                      @php echo '0/5.0'; @endphp
                                      @endif
                                      <!-- Reviews -->
                                      @if(@$rental->review != '')
                                      @php echo '('.$rental->review.' reviews )'; @endphp
                                      @else
  
                                      @endif
                                </div>
                            </div>

                            <h3><?php echo $rental->name; ?></h3>
                            <p>From : <?php echo $rental->price; ?></p>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>

    </div>
</section>
 