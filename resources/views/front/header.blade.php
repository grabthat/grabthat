<header class="header-sec <?php echo( Request::is('/') && (!Auth::check())) ? "" : "inner-page-header"; ?>">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
    <div class="top-head">
        <div class="container flexbox">
            <div class="logo">
                <a href="/"><img src="/images/logo.png" alt="Grab That"></a>
            </div>

            <div class="modal fade" id="locationModal" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="locationModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="locationModal"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <!-- <label for="recipient-name" class="col-form-label"></label> -->
                                    <input type="text" class="form-control"  name="location_input" id="location_input" placeholder="Where to?">
                                </div>
                                <div id="infowindow-content">
                                        <img src="" width="16" height="16" id="place-icon">
                                        <span id="place-name"  class="title"></span><br>
                                          <span id="place-address"></span>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <!-- <label for="recipient-name" class="col-form-label"></label> -->
                                    <span class="btn btn-success"   name="get_current_location" id="get_current_location"><i class="fa fa-paper-plane" aria-hidden="true"></i> Nearby</span>
                                </div>
                             
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <?php if (Request::is('/') && (!Auth::check())) { ?>
                <div class="head-location">You are in 
                    @if(Session::has('loc'))
                    {!! ucfirst(Session::get('loc')) !!} 
                    @else
                    {!! $city !!}
                    @endif - <a href="#" data-toggle="modal" data-target="#locationModal" data-whatever="@mdo">Change Location?</a></div>
            <?php } else { ?>
                <div class="home-top-search">
                    <form action="/search">
                        <input type="hidden" name="search" value="1">
                        <input type="search" name="key" placeholder="Search anything">
                    </form>
                </div>
            <?php } ?>

            <div class="head-links flexbox">
                @if (Auth::check())
                <a href="{{route('frontAddRental')}}" class="login-btn">Share Your Grab</a>

                <div class="login-profile">
                    <img src="<?php echo GetUserImage(Auth::user()->image); ?>" alt="">
                    <h3>{{ Auth::user()->display_name }}</h3>
                </div>

                <div class="dropdown-tooltip">
                    <ul>
                        <li><a href="{{route('FrontUserProfile')}}">Profile</a></li>
                        <li><a href="http://psdtohtmlconvert.com/kunal/grabthat/account-settings.html">Account Settings</a></li>
                        <li><a href="{{ route('mymessage')}}">Message Inbox <span class="new">(2)</span></a></li>
                        <li><a href="{{ route('FrontUserRentals')}}">My Rentals</a></li>
                        <li><a href="{{ route('UserMypayment')}}">My Payments</a></li>
                        <li><a href="{{ route('logout') }}">Log out</a></li>
                    </ul>
                </div>
                @else
                <a href="{{ route('frontAddRental') }}">Share Your Grab</a>
                <a href="{{route('login')}}" class="login-btn">Login / Sign Up</a>
                @endif
            </div>
        </div>
        <div class="transparent-box"></div>
    </div>
        @if(Request::is('/')  && (!Auth::check()))
        <div class="container">
            <div class="top-form">
                <h3>Rent anything. anytime. anywhere.</h3>

                <form action="/search">
                    <input type="hidden" name="search" value="1">

                    <div class="form-row">
                        <label>Where?</label>
                        <input type="text" name="location[]" class="txt-style" placeholder="Anywhere">
                    </div>

                    <div class="form-row">
                        <label>What Item?</label>
                        <input type="text" name="key" class="txt-style" placeholder="Any Item">
                    </div>

                    <div class="form-row flexbox">
                        <div class="form-col">
                            <label>Grab Item</label>
                            <input type="text" name="grab" id="grab" class="txt-style" placeholder="DD/MM/YYYY">
                        </div>

                        <div class="form-col">
                            <label>Return Item</label>
                            <input type="text" name="return" id="return" class="txt-style" placeholder="DD/MM/YYYY">
                        </div>
                    </div>

                    <div class="form-btn">
                        <input type="submit" value="Search" class="submit-btn">
                    </div>
                </form>
            </div>
        </div>
     @endif
</header>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/script/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBnC3pZjxxNQg5mpxV9x_FJOUdTLb6aAJM&libraries=places"></script>
<script>
    $('#grab').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true //, startDate: '0d'
    });

    $('#return').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true //, startDate: '0d'
    });
</script>

    <script type="text/javascript">
      var input = document.getElementById('location_input');
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
    </script>

<script>
var x = document.getElementById("get_current_location");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
function showPosition(position) {
    let lat = '';
    let lon = '';
    this.lat = position.coords.latitude;
    this.lon = position.coords.longitude;

}
</script>
<script>
    $('#location_input').on('change',function(event){
    let url = 'ajax/set/nearby';
    let val = $('#location_input').val();
    $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: {
        "_token": "{{ csrf_token() }}",
        "val" : val,
        "id"  : 1
    },
    success: function(response)
    { if(response.data == 1)
        {
            $('.close').click();
            window.location.href="/";
        }else
        {
            $('.close').click();
            // window.location.href="/";
        }     
    }

 })
// }
});

</script>
<script>
    $('#get_current_location').on('click',function(event){
    let url = 'ajax/set/nearby';
    let val = $('#location_input').val();
    $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: {
        "_token": "{{ csrf_token() }}",
        "val" : val,
        "id"  : 2
    },
    success: function(response)
    { if(response.data == 1)
        {
            $('.close').click();
            window.location.href="/";
        }else
        {
            $('.close').click();
            // window.location.href="/";
        }     
    }

 })
// }
});

</script>