@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <a href="{{route('allCategoryList')}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i></a>
        </div>

        <h4 class="card_title">Manage Category</h4>

        <form action="{{route('StoreCategory')}}" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" value="<?php echo @$category->id; ?>">

            <div class="form-group">
                <label>Category Name</label>
                <input type="text" name="name" value="<?php echo @$category->name; ?>" class="form-control" required>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Parent Category</label>
                    <select name="parent" class="form-control" required>
                        <option value="0">None</option>
                        @if($categories)
                        @foreach($categories as $row)
                        
                    
                            <option value="<?php echo $row['id']; ?>" <?php echo (@$category->parent == $row['id']) ? "selected" : ""; ?>>
                                <?php echo $row['name'] ?>
                            </option>
                        @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label>Display on Homepage</label>
                    <?php echo YesNo(@$category->on_homepage, 'on_homepage'); ?>
                </div>
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control editor" rows="3"><?php echo @$category->description; ?></textarea>
            </div>

            <div class="form-group">
                <label>Image</label>
                <input type="file" name="image[]" class="form-control" accept="image/*">
            </div>

            <?php if(@$category->image != '') { ?>
            <div class="form-group">
                <img src="<?php echo $category->image; ?>" alt="">
            </div>
            <?php } ?>

            <div class="form-group">
                <label>Meta Keyword</label>
                <input type="text" name="meta_keyword" value="<?php echo @$category->meta_keyword; ?>" class="form-control">
            </div>

            <div class="form-group">
                <label>Meta Description</label>
                <input type="text" name="meta_description" value="<?php echo @$category->meta_description; ?>" class="form-control">
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Status</label>
                    <?php echo StatusControl(@$category->status); ?>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
