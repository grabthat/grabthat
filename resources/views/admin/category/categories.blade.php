@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <form action="{{route('CategorySearch')}}" method="get" class="form-inline">
                <input type="hidden" name="action" value="search">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <input type="text" name="name" class="form-control form-control-sm" value="<?php echo @$_REQUEST['name']; ?>" placeholder="Enter category name ">
                    </div>
                    <div class="col-auto">
                        <select name="status" class="form-control form-control-sm">
                            <option value="">-- Status --</option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('addCategory')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </form>
        </div>

        <h4 class="card_title">View Categories</h4>

        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Display on Homepage</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $k => $category)
                        <tr>
                            <th scope="row"><?php echo $k + 1; ?></th>
                            <td><?php echo $category['name']; ?></td>
                            <td><?php echo GetYesNo($category['on_homepage']); ?></td>
                            <td><?php echo GetStatus($category['status']); ?></td>
                            <td>
                                <ul class="d-flex justify-content-center">
                                    <li class="mr-3">
                                        
                                        <a href="{{ route('editCategory', $category['id'])}}" class="btn btn-inverse-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </li>
                                    <li>
                                        
                                        <a href="{{route('removeCategory', $category['id'])}}" class="btn btn-inverse-danger deleted">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
