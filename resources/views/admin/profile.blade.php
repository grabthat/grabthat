@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card_title">My Profile</h4>

        <form action="" method="post" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>First Name</label>
                            <input type="text" name="first_name" value="<?php echo @$user->first_name; ?>" class="form-control" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Last Name</label>
                            <input type="text" name="last_name" value="<?php echo @$user->last_name; ?>" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" name="email" value="<?php echo @$user->email; ?>" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" name="phone" value="<?php echo @$user->phone; ?>" class="form-control">
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Date of Birth</label>
                            <input type="text" name="dob" value="<?php echo @$user->dob; ?>" class="form-control date">
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Gender</label>
                            <?php echo Gender(@$user->gender); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Designation</label>
                        <input type="text" name="designation" value="<?php echo @$user->designation; ?>" class="form-control">
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Country</label>
                            <input type="text" name="country" value="<?php echo @$user->country; ?>" class="form-control" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>State</label>
                            <input type="text" name="state" value="<?php echo @$user->state; ?>" class="form-control" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>City</label>
                            <input type="text" name="city" value="<?php echo @$user->city; ?>" class="form-control" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Zip</label>
                            <input type="text" name="zip" value="<?php echo @$user->zip; ?>" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Profile Image</label>
                        <input type="file" name="image[]" class="form-control" accept="image/*">
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" value="" class="form-control" <?php echo isset($user->id) ? "" : "required"; ?>>
                        <small class="text-muted">Leave blank if no change</small>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
