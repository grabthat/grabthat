<nav class="rt_nav_header horizontal-layout col-lg-12 col-12 p-0">
    <div class="top_nav flex-grow-1">
        <div class="container d-flex flex-row h-100 align-items-center">
            <div class="text-center rt_nav_wrapper d-flex align-items-center">
                <a class="nav_logo" href="{{route('home')}}"><img src="/images/logo.png" alt="logo"/></a>
            </div>

            <div class="nav_wrapper_main d-flex align-items-center justify-content-between flex-grow-1">
                <ul class="navbar-nav navbar-nav-right mr-0 ml-auto">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <span class="profile_name"> {{ Auth::user()->first_name }}
                                <i class="fa fa-chevron-down"></i>
                            </span>
                            <img src="/images/user.jpg" alt="profile">
                        </a>

                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown pt-2" aria-labelledby="profileDropdown">
                            <a class="dropdown-item" href="{{route('adminProfile')}}">
                                <i class="fa fa-user text-dark mr-3"></i> My Profile
                            </a>
                            <span role="separator" class="divider"></span>
                            <a class="dropdown-item" href="/logout">
                                <i class="fa fa-sign-out text-dark mr-3"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>

                <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="fa fa-bars text-white"></span>
                </button>
            </div>
        </div>
    </div>

    <div class="nav-bottom">
        <div class="container">
            <ul class="nav page-navigation">
                <li class="nav-item">
                    <a href="{{route('admin')}}" class="nav-link">
                        <span class="menu-title">Home</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <span class="menu-title">Users</span>
                    </a>
                    <div class="submenu">
                        <ul class="submenu-item">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('alluserlist')}}"><span>View All Users</span></a>
                                <a class="nav-link" href="{{ route('addNewUser')}}"><span>Add User</span></a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <span class="menu-title">Categories</span>
                    </a>
                    <div class="submenu">
                        <ul class="submenu-item">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('allCategoryList')}}"><span>View All Categories</span></a>
                                <a class="nav-link" href="{{ route('addCategory') }}"><span>Add Category</span></a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <span class="menu-title">Rentals</span>
                    </a>
                    <div class="submenu">
                        <ul class="submenu-item">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('allRentalList') }}"><span>View All Rentals</span></a>
                                <a class="nav-link" href="{{ route('adminAddRental')}}"><span>Add Rental</span></a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <span class="menu-title">Settings</span>
                    </a>
                    <div class="submenu">
                        <ul class="submenu-item">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('addSetting')}}"><span>Add Settings</span></a>
                                
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <span class="menu-title">Menu 2</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <span class="menu-title">Menu 3</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
