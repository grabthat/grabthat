@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <form action="{{route('searchUser')}}" method="get" class="form-inline">
                <input type="hidden" name="action" value="search">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <input type="text" name="name" class="form-control form-control-sm" value="<?php echo @$_REQUEST['name']; ?>" placeholder="Enter name or email address">
                    </div>
                    <div class="col-auto">
                        <select name="status" class="form-control form-control-sm">
                            <option value="">-- Status --</option>
                            <option value="1">Verified</option>
                            <option value="2">Not-Verified</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
                    </div>
                    <div class="col-auto">
                        <a href="{{route('addNewUser')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </form>
        </div>

        <h4 class="card_title">View Users</h4>

        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $k => $user)
                        <tr>
                            <th scope="row"><?php echo $k + 1; ?></th>
                            <td><?php echo $user->display_name; ?></td>
                            <td><?php echo $user->email; ?></td>
                            <td><?php echo $user->verified ? 'Verified' : 'Not-Verified'; ?></td>
                            <td>
                                <ul class="d-flex justify-content-center">
                                    <li class="mr-3">
                                        <a href="{{route('UserProfie', $user->id)}}" class="btn btn-inverse-primary">
                                            <i class="fa fa-info"></i>
                                        </a>
                                    </li>
                                    <li class="mr-3">
                                        
                                        <a href="{{route('editUser', $user->id)}}" class="btn btn-inverse-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </li>
                                    <li>
                                    
                                        <a href="{{route('RemoveUser',  $user->id)}}" class="btn btn-inverse-danger deleted">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
