@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <a href="{{route('alluserlist')}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i></a>
        </div>

        <h4 class="card_title">Manage User</h4>

        <form action="" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" value="<?php echo @$user->id; ?>">
            
            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Username</label>
                    <input type="text" name="username" value="<?php echo @$user->username; ?>" class="form-control" data-check="0" required>
                    <small class="error text-danger d-none">Username is already in use</small>
                </div>

                <div class="form-group col-sm-6">
                    <label>Email Address</label>
                    <input type="email" name="email" value="<?php echo @$user->email; ?>" class="form-control" data-check="0" required>
                    <small class="error text-danger d-none">Email address is already in use</small>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Password</label>
                    <input type="password" name="password" value="" class="form-control" <?php echo isset($user->id) ? "" : "required"; ?>>
                    <?php echo isset($user->id) ? "<small class=\"text-muted\">Leave blank if no change</small>" : ""; ?>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>First Name</label>
                    <input type="text" name="first_name" value="<?php echo @$user->first_name; ?>" class="form-control" required>
                </div>

                <div class="form-group col-sm-6">
                    <label>Last Name</label>
                    <input type="text" name="last_name" value="<?php echo @$user->last_name; ?>" class="form-control" required>
                </div>

                <div class="form-group col-sm-6">
                    <label>Phone</label>
                    <input type="text" name="phone" value="<?php echo @$user->phone; ?>" class="form-control">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Date of Birth</label>
                    <input type="text" name="dob" value="<?php echo @$user->dob; ?>" class="form-control date">
                </div>

                <div class="form-group col-sm-6">
                    <label>Gender</label>
                    <?php echo Gender(@$user->gender); ?>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Country</label>
                    <input type="text" name="country" value="<?php echo @$user->country; ?>" class="form-control" required>
                </div>

                <div class="form-group col-sm-6">
                    <label>State</label>
                    <input type="text" name="state" value="<?php echo @$user->state; ?>" class="form-control" required>
                </div>

                <div class="form-group col-sm-6">
                    <label>City</label>
                    <input type="text" name="city" value="<?php echo @$user->city; ?>" class="form-control" required>
                </div>

                <div class="form-group col-sm-6">
                    <label>Zip</label>
                    <input type="text" name="zip" value="<?php echo @$user->zip; ?>" class="form-control" required>
                </div>
            </div>

            <div class="form-group">
                <label>Profile Image</label>
                <input type="file" name="image[]" class="form-control" accept="image/*">
            </div>

            <br>

            <h3>Professional Details</h3>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>I am</label>
                    <?php echo CompanyRole(@$user->company_role); ?>
                </div>

                <div class="form-group col-sm-6">
                    <label>Company Name</label>
                    <input type="text" name="company" value="<?php echo @$user->company; ?>" class="form-control">
                </div>

                <div class="form-group col-sm-6">
                    <label>Designation</label>
                    <input type="text" name="designation" value="<?php echo @$user->designation; ?>" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6 text-left">
                <label for="">Verification status</label>
                
                <select name="verified" id="" class="form-control">
                    <option value="1" {{ (@$user->verified == 1) ? 'selected' : ''}}>Verified</option>
                    <option value="0" {{ (@$user->verified == 0) ? 'selected' : ''}}>Unverified</option>
                </select>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
