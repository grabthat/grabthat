@extends('layouts.admin')

<style>
.form-group label {
    font-weight: bold;
}
</style>
@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <a href="{{route('alluserlist')}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i></a>
        </div>

        <h4 class="card_title">View User</h4>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Username</label>: <?php echo $user->username; ?>
                </div>

                <div class="form-group">
                    <label>Email Address</label>: <?php echo $user->email; ?>
                </div>

                <div class="form-group">
                    <label>Name</label>: <?php echo $user->display_name; ?>
                </div>

                <div class="form-group">
                    <label>Phone</label>: <?php echo $user->phone; ?>
                </div>

                <div class="form-group">
                    <label>Date of Birth</label>: <?php echo $user->dob; ?>
                </div>

                <div class="form-group">
                    <label>Gender</label>: <?php echo GetGender($user->gender); ?>
                </div>

                <div class="form-group">
                    <label>Location</label>: <?php echo $user->country; ?>, <?php echo $user->state; ?>, <?php echo $user->city; ?>
                </div>

                <div class="form-group">
                    <label>Zip</label>: <?php echo $user->zip; ?>
                </div>

                <h3>Professional Details</h3>

                <div class="form-group">
                    <label>I am</label>: <?php echo GetCompanyRole($user->company_role); ?>
                </div>

                <div class="form-group">
                    <label>Company Name</label>: <?php echo $user->company; ?>
                </div>

                <div class="form-group">
                    <label>Designation</label>: <?php echo $user->designation; ?>
                </div>
            </div>

            <div class="col-sm-6 text-center">
                <img src="<?php echo $user->image; ?>" alt="">
            </div>
        </div>
    </div>
</div>
@endsection
