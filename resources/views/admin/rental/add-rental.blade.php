@extends('layouts.admin')

@section('content')
<link href="{{asset('/css/timepicker.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{asset('/css/daterangepicker.css')}}" />
<div class="card">
    <div class="card-body">

        @if(isset($errors))
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger col-lg-3 col-md-3 col-12" style="padding: 5px 10px; margin: 5px"> <i class=" fa fa-close" style="cursor: pointer; padding-right:5px"></i>{{ $error }} </div>
            @endforeach
        </div>

        @endif
        <div id="search-bar" class="pull-right text-right">
            <a href="{{route('allRentalList')}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i></a>
        </div>

        <h4 class="card_title">Manage Rental</h4>


        <form action="" method="post" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="id" value="<?php echo @$rental->id; ?>">

            <div class="form-group">
                <label>Title</label>
                <input type="text" name="name" value="<?php echo @$rental->name; ?>" class="form-control" required>
                @if($errors->has('name'))
                <div class="error alert-danger">{{$errors->first('name')}}</div>
                @endif
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label>Category</label>
                    <select name="category" class="form-control" required>
                        <option value="">-- Select --</option>

                        @foreach($categories as $k => $row)
                        <option value="{{ $row['id'] }}" {{ (@$rental->category == $row['id']) ? "selected" : "" }}>
                            {{ $row['name'] }}
                        </option>
                        @endforeach
                    </select>
                    @if($errors->has('category'))
                    <div class="error alert-danger">{{$errors->first('category')}}</div>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label>Duration type</label>
                    <?php echo RentalDuration(@$rental->rental_type); ?>
                    @if($errors->has('rental_type'))
                    <div class="error alert-danger">{{$errors->first('rental_type')}}</div>
                    @endif
                </div>
            </div>

            <div class="price_slab price-hourly <?php echo (@$rental->rental_type == 'hourly') ? "" : "d-none"; ?>">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label>Define Hours</label>
                        <select name="hours" class="form-control" required>
                            <option value="">-- Select hours --</option>
                            <?php for ($h = 1; $h <= 24; $h++) { ?>
                                <option value="<?php echo $h; ?>" {{ (@$rental->duration == $h) ? "selected" : ""}}>
                                    {{ $h }}
                                </option>
                            <?php } ?>
                        </select>
                        @if($errors->has('hours'))
                        <div class="error alert-danger">{{$errors->first('hours')}}</div>
                        @endif
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Price (Hourly)</label>
                        <input type="number" name="price_hourly" class="form-control" value="<?php echo (@$rental->rental_type == 'hourly') ? @$rental->price : ''; ?>" min="0" step="1">
                        @if($errors->has('price_hourly'))
                        <div class="error alert-danger">{{$errors->first('price_hourly')}}</div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="price_slab price-daily <?php echo (@$rental->rental_type == 'daily') ? "" : "d-none"; ?>">
                <p><b>Leave the day field empty in which your rental is unavailable</b></p>

                <div class="row">
                    <div class="form-group col-sm-6">
                        <label>Define days a user can rent for</label>
                        <select name="days" class="form-control" required>
                            <option value="">-- Select number of days --</option>
                            <?php for ($h = 1; $h <= 31; $h++) { ?>
                                <option value="<?php echo $h; ?>" <?php echo (@$rental->duration == $h) ? "selected" : ""; ?>>
                                    <?php echo $h; ?>
                                </option>
                            <?php } ?>
                        </select>
                        @if($errors->has('days'))
                        <div class="error alert-danger">{{$errors->first('days')}}</div>
                        @endif
                    </div>


                    <div class="col-lg-6 col-md-4 col-sm-6">
                        <label>Price Per day</label>
                        <input type="number" name="price_daily" class="form-control" value="<?php echo (@$rental->rental_type == 'daily') ? @$rental->price : ''; ?>" min="0" step="1">
                        @if($errors->has('price_daily'))
                        <div class="error alert-danger">{{$errors->first('price_daily')}}</div>
                        @endif
                    </div>

                </div>
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control editor">{{ html_entity_decode( @$rental->description)}}</textarea>
                @if($errors->has('description'))
                <div class="error alert-danger">{{$errors->first('description')}}</div>
                @endif
            </div>

            <div class="form-group">
                <label>Including</label>
                <textarea name="including" class="form-control editor">{{ html_entity_decode(@$rental->including)}}</textarea>
                @if($errors->has('including'))
                <div class="error alert-danger">{{$errors->first('including')}}</div>
                @endif
            </div>

            <div class="form-group">
                <label>Excluding</label>
                <textarea name="excluding" class="form-control editor">{{ html_entity_decode(@$rental->excluding) }}</textarea>
                @if($errors->has('excluding'))
                <div class="error alert-danger">{{$errors->first('excluding')}}</div>
                @endif
            </div>
            {{--
            <div class="row">
                <div class="form-group col-sm-6">
                    <label>How would this rental booking be confirmed</label>
                    {{RentalConfirmation(@$rental->confirmation)}}
    </div>
</div>
--}}

<div class="row">
    <div class="form-group col-sm-6">
        <label>Manage Availability of This Rental</label>
        <?php echo RentalAvailability(@$rental->availability); ?>
        @if($errors->has('availability'))
        <div class="error alert-danger">{{$errors->first('availability')}}</div>
        @endif
    </div>

    <div id="availability_dates" class="form-group col-sm-6 <?php echo (@$rental->id == '' || @$rental->availability == 1) ? "" : "d-none"; ?>">
        <div class="row">
            <div class="form-group col-sm-6">
                <label>Start Date</label>
                 
                <input type="text" name="available_start_date" value="{{ date('Y-m-d', @$rental->available_start_date) }}" class="form-control" required>
                @if($errors->has('available_start_date'))
                <div class="error alert-danger">{{$errors->first('available_start_date')}}</div>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label>End Date</label>
                <input type="text" name="available_end_date" value="{{ date('Y-m-d', @$rental->available_end_date) }}" class="form-control" required>
                @if($errors->has('available_end_date'))
                <div class="error alert-danger">{{$errors->first('available_end_date')}}</div>
                @endif
            </div>
            <div class="form-group col-sm-6 hour_duration <?php echo (@$rental->rental_type == 'hourly') ? "" : "d-none"; ?>">
                <label>Start Hour</label>
                <input type="text" name="start_hour" value="{{date('h:i a', @$rental->available_start_date)}}" class="form-control text-uppercase">
                @if($errors->has('start_hour'))
                <div class="error alert-danger">{{$errors->first('start_hour')}}</div>
                @endif
            </div>

            <div class="form-group col-sm-6 hour_duration <?php echo (@$rental->rental_type == 'hourly') ? "" : "d-none"; ?>">
                <label>End Hour</label>
                <input type="text" name="end_hour" value="{{date('h:i a ', @$rental->available_end_date)}}" class="form-control text-uppercase">
 

                @if($errors->has('end_hour'))
                <div class="error alert-danger">{{$errors->first('end_hour')}}</div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12">
        <label>Address</label>
        <input type="text" name="address" value="<?php echo @$rental->address; ?>" class="form-control" required>
        @if($errors->has('address'))
        <div class="error alert-danger">{{$errors->first('address')}}</div>
        @endif
    </div>

    <div class="form-group col-sm-4">
        <label>Country</label>
        <input type="text" name="country" value="<?php echo @$rental->country; ?>" class="form-control" required>
        @if($errors->has('country'))
        <div class="error alert-danger">{{$errors->first('country')}}</div>
        @endif
    </div>

    <div class="form-group col-sm-4">
        <label>State</label>
        <input type="text" name="state" value="<?php echo @$rental->state; ?>" class="form-control" required>
        @if($errors->has('state'))
        <div class="error alert-danger">{{$errors->first('state')}}</div>
        @endif
    </div>

    <div class="form-group col-sm-4">
        <label>City</label>
        <input type="text" name="city" value="<?php echo @$rental->city; ?>" class="form-control" required>
        @if($errors->has('city'))
        <div class="error alert-danger">{{$errors->first('city')}}</div>
        @endif
    </div>

    <div class="form-group col-sm-4">
        <label>Zip</label>
        <input type="text" name="zip" value="<?php echo @$rental->zip; ?>" class="form-control" required>
        @if($errors->has('zip'))
        <div class="error alert-danger">{{$errors->first('zip')}}</div>
        @endif
    </div>
</div>

<div class="form-group">
    <label>Tips for this rental</label>
    <textarea name="rental_tips" class="form-control editor">{{ html_entity_decode(@$rental->rental_tips)}}</textarea>
    @if($errors->has('rental_tips'))
    <div class="error alert-danger">{{$errors->first('rental_tips')}}</div>
    @endif
</div>

<div class="form-group">
    <label>Guest Requirements</label>
    <textarea name="guest_requirements" class="form-control editor">{{ html_entity_decode(@$rental->guest_requirements)}}</textarea>
    @if($errors->has('guest_requirements'))
    <div class="error alert-danger">{{$errors->first('guest_requirements')}}</div>
    @endif
</div>

<div class="form-group">
    <label>Cancellation Policy</label>
    <textarea name="cancellation_policy" class="form-control editor">{{ html_entity_decode(@$rental->cancellation_policy)}}</textarea>
    @if($errors->has('cancellation_policy'))
    <div class="error alert-danger">{{$errors->first('cancellation_policy')}}</div>
    @endif
</div>

<div class="form-group">
    <label>Featured Image</label>
    <input type="file" name="image[]" class="form-control" accept="image/*">
    @if($errors->has('image'))
    <div class="error alert-danger">{{$errors->first('image')}}</div>
    @endif
</div>

<?php if (@$rental->image != '') { ?>
    <div class="form-group">
        <img src="<?php echo $rental->image; ?>" alt="">
    </div>
<?php } ?>

<div class="form-group">
    <label>Images</label>
    <input type="file" name="images[]" multiple class="form-control" accept="image/*">
    <small class="form-text text-muted">You can select multiple images</small>
    @if($errors->has('images'))
    <div class="error alert-danger">{{$errors->first('images')}}</div>
    @endif
</div>

<?php if (isset($images)) { ?>
    <div class="form-group">
        <?php foreach ($images as $row) { ?>
            <img src="<?php echo $row['image']; ?>" alt="">
        <?php } ?>
    </div>
<?php } ?>

<div class="form-group">
    <label>Meta Keyword</label>
    <input type="text" name="meta_keyword" value="<?php echo @$rental->meta_keyword; ?>" class="form-control">
    @if($errors->has('meta_keyword'))
    <div class="error alert-danger">{{$errors->first('meta_keyword')}}</div>
    @endif
</div>

<div class="form-group">
    <label>Meta Description</label>
    <input type="text" name="meta_description" value="<?php echo @$rental->meta_description; ?>" class="form-control">
    @if($errors->has('meta_description'))
    <div class="error alert-danger">{{$errors->first('meta_description')}}</div>
    @endif
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label>Status</label>
        <?php echo StatusControl(@$rental->status); ?>
        @if($errors->has('status'))
        <div class="error alert-danger">{{$errors->first('status')}}</div>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" name="draft" class="form-check-input" value="1" <?php echo (@$rental->draft == 1) ? "checked" : ""; ?>>
            Save as Draft. I will publish the rental later.
        </label>
        @if($errors->has('draft'))
        <div class="error alert-danger">{{$errors->first('draft')}}</div>
        @endif
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{asset('/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/timepicker.js')}}"></script>
 
<script>
    $(document).on('click', '.fa-close', function() {
        $(this).parent().remove();
    })
    $(function() {
        $('input[name="start_hour"]').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,

            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $('input[name="end_hour"]').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            dynamic: true,
            dropdown: true,
            scrollbar: true
        });
    })
    $(document).on('change', 'input[name="start_hour"]', function() {
        var time = $('input[name="start_hour"]').val();
        console.log(time);
    })
    $(function() {
        $('input[name="available_start_date"]').daterangepicker({
            timePicker: false,
            timePicker24Hour: false,
            singleDatePicker: true,
            maxSpan: {
                days: 30,

            },
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'YYYY-MM-DD',
                applyLabel: "Apply"
            },
            minDate: "{{(@$rental->available_start_date) ? date('Y-m-d', @$rental->available_start_date) : date('Y-m-d') }}", 
            startDate: "{{(@$rental->available_start_date > time()) ? date('Y-m-d', @$rental->available_start_date) : date('Y-m-d') }}",
            

        });//{{ date('Y-m-d', @$rental->available_start_date) }}
    });
    $(document).on('change', 'input[name="available_start_date"]', function() {
        var date = $('input[name="available_start_date"]').val();
        console.log(date);
        console.log(Date.parse(date));
        var end_start_date = date.toString('YYYY/MM/DD');

        $(function() {
            $('input[name="available_end_date"]').daterangepicker({
                timePicker: false,
                timePicker24Hour: false,
                singleDatePicker: true,
                maxSpan: {
                    days: 0
                },
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'YYYY-MM-DD',
                    applyLabel: "Apply"
                },
                minDate: end_start_date,
                // maxDate: "",
                startDate: end_start_date,

            });
        });
    })
</script>
@endsection