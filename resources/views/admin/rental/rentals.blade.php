@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <form action="{{route('AdminrentalSearch')}}" method="get" class="form-inline">
                <input type="hidden" name="action" value="search">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <input type="text" name="name" class="form-control form-control-sm" value="<?php echo @$_REQUEST['name']; ?>" placeholder="Enter rental name">
                    </div>
                    <div class="col-auto">
                        <select name="status" class="form-control form-control-sm">
                            <option value="">-- Status --</option>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <select name="draft" class="form-control form-control-sm">
                            <option value="">-- Publish --</option>
                            <option value="0">Published</option>
                            <option value="1">Draft</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
                    </div>
                    <div class="col-auto">
                        <a href="{{ route('adminAddRental')}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </form>
        </div>

        <h4 class="card_title">View Rentals</h4>

        <div class="single-table">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th> Type </th>
                        <th>Duration</th>
                      
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($rentals as $k => $rental)
                        <tr>
                            <th scope="row"><?php echo $k + 1; ?></th>
                            <td>
                                <?php echo $rental->name; ?> <?php echo $rental->draft ? " - (Draft)" : ""; ?>
                                <br>
                                <small>By: <?php echo $rental->display_name; ?></small>
                            </td>
                            <td><?php echo $rental->category; ?></td>
                            <td>{{ @$rental->rental_type }}</td>
                      
                            <td>{{ @$rental->duration }} {{ (@$rental->rental_type =='daily') ? 'day' : 'hour' }}{{(@$rental->duration > 1)? 's': ''}}</td>
                            
                         
                            <td><?php echo GetStatus($rental->status); ?></td>
                            <td>
                                <ul class="d-flex justify-content-center">
                                    <li class="mr-3">
                                        <a href="{{ route('AdminEditRental', $rental->id)}}" class="btn btn-inverse-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('AdminremoveRental', @$rental->id) }}" class="btn btn-inverse-danger deleted">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
