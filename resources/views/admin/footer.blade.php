<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/script/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.min.js"></script>
<script src="/script/validation.js"></script>
<script src="/assets/js/jquery.stylish-select.min.js"></script>
<script src="/script/admin.js"></script>
<script src="assets/js/custom.js"></script>
<script>
    $('.my-dropdown').sSelect();
</script>
@yield('script')
</body>
</html>
