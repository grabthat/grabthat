@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <div id="search-bar" class="pull-right text-right">
            <a href="{{ route('allRentalList')}}" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i></a>
        </div>

        <h4 class="card_title">Manage Tax and Survice Fee</h4>
        @include('admin.notification')
        
        <form action="{{ route('Updatesettings')}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
                <label>Tax</label>
                <input type="text" name="tax" value="{{(@$tax_info->tax) ? @$tax_info->tax :old('tax')}}" class="form-control" placeholder="%" required>
                @if($errors->has('tax'))
                <div  class="alert alert-danger">
                    <strong>{{$errors->first('tax')}}</strong>
                </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Service Fee</label>
                <input type="text" name="service_fee" value="{{ (@$tax_info->service_fee) ?@$tax_info->service_fee : old('service_fee')}}" class="form-control" placeholder="%" required>
                @if($errors->has('service_fee'))
                <div  class="alert alert-danger">
                    <strong>{{$errors->first('service_fee')}}</strong>
                </div>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection