@extends('layouts.admin')

@section('content')
<div class="col-md-12 rt_subheader">
    <div class="rt_subheader_main">
        <h3 class="rt_subheader_title mb-mob-2">Hello {{ Auth::user()->first_name }}!</h3>
    </div>

    <div class="subheader_btns">
        <a href="#" class="btn btn-sm btn-primary btn-inverse-primary">
            <span>Today:</span>&nbsp;
            <span>Jan 21</span>
            <i class="feather ft-calendar ml-2"></i>
        </a>
    </div>
</div>

<div class="row mt-4">
    <div class="col-lg-3 col-md-6 stretched_card pl-mob-3 mb-mob-4">
        <div class="card bg-primary analytics_card">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-center justify-content-between">
                    <div class="icon-rounded">
                        <i class="fa fa-home text-primary"></i>
                    </div>
                    <div class="text-white">
                        <p class="mt-xl-0 text-xl-left mb-2">Total Traffic</p>
                        <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                            <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1 text-white">600</h3>
                            <small class="stats_icon">3% <span class="feather ft-chevron-up"></span></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 stretched_card mb-mob-4">
        <div class="card bg-success analytics_card">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
                    <div class="icon-rounded">
                        <i class="fa fa-home text-success"></i>
                    </div>
                    <div class="text-white">
                        <p class="mt-xl-0 text-xl-left mb-2">Total Orders</p>
                        <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                            <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1 text-white">2300</h3>
                            <small class="stats_icon">5% <span class="feather ft-chevron-up"></span></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 stretched_card mb-xs-mob-4">
        <div class="card bg-danger analytics_card">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap  align-items-center justify-content-between">
                    <div class="icon-rounded">
                        <i class="fa fa-users text-danger"></i>
                    </div>
                    <div class="text-white">
                        <p class="mt-xl-0 text-xl-left mb-2">New Customers</p>
                        <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                            <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1 text-white">2765</h3>
                            <small class="stats_icon">2% <span class="feather ft-chevron-up"></span></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 stretched_card pr-mob-3">
        <div class="card bg-dark analytics_card">
            <div class="card-body">
                <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-center justify-content-between">
                    <div class="icon-rounded">
                        <i class="fa fa-home text-dark"></i>
                    </div>
                    <div class="text-white">
                        <p class="mt-xl-0 text-xl-left mb-2">Total Products</p>
                        <div class="d-flex flex-md-column flex-xl-row flex-wrap align-items-baseline align-items-md-center align-items-xl-baseline">
                            <h3 class="mb-0 mb-md-1 mb-lg-0 mr-1 text-white">628</h3>
                            <small class="stats_icon">7% <span class="feather ft-chevron-up"></span></small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 mt-4">
        <div class="card">
            <div class="card-body">
                <h4 class="card_title">Orders Overview</h4>
                <div class="single-table">
                    <div class="table-responsive">
                        <table class="table table-hover progress-table text-center">
                            <thead>
                            <tr>
                                <th scope="col">Order ID</th>
                                <th scope="col">Customer</th>
                                <th scope="col">Product</th>
                                <th scope="col">Date</th>
                                <th scope="col">Price</th>
                                <th scope="col">status</th>
                                <th scope="col">action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">6583</th>
                                <td>Mark Spence</td>
                                <td>Macbook Pro</td>
                                <td>09 / 07 / 2018</td>
                                <td>672.56$</td>
                                <td><span class="badge badge-primary">Progress</span></td>
                                <td>
                                    <ul class="d-flex justify-content-center">
                                        <li class="mr-3">
                                            <button type="button" class="btn btn-inverse-primary">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-inverse-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">4652</th>
                                <td>David Rebon</td>
                                <td>iPhone X</td>
                                <td>09 / 07 / 2018</td>
                                <td>672.56$</td>
                                <td><span class="badge badge-warning">Pending</span></td>
                                <td>
                                    <ul class="d-flex justify-content-center">
                                        <li class="mr-3">
                                            <button type="button" class="btn btn-inverse-primary">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" class="btn btn-inverse-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
