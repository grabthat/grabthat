@extends('layouts.app')

@section('content')

<section class="home-content">
    <div class="container">
        <div class="payment-page">
            @include('myaccount.side-menu')
            <div class="payment-right">
                <h1>Request a withdrawl</h1>
                <div class="bank-details">
                    <div class="bank-details-left">
                        <div class="withdraw-row">
                            <label for="">Account Balance</label>
                            <?php
                            $possible_withdraw = $balance['available_balance'] - $balance['stripe_transfered']->balance;
                            ?>
                            <p> ${{ number_format(@$balance['available_balance'], 2)}}</p>
                        </div>
                        <div class="withdraw-row">
                            <label>Available Balance</label>

                            <input type="text" class="txt-style" placeholder="{{ floor(@$possible_withdraw)}}" disabled>
                            <span>in USD</span>
                        </div>
                        <form action="javascript:;" id="withdrawForm" method="post">
                            @csrf

                            <div class="withdraw-row">
                                <label>Withdraw</label>
                                <input type="text" class="txt-style" id="price_box" placeholder="20" name="withdraw_amount">
                                <span>in USD</span>
                            </div>
                            <div class="form-btn">
                                <input type="submit" value="Submit" class="btn-submit withdraw_cash">
                            </div>
                        </form>
                    </div>
                    <p>
                        All requests will be processed within 24 hours.
                        <br>
                        <br>
                        In case you have requested a withdrawl on weekend, the request will be processed on monday.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
    function preventAlph(className) {
        $(className).keypress(function(event) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    }
    var class_name = $('#price_box, #discount_bx');
    preventAlph(class_name);
    $(document).on('submit', '#withdrawForm', function(e) {
        e.preventDefault();
        $('.withdraw_cash').val('Submitting...').attr('disabled', 'disabled');
        $.ajax({
            url: "{{ route('withdrawcash') }}",
            method: 'POST',

            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    alert(response.message);
                    $('form').removeClass('submitAlliformation');
                    $('.btn-submit').attr('disabled', 'disabled');
                    $('.bank-details').html(response.html);
                } else if (response.status == false) {
                    alert(response.message);
                    $('.withdraw_cash').val('Submit').removeAttr('disabled', 'disabled');
                }
            }
        })
    })
</script>
@endsection