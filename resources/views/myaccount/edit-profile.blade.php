@extends('layouts.app')

@section('content')
<section class="home-content">
    <div class="container">
        <form action="" method="post" enctype="multipart/form-data">
            @csrf

            <div class="profile-view-page flexbox">
                <div class="profile-view-left">
                    <div class="profile-view-leftbox">
                        <div class="profile-view-pic">
                            <img src="<?php echo $user->image; ?>" alt="">

                            <div class="profile-pic-edit">
                                <label>
                                    <input type="file" name="image[]" accept="image/*">
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                </label>
                            </div>
                        </div>

                        <h3><?php echo $user->display_name; ?></h3>
                        <p><?php echo $user->email; ?></p>
                    </div>
                </div>

                <div class="profile-view-right profile-edit">
                    <h3>Personal Detail</h3>

                    <div class="profile-row flexbox">
                        <div class="profile-col">
                            <label>First Name</label>
                            <input type="text" name="first_name" value="<?php echo $user->first_name; ?>" class="txt-style" required>
                        </div>
                        <div class="profile-col">
                            <label>Last Name</label>
                            <input type="text" name="last_name" value="<?php echo $user->last_name; ?>" class="txt-style" required>
                        </div>
                    </div>

                    <div class="profile-row flexbox">
                        <div class="profile-col">
                            <label>Gender</label>

                            <div class="profile-row custom-radio">
                                <label>
                                    <input name="gender" type="radio" value="1" <?php echo ($user->gender == 1) ? "checked" : ""; ?>>
                                    <span></span> Male
                                </label>
                                <label>
                                    <input name="gender" type="radio" value="2" <?php echo ($user->gender == 2) ? "checked" : ""; ?>>
                                    <span></span> Female
                                </label>
                            </div>
                        </div>

                        <div class="profile-col">
                            <label>Date of Birth</label>

                            <div class="profile-row flexbox">
                                <div class="col-25 sel-sty">
                                    <select name="dd" class="my-dropdown">
                                        <option value="">DD</option>
                                        <?php for ($i = 1; $i <= 31; $i++) {
                                            echo '<option value="' . $i . '" '.($user->dob[2] == $i ? "selected" : "").'>' . $i . '</option>';
                                        } ?>
                                    </select>
                                </div>

                                <div class="col-25 sel-sty">
                                    <select name="mm" class="my-dropdown">
                                        <option value="">MM</option>
                                        <?php for ($i = 1; $i <= 12; $i++) {
                                            echo '<option value="' . $i . '" '.($user->dob[1] == $i ? "selected" : "").'>' . $i . '</option>';
                                        } ?>
                                    </select>
                                </div>

                                <div class="col-50 sel-sty">
                                    <select name="yy" class="my-dropdown">
                                        <option value="">YYYY</option>
                                        <?php for ($i = $year; $i > ($year - 100); $i--) {
                                            echo '<option value="' . $i . '" '.($user->dob[0] == $i ? "selected" : "").'>' . $i . '</option>';
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="profile-row flexbox">
                        <div class="profile-col sel-sty">
                            <label>Phone</label>
                            <input type="text" name="phone" value="<?php echo $user->phone; ?>" class="txt-style" required>
                        </div>
                    </div>

                    <div class="profile-row flexbox">
                        <div class="profile-col sel-sty">
                            <label>Country</label>
                            <input type="text" name="country" value="<?php echo $user->country; ?>" class="txt-style" required>
                        </div>

                        <div class="profile-col">
                            <label>State</label>
                            <input type="text" name="state" value="<?php echo $user->state; ?>" class="txt-style" required>
                        </div>
                    </div>

                    <div class="profile-row flexbox">
                        <div class="profile-col">
                            <label>City</label>
                            <input type="text" name="city" value="<?php echo $user->city; ?>" class="txt-style" required>
                        </div>
                    </div>

                    <h3 class="marginTop50">Professional Detail</h3>

                    <div class="profile-row custom-radio">
                        <label>
                            <input name="company_role" type="radio" value="1" <?php echo ($user->company_role == 1) ? "checked" : ""; ?>>
                            <span></span> I am an Employer
                        </label>

                        <label>
                            <input name="company_role" type="radio" value="2" <?php echo ($user->company_role == 2) ? "checked" : ""; ?>>
                            <span></span> I am an Employee
                        </label>
                    </div>

                    <div class="profile-row">
                        <label>Company Name</label>
                        <input type="text" name="company" value="<?php echo $user->company; ?>" class="txt-style">
                    </div>

                    <div class="profile-row">
                        <label>Designation</label>
                        <input type="text" name="designation" value="<?php echo @$user->designation; ?>" class="txt-style">
                    </div>

                    <div class="profile-row">
                        <div class="profile-col">
                            <input type="submit" value="Save" class="submit-btn">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection
