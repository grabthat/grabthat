@csrf
<h1>Submit All Information</h1>
<div class="bank-details">
    <div class="bank-details-left">

        <div class="form-row">
            
                <p>You must agree with our terms and conditions to start your business with us.</p>
                <p> If agreement button is not appearing on your screen you have not updated your information properly. Please update your proper information and try again. </p>
                <div class="profile-row custom-radio">
                    <label>
                        <input name="agreement" type="radio" {{ ($bankdetail->agreement == 'yes') ? 'checked' : 'unchecked'}}>
                        <span></span> I Agree <a href="{{route('homepage')}}">Terms & Conditions</a>
                    </label>

                </div>
             

        </div>




        <div class="form-btn">
            <input type="submit" value="Submit" class="btn-submit bank_info">
        </div>
    </div>
    <p>Please provide correct bank details in order to get the money transferred to your account.<br><br>Bank account details verification will take 3-5 working days.</p>
</div>