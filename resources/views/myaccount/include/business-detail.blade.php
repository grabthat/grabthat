@csrf
<h1>Business Detail</h1>
<div class="bank-details">
    <div class="bank-details-left">
        <!-- <div class="form-row">
            <label for="">Mercent Category code <span class="required_field">*</span></label>
            <input type="text" name="mcc" value="{{ @$bankdetail->mcc}}" class="txt-style" placeholder="">
        </div> -->
        <div class="form-row">
            <label for="">Support Email <span class="required_field">*</span></label>
            <input type="text" name="support_email" value="{{ @$bankdetail->support_email}}" class="txt-style" placeholder="Suppport Email">
        </div>
        <div class="form-row">
            <label for="">Support Phone <span class="required_field">*</span></label>
            <input type="text" name="support_phone" value="{{ @$bankdetail->support_phone}}" class="txt-style" placeholder="Support Phone">
        </div>
        <div class="form-row">
            <label for="">Business Url  </label>
            <input type="text" name="support_url" value="{{ @$bankdetail->support_url}}" class="txt-style" placeholder="Business Url Link">
        </div>
        <div class="form-row">
            <label for="">Other Url  </label>
            <input type="text" name="url" value="{{ @$bankdetail->url}}" class="txt-style" placeholder="Other Web Url" >
        </div>
      
        
       
        <div class="form-btn">
            <input type="submit" value="Next" class="btn-submit bank_info">
        </div>
    </div>
    <p>Please provide correct bank details in order to get the money transferred to your account.<br><br>Bank account details verification will take 3-5 working days.</p>
</div>