@csrf
<h1>Bank Details</h1>
<div class="bank-details">
    <div class="bank-details-left">
        <div class="form-row">
            <label>Bank Name <span class="required_field">*</span></label>
            <input type="text" name="bank_name" class="txt-style" value="{{ @$bankdetail->bank_name }}" placeholder="HSBC CORPORATION LIMITED">
        </div>
        <div class="form-row">
            <label>Account Number <span class="required_field">*</span></label>
            <input type="text" name="account_number" value="{{ @$bankdetail->account_number }}" class="txt-style" placeholder="4500056585">
        </div>
        <div class="form-row">
            <label>Account Holder Name <span class="required_field">*</span></label>
            <input type="text" name="account_holder_name" value="{{ @$bankdetail->account_holder_name }}" class="txt-style" placeholder="Ex.John Smith">
        </div>
        <!-- <div class="form-row">
            <label>Bank Account Type <span class="required_field">*</span></label>
            <select name="account_type" id="" class="txt-style">
                <option value=""></option>
            </select>
        </div> -->
        <div class="form-row">
            <label>Routing Number </label>
            <input type="text" name="routing_number" value="{{ @$bankdetail->routing_number }}" class="txt-style" placeholder="Routing No Optional">
        </div>
        <div class="form-row">
            <label>Bank Location Country <span class="required_field">*</span></label>
            <select name="country_of_bank" id="" class="txt-style">
                <option value="US" selected  {{ (@$bankdetail->country_of_bank  == 'US') ? 'selected' : ''}}>United States</option>
            </select>
        </div>
        <div class="form-row">
            <label>Currency Accepted by bank <span class="required_field">*</span></label>
            <select name="currency_accepted_by_bank" id="" class="txt-style">
                <option value="USD" selected  {{ (@$bankdetail->currency_accepted_by_bank  == 'USD') ? 'selected' : ''}}>US Dollar</option>
            </select>
        </div>


        <div class="form-row">
            <label>Swift Code</label>
            <input type="text" class="txt-style" name="swift_code"  value="{{ @$bankdetail->swift_code }}" placeholder="Ex:4500056585 Optional">
        </div>
         
        <div class="form-btn">
            <input type="submit" value="Next" class="btn-submit">
        </div>
    </div>
    <p>Please provide correct bank details in order to get the money transferred to your account.<br><br>Bank account details verification will take 3-5 working days.</p>
</div>