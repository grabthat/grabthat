@csrf
<h1>Address Detail</h1>
<div class="bank-details">
    <div class="bank-details-left">
        <div class="form-row">
            <label for="">City <span class="required_field">*</span></label>
            <input type="text" name="city" class="txt-style" value="{{ @$user->city }}" placeholder="City">
        </div>
        <div class="form-row">
            <label for="">State <span class="required_field">*</span></label>
            <input type="text" name="state" value="{{ @$user->state }}" class="txt-style" placeholder="State">
        </div>
        <div class="form-row">
            <label for="">Country <span class="required_field">*</span></label>
            <select name="country" id="" class="txt-style">
                <option value="US" selected >United States</option>
                {{--
                    <option value="GB" disabled >Great Bretain</option>
                    <input type="text" name="country" value="{{ @$user->country }}" class="txt-style" placeholder="Country">
                    --}}
            </select>
        </div>
        <div class="form-row">
            <label for="">Address line 1 <span class="required_field">*</span></label>
            <input type="text" name="address_line_1" value="{{ @$bankdetail->address_line_1 }}" class="txt-style" placeholder="Address Line 1">
        </div>
        <div class="form-row">
            <label for="">Address line 2 <span class="required_field">*</span></label>
            <input type="text" name="address_line_2" value="{{ @$bankdetail->address_line_2 }}" class="txt-style" placeholder="Address Line 2">
        </div>
        <div class="form-row">
            <label for="">Postal Code (Zip) <span class="required_field">*</span></label>
            <input type="text" name="postal_code" value="{{ @$bankdetail->postal_code }}" class="txt-style" placeholder="Postal Code">
        </div>
        
        <div class="form-btn">
            <!-- <input type="button" value="Next" class="btn-submit button__address"> -->
            <input type="submit" value="Next" class="btn-submit button__address">
        </div>
    </div>
    <p>Please provide correct bank details in order to get the money transferred to your account.<br><br>Bank account details verification will take 3-5 working days.</p>
</div>