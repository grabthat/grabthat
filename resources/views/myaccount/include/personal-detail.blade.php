<input type="hidden" name="business_type" value="individual">
@csrf
<h1>Personal Detail</h1>
<div class="bank-details">
    <div class="bank-details-left">
        <div class="form-row">
            <label for="">First Name <span class="required_field">*</span></label>
            <input type="text" name="first_name" value="{{ @$personal_detail->first_name}}" class="txt-style" placeholder="First Name">
        </div>
        <div class="form-row">
            <label for="">Last Name <span class="required_field">*</span></label>
            <input type="text" name="last_name" value="{{ @$personal_detail->last_name}}" class="txt-style" placeholder="Last Name">
        </div>
        <div class="form-row">
            <label for="">Date of Birth <span class="required_field">*</span></label>
            <input type="text" name="dob" value="{{ @$personal_detail->dob}}" class="txt-style" placeholder="Date Of Birth">
        </div>
        <div class="form-row">
            <label for="">Phone <span class="required_field">*</span></label>
            <input type="text" name="phone" value="{{ @$personal_detail->phone}}" class="txt-style" placeholder="Phone No">
        </div>
        <div class="form-row">
            <label for="">Email Address <span class="required_field">*</span></label>
            <input type="text" name="email" value="{{ @$personal_detail->email}}" class="txt-style" placeholder="Email Address">
        </div>
         
        <div class="form-row">
            <label for="">Social Security No <span class="required_field">*</span></label>
            <input type="text" name="social_id_number" value="{{ @$bank_info->social_id_number }}" class="txt-style" placeholder="Your Social Security No">
        </div>
        
       
        <div class="form-btn">
            <input type="submit" value="Next" class="btn-submit personal_detail">
        </div>
    </div>
    <p>Please provide correct bank details in order to get the money transferred to your account.<br><br>Bank account details verification will take 3-5 working days.</p>
</div>