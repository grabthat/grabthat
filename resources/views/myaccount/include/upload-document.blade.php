<input type="hidden" name="business_type" value="individual">
@csrf
<h1>Identity Proof Document</h1>
<div class="bank-details">
    <div class="bank-details-left">
        <div class="form-row">
            <label>Front view of Id Proof ducument</label>
            <input type="file" name="document_front_side" class="txt-style" accept="image/*" onchange="showFront(this);">
            @php
                $front_image = asset('/images/categories-img.jpg')
            @endphp
            @if(isset($bankdetail) &&  !empty($bankdetail->id_front) && file_exists(public_path().'/uploads/user/document/'.$bankdetail->id_front))
            @php 
                $front_image = asset('/uploads/user/document/'.$bankdetail->id_front);
            @endphp
            @endif
            <img src="{{ $front_image }}" id="front_image_show" class="img img-thumbnail" alt="">
        </div>
        <div class="form-row">
            <label>Id Proof Back</label>
            <input type="file" name="document_back_side" class="txt-style" accept="image/*" onchange="showBack(this);">
            @php
                $back_image = asset('/images/categories-img.jpg')
            @endphp
            @if(isset($bankdetail) &&  !empty($bankdetail->id_back) && file_exists(public_path().'/uploads/user/document/'.$bankdetail->id_back))
            @php 
                $back_image = asset('/uploads/user/document/'.$bankdetail->id_back);
            @endphp
            @endif
            <img src="{{ $back_image }}" id="back_image_show" class="img img-thumbnail" alt="">
        </div>
        <div class="form-btn">
            <input type="submit" value="Next" class="btn-submit button__upload">
        </div>
    </div>
    <p>Please provide correct ide proof document in order to get the money transferred to your account.<br><br>Personal Identity proof details verification will take 3-5 working days.</p>
</div>