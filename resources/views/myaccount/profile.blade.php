@extends('layouts.app')

@section('content')
<section class="home-content">
    <div class="container">
        <div class="profile-view-page flexbox">
            <div class="profile-view-left">
                <div class="profile-view-leftbox">
                    <div class="profile-view-pic">
                        <img src="<?php echo $user->image; ?>" alt="">
                    </div>

                    <h3><?php echo $user->display_name; ?></h3>
                    <p><?php echo $user->email; ?></p>
                </div>
            </div>

            <div class="profile-view-right">
                <h1>
                    Hi! I am <?php echo $user->display_name; ?>
                    <a href="{{route('editProfile')}}"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Profile</a>
                </h1>

                <p>Joined in <?php echo $user->joined; ?></p>

                <div class="profile-address">
                    <p><?php echo $user->age; ?> <?php echo $user->gender; ?>
                        <br>
                        <?php echo $user->country .', ' .$user->state . ', ' . $user->city ; ?>
                        <br>
                        <?php echo $user->designation; ?>
                    </p>
                </div>

                <h3>Reviews You Have Posted (10)</h3>

                <div class="profile-review">
                    <ul>
                        <li>
                            <div class="review-top">
                                <div class="review-img">
                                    <img src="assets/images/featured-rentals-img.jpg" alt="">
                                </div>
                                <div class="review-info">
                                    <h4>Anna - <span>November, 2019</span></h4>
                                    <div class="review-start">
                                        <img src="assets/images/star.png" alt="">
                                    </div>
                                </div>
                                <p>Old delhiâ€™s charm canâ€™t be left unnoticed with its historic significance,
                                    Indian Cuisines and breathtaking views. Youâ€™ll be able to know about the
                                    hidden heritage that Old Delhi has &amp; its architecture</p>
                            </div>
                        </li>
                        <li>
                            <div class="review-top">
                                <div class="review-img">
                                    <img src="assets/images/featured-rentals-img.jpg" alt="">
                                </div>
                                <div class="review-info">
                                    <h4>Anna - <span>November, 2019</span></h4>
                                    <div class="review-start">
                                        <img src="assets/images/star.png" alt="">
                                    </div>
                                </div>
                                <p>Old delhiâ€™s charm canâ€™t be left unnoticed with its historic significance,
                                    Indian Cuisines and breathtaking views. Youâ€™ll be able to know about the
                                    hidden heritage that Old Delhi has &amp; its architecture</p>
                            </div>
                        </li>
                        <li>
                            <div class="review-top">
                                <div class="review-img">
                                    <img src="assets/images/featured-rentals-img.jpg" alt="">
                                </div>
                                <div class="review-info">
                                    <h4>Anna - <span>November, 2019</span></h4>
                                    <div class="review-start">
                                        <img src="assets/images/star.png" alt="">
                                    </div>
                                </div>
                                <p>Old delhiâ€™s charm canâ€™t be left unnoticed with its historic significance,
                                    Indian Cuisines and breathtaking views. Youâ€™ll be able to know about the
                                    hidden heritage that Old Delhi has &amp; its architecture</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
