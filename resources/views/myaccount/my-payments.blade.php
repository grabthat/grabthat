@extends('layouts.app')

@section('content') 
<link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" />
<section class="home-content">

    <div class="container">
        <div class="payment-page">
            @include('myaccount.side-menu')
            <div class="payment-right">
                <div class="my-rental-head flexbox">
                    <div class="my-rental-left">
                        <h1>My Payments</h1>
                    </div>
                    <div class="my-rental-btn">
                        <input type="button" value="Being a guest" class="active">
                        <input type="button" value="Being a host">
                    </div>
                </div>
                <div class="my-rental-content">
                    <div class="my-rental-row">
                        <ul>
                            <li>
                                <ul>
                                    <li>Reference No</li>
                                    <li>Payment To</li>
                                    <li>Amount</li>
                                    <li>Date</li>
                                    <li>Action</li>
                                </ul>
                            </li>
                            <li>

                                @if(isset($expenses) && $expenses->count())
                                @foreach($expenses as $expenses_data)
                            <li>
                                <ul>
                                    <li>#{{ @$expenses_data->reference_id }}</li>
                                    <li> {{ @$expenses_data->SellerInfo->first_name }} {{ @$expenses_data->SellerInfo->last_name }} </li>
                                    <li> ${{ number_format(@$expenses_data->total_amount, 2) }}</li>
                                    <li>
                                    {{date('M d Y h:i a', strtotime($expenses_data->created_at))}}
                                    </li>
                                    <li>
                                    <button type="button" data-booking_id="{{ @$expenses_data->id }}" class="btn btn-info paidDetail">Detail</a>
                                    </li>
                                </ul>
                            </li>
                            @endforeach
                            @endif
                             
                        </li>

                        </ul>
                    </div>
                    <div class="my-rental-row">
                        <ul>
                            <li>
                                <ul>
                                    <li>Reference No</li>
                                    <li>Payment From</li>
                                    <li>Amount</li>
                                    <li>Date</li>
                                    <li>Action</li>
                                </ul>
                            </li>
                            @if(isset($income) && $income->count())
                            @foreach($income as $income_data)
                            <li>
                                <ul>

                                    <li>#{{ @$income_data->reference_id }}</li>
                                    <li> {{ @$income_data->UserInfo->last_name }} {{ @$income_data->UserInfo->first_name }} </li>
                                    <li>${{ number_format($income_data->rental_income, 2)}}</li>
                                    <li>{{date('M d Y h:i a', strtotime($income_data->created_at))}}</li>
                                    <li>
                                        <button type="button" data-booking_id="{{ @$income_data->id }}" class="payment-details btn">Details</button>
                                    </li>
                                </ul>
                            </li>
                            @endforeach
                            @endif

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Popup -->
    <div class="payment-popup">
        <div class="popup-content">
            <h3>Transaction Details</h3>
            <div class="signin-form">
                <div class="payment-details-box" id="transactionDetail">

                </div>
                <div class="popup-btn">
                    <input type="button" class="popup-close" value="Close">
                </div>
            </div>
        </div>
    </div>
    <div class="dark-bg"></div>
    <!-- Popup -->


</section>

@endsection
@section('script')
<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
<script>
    $(document).on('click', '.paidDetail', function(){
        var booking_id = $(this).data('booking_id');
        
        $.ajax({
            url: "{{ route('transactionDetail') }}",
            method: 'GET',
            data: {
                booking_id: booking_id
            },
            success: (response) => {
                if (response.status == true) {
                    $('#transactionDetail').html(response.data);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
            modalShow();
            }
        });
    })
    $('.payment-details').click(function() {
        var booking_id = $(this).data('booking_id');
        $.ajax({
            url: "{{ route('transactionDetail') }}",
            method: 'GET',
            data: {
                booking_id: booking_id
            },
            success: (response) => {
                if (response.status == true) {
                    $('#transactionDetail').html(response.data);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
                modalShow();
            }
            
        });
        
    });
    function modalShow(){
        $('.payment-popup').show().animate({
            top: '0'
        }, 400);
        $('.dark-bg').fadeIn();
    }
    $('.popup-close').click(function() {
        $('.location-box, .calender-popup, .payment-popup').animate({
            top: '-100px'
        }, 400, function() {
            $(this).hide();
            $('.dark-bg').fadeOut();
        });
    });

    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            position: 'top-end',
            html: html_error,
            timer: 10000
        });
    }

    function DataSuccessInDatabase(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Done',
            html: message,
            confirmButtonText: 'Close',
            timer: 3000,

            timerProgressBar: true,
            onBeforeOpen: () => {
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('b')
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval);

            }
        });
    }
</script>
@endsection