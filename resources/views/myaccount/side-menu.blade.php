<div class="payment-left">
    <div class="balance-box">
         
        Balance <span>${{ number_format(@$balance['available_balance'] , 2) }}</span>
    </div>
    <ul>
        <li>
            <a href="{{route('UserMypayment')}}" class="{{ (\Request::route()->getName() == 'UserMypayment') ? 'active' : ''}}">
                Transactions Summary
            </a>
        </li>
        <li>
            <a href="{{route('makeWithdrawl')}}" class="{{ (\Request::route()->getName() == 'makeWithdrawl') ? 'active' : ''}}">
                Withdraw Funds
            </a>
        </li>
        <li>
            <a href="{{route('myBankDetail')}}" class="{{ (\Request::route()->getName() == 'myBankDetail') ? 'active' : ''}}">
                Bank Details
            </a>
            <!-- <ul>
        <li>
            <a href="">sdfsfdsaf</a>
        </li>
    </ul> -->
        </li>
    </ul>
</div>