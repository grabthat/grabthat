@extends('layouts.app')

@section('content')
<style>
    .required_field {
        color: red;
        font-size: 20px;
        font-weight: 700;
    }

    .rental-duration .profile-row {
        display: flex;
    }

    label.error {
        padding: 5px 15px;
        margin: 0;
        color: white;
        background: red;
    }

    .rental-row textarea.txt-style {

        margin: 0;
    }

    .error_type {
        padding: 0px 5px;
        background: #b01d1d;
        color: white;
    }

    .hidden {
        display: none;
    }

    .txt-style {
        margin-bottom: 0;
    }
</style>
<link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" />
<link href="{{asset('/css/timepicker.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{asset('/css/daterangepicker.css')}}" />
<section class="home-content">
    <div class="container">
        @if(isset($errors))
        <div class="row">
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger col-lg-3 col-md-3 col-12" style="padding: 5px 10px; margin: 5px"> <i class=" fa fa-close" style="cursor: pointer; padding-right:5px"></i>{{ $error }} </div>
            @endforeach

        </div>

        @endif
        <form action="" method="post" enctype="multipart/form-data">
            @csrf

            <div class="add-rental-page">

                <div class="add-rental-row">
                    <h3>Tell us about your rental</h3>

                    <div class="rental-row">
                        <label>Title <span class="required_field">*</span></label>
                        <input type="text" name="name" id="title" class="txt-style">
                        <div class="error_type title_error hidden"></div>
                    </div>

                    <!-- <div class="rental-row">
                        <label>Is this a?</label>

                        <div class="profile-row custom-radio">
                            <label>
                                <input name="rental-type" type="radio" value="1">
                                <span></span> Item
                            </label>
                            <label>
                                <input name="rental-type" type="radio" value="2">
                                <span></span> Skill
                            </label>
                        </div>
                    </div> -->

                    <div class="rental-row">
                        <label>Now choose a rental type <span class="required_field">*</span></label>

                        <div class="profile-row custom-radio custom-radio2">
                            <?php foreach ($categories as $category) { ?>
                                <label>
                                    <input name="category" class="category_item" type="radio" value="<?php echo $category->id; ?>" required>
                                    <span><?php echo $category->name; ?></span>
                                </label>
                            <?php } ?>
                        </div>
                        <div class="error_type rental_type_error hidden"></div>
                    </div>

                    <p>Category description are furnished and serviced by professional management companies. They offer
                        hotel-like amenities such as daily cleaning, laundry service, a concierge and a front desk,
                        making them popular choices for corporate housing and guests staying longer than 30 days.</p>

                    <div class="add-rental-btn">
                        <div class="rental-continueBtn">
                            <input type="button" value="Continue" class="next-btn first__button">
                        </div>
                    </div>
                </div>

                <div class="add-rental-row">
                    <h3>Additional information</h3>

                    <div class="rental-row">
                        <label>Rental details</label>
                        <textarea name="description" id="rental_description" class="txt-style"></textarea>
                        <div class="error_type description_error hidden"></div>
                    </div>

                    <div class="rental-row">
                        <label>Upload thumbnail Image</label>
                        <div class="profile-row">
                            <input type="file" name="image[]" value="Choose File" required onchange="showThumbnail(this);">
                        </div>
                        <div class="error_type image_error hidden"></div>
                        <img src="" alt="No Image" class=" img img-thumbnail  img-sm d-none" id="thumbnail">
                    </div>

                    <div class="rental-row">
                        <label>Related Images</label>
                        <div class="profile-row">
                            <input type="file" name="images[]" multiple   accept="image/*">
                        </div>
                        <small class="form-text text-muted">You can select multiple images</small>
                        @if($errors->has('images'))
                        <div class="error alert-danger">{{$errors->first('images')}}</div>
                        @endif
                    </div>

                    <div class="add-rental-btn">
                        <div class="rental-backBtn">Back</div>

                        <div class="rental-continueBtn">
                            <input type="button" value="Continue" class="next-btn second__button">
                        </div>
                    </div>
                </div>

                <div class="add-rental-row rental-step3">
                    <h3>Price &amp; availability</h3>

                    <div class="rental-row">
                        <label>Please tell us if this is an? <span class="required_field">*</span></label>

                        <div class="profile-row custom-radio rental__timing">
                            <label>
                                <input name="rental_type" type="radio" value="hourly">
                                <span></span> Hourly rental
                            </label>
                            <label>
                                <input name="rental_type" type="radio" value="daily">
                                <span></span> Daily rental
                            </label>
                        </div>
                        <div class="error_type rental_timing_error hidden"></div>
                    </div>

                    <div class="rental-row  rental-duration rental-hourly" style="display: none;">


                        <div class="profile-row">

                            <div class="profile-col">
                                <label>Price (per hour) <span class="required_field">*</span></label>
                                <input type="text" name="price_hourly" class="txt-style">
                                <div class="error_type h_price_error hidden"></div>
                            </div>
                            <div class="profile-col">
                                <label>Define maximum rent hours <span class="required_field">*</span></label>
                                <select name="hours" class="txt-style" required>
                                    <option value="">-- Select --</option>
                                    <?php for ($h = 1; $h <= 24; $h++) { ?>
                                        <option value="<?php echo $h; ?>" <?php echo (@$rental->hours == $h) ? "selected" : ""; ?>>
                                            <?php echo $h; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <div class="error_type h_hour_error hidden"></div>
                            </div>

                        </div>
                        <div class="profile-row">
                            <div class="profile-col">
                                <label>Start Hour <span class="required_field">*</span></label>
                                <input type="text" name="start_hour" value="{{date('h:i a', strtotime(@$rental->start_hour))}}" class="txt-style text-uppercase">
                                <div class="error_type start_hour_error hidden"></div>
                            </div>
                            <div class="profile-col">
                                <label>End Hour <span class="required_field">*</span></label>
                                <input type="text" name="end_hour" value="{{date('h:i a ', strtotime(@$rental->end_hour))}}" class="txt-style text-uppercase">
                                <div class="error_type end_hour_error hidden"></div>
                            </div>
                        </div>

                    </div>

                    <div class="add-rental-calender rental-duration rental-weekly" style="display: none">
                        <div class="profile-row">
                            <div class="profile-col">
                                <label>Enter Daily Price <span class="required_field">*</span></label>
                                <input type="text" name="price_daily" class="txt-style">
                                <div class="error_type day_price_error hidden"></div>
                            </div>
                            <div class="profile-col">
                                <label>Define days a user can rent for <span class="required_field">*</span></label>
                                <select name="days" class="txt-style">
                                    <option value="">-- Select number of days --</option>
                                    <?php for ($h = 1; $h <= 31; $h++) { ?>
                                        <option value="<?php echo $h; ?>" <?php echo (@$rental->hours == $h) ? "selected" : ""; ?>>
                                            <?php echo $h; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <div class="error_type day_limit_error hidden"></div>
                            </div>

                        </div>

                    </div>

                    <h4>Select the dates during which this rental will be available for booking</h4>

                    <div class="profile-row flexbox">
                        <div class="profile-col">
                            <label>Starting from <span class="required_field">*</span></label>
                            <input type="text" name="available_start_date" class="txt-style">
                            <div class="error_type start_date_error hidden"></div>
                        </div>
                        <div class="profile-col">
                            <label>Till date <span class="required_field">*</span></label>
                            <input type="text" name="available_end_date" class="txt-style">
                            <div class="error_type end_date_error hidden"></div>
                        </div>
                    </div>

                    <!-- <div class="rental-row">
                        <label>How would this rental booking be confirmed? </label>

                        <div class="profile-row custom-radio">
                            <label>
                                <input name="confirmation" type="radio" value="1">
                                <span></span> Need Approval
                            </label>
                            <label>
                                <input name="confirmation" type="radio" value="2">
                                <span></span> Instant booking
                            </label>
                        </div>
                    </div> -->

                    <div class="add-rental-btn">
                        <div class="rental-backBtn">Back</div>

                        <div class="rental-continueBtn">
                            <input type="button" value="Continue" class="next-btn third_button">
                        </div>
                    </div>
                </div>

                <div class="add-rental-row">
                    <h3>Location</h3>

                    <div class="rental-row">
                        <label>Address <span class="required_field">*</span></label>
                        <input type="text" name="address" class="txt-style">
                        <div class="error_type address_error hidden"></div>
                    </div>

                    <div class="profile-row flexbox">
                        <div class="profile-col">
                            <label>Country <span class="required_field">*</span></label>
                            <input type="text" name="country" class="txt-style">
                            <div class="error_type country_error hidden"></div>
                        </div>

                        <div class="profile-col">
                            <label>State <span class="required_field">*</span></label>
                            <input type="text" name="state" class="txt-style">
                            <div class="error_type state_error hidden"></div>
                        </div>
                    </div>

                    <div class="profile-row flexbox">
                        <div class="profile-col">
                            <label>City <span class="required_field">*</span></label>
                            <input type="text" name="city" class="txt-style">
                            <div class="error_type city_error hidden"></div>
                        </div>

                        <div class="profile-col">
                            <label>Zip Code <span class="required_field">*</span></label>
                            <input type="text" name="zip" class="txt-style">
                            <div class="error_type zip_error hidden"></div>
                        </div>
                    </div>

                    <div class="add-rental-btn">
                        <div class="rental-backBtn">Back</div>

                        <div class="rental-continueBtn">
                            <input type="button" value="Continue" class="next-btn forth_button">
                        </div>
                    </div>
                </div>

                <div class="add-rental-row">
                    <h3>Other information</h3>

                    <div class="rental-row">
                        <label>This rental includes <span class="required_field">*</span></label>
                        <textarea name="including" class="txt-style"></textarea>
                        <div class="error_type including_error hidden"></div>
                    </div>

                    <div class="rental-row">
                        <label>This rental excludes <span class="required_field">*</span></label>
                        <textarea name="excluding" class="txt-style"></textarea>
                        <div class="error_type excluding_error hidden"></div>
                    </div>

                    <div class="rental-row">
                        <label>Tips for guests <span class="required_field">*</span></label>
                        <textarea name="rental_tips" class="txt-style"></textarea>
                        <div class="error_type rental_tips_error hidden"></div>
                    </div>

                    <div class="rental-row">
                        <label>Cancellation policy <span class="required_field">*</span></label>
                        <textarea name="cancellation_policy" class="txt-style"></textarea>
                        <div class="error_type cancellation_policy_error hidden"></div>
                    </div>

                    <div class="rental-row">
                        <label>Requirements from guests <span class="required_field">*</span></label>
                        <textarea name="guest_requirements" class="txt-style"></textarea>
                        <div class="error_type guest_requirements_error hidden"></div>
                    </div>

                    <div class="add-rental-btn">
                        <div class="rental-backBtn">Back</div>

                        <div class="rental-continueBtn">
                            <input type="submit" value="Submit">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection
@section('script')
<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
<script src="{{asset('/script/validation.js')}}"></script>

<script type="text/javascript" src="{{asset('/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/timepicker.js')}}"></script>
<script>
    function showThumbnail(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
        }
        reader.onload = function(e) {
            $('#thumbnail').attr('src', e.target.result).removeClass('d-none');
        }

        reader.readAsDataURL(input.files[0]);
    }
    $('form').each(function() {
        $(this).validate();
    });
    $(document).on('click', '.fa-close', function() {
        $(this).parent().remove();
    })
    $(function() {
        $('input[name="available_start_date"]').daterangepicker({
            timePicker: false,
            timePicker24Hour: false,
            singleDatePicker: true,
            maxSpan: {
                days: 30,

            },
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'YYYY-MM-DD',
                applyLabel: "Apply"
            },
            minDate: "{{date('Y-m-d')}}",
            startDate: "{{date('Y-m-d')}}",

        });
    });
    $(document).on('change', 'input[name="available_start_date"]', function() {
        var date = $('input[name="available_start_date"]').val();
        console.log(date);
        console.log(Date.parse(date));
        var end_start_date = date.toString('YYYY/MM/DD');

        $(function() {
            $('input[name="available_end_date"]').daterangepicker({
                timePicker: false,
                timePicker24Hour: false,
                singleDatePicker: true,
                maxSpan: {
                    days: 0
                },
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                locale: {
                    format: 'YYYY-MM-DD',
                    applyLabel: "Apply"
                },
                minDate: end_start_date,
                // maxDate: "",
                startDate: end_start_date,

            });
        });
    })
    $(function() {
        $('input[name="start_hour"]').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,

            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $('input[name="end_hour"]').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            dynamic: true,
            dropdown: true,
            scrollbar: true
        });
    })
    $(document).on('click', '.custom-radio2 input', function() {
        $('.custom-radio2 input').each(function() {
            $(this).removeAttr('checked', 'checked');
        })
        $(this).attr('checked', 'checked');
    })

    $(document).ready(function() {
        $('#title').keydown(function() {
            if ($('#title').val().length > 5) {
                $('.title_error').addClass('hidden');
            } else {
                $('.title_error').removeClass('hidden');
            }
        })
        $('.first__button').click(function() {
            var count = 0;
            $('.custom-radio2 input').each(function() {
                if ($(this).prop('checked')) {
                    count = count + 1;
                }
            })
            if ($('#title').val() == '') {
                $('.title_error').removeClass('hidden');
                $('.title_error').html('Field title is empty. Please enter attractive title.');
                return false;
            } else {
                $('.title_error').addClass('hidden');
            }
            if ($('#title').val().length < 4) {
                $('.title_error').removeClass('hidden');
                $('.title_error').html('Field title is very sort. Please enter attractive title.');
                return false;
            } else {
                $('.title_error').addClass('hidden');
            }
            if (count < 1) {
                $('.rental_type_error').removeClass('hidden');
                $('.rental_type_error').html('Category is not selected. Please select a category.');
                return false;
            } else {
                $('.rental_type_error').addClass('hidden');
            }

            $(this).parents('.add-rental-row').hide().next().fadeIn();
        });
    })
    $(document).ready(function() {
        $(function() {
            $('#rental_description').keydown(function() {
                var descrip = $('#rental_description').val().length;
                if ($('#rental_description').val().length > 5) {
                    $('.description_error').addClass('hidden');
                } else {
                    $('.description_error').removeClass('hidden');
                }
            })
        })
        $('.second__button').on('click', function() {
            var descrip = $('#rental_description').val().length;
            if ($('#rental_description').val().length > 5) {
                $('.description_error').addClass('hidden');
            } else {
                $('.description_error').removeClass('hidden');
            }

            if ($('#rental_description').val() == '') {
                $('.description_error').removeClass('hidden');
                $('.description_error').removeClass('hidden').html('Field description is empty. Please enter detail about your rental.');
                return false;
            } else {
                $('.description_error').addClass('hidden');
            }
            if (descrip < 5) {
                $('.description_error').removeClass('hidden');
                $('.description_error').removeClass('hidden').html('Field description is not enough. Describe about your rental more than 5 words.');
                return false;
            } else {
                $('.description_error').addClass('hidden');
            }
            var image = $('.profile-row input[type="file"]').val();
            if (image == null || image == '') {
                $('.image_error').removeClass('hidden').html('Image is not uploaded. Please upload a grab image .');
                return false;
            } else {
                $('.image_error').addClass('hidden')
            }
            $(this).parents('.add-rental-row').hide().next().fadeIn();
        })
    })


    $('input[name=rental_type]').change(function() {
        var _val = $(this).val();

        $('.rental-duration').hide();

        if (_val == 'hourly') $('.rental-hourly').show();
        if (_val == 'daily') $('.rental-weekly').show();
    });


    $('.rental__timing label input').on('click', function() {
        $('.rental_timing_error').addClass('hidden');
        var rentel_timing = $(this).val();
        // alert(rentel_timing);
        $('.rental__timing label input').each(function() {
            $(this).removeAttr('checked', 'checked');
        })
        if (rentel_timing == 'hourly') {
            $(this).attr('checked', 'checked');
        } else if (rentel_timing == 'daily') {
            $(this).attr('checked', 'checked');
        }
    })
    $(document).ready(function() {
        $(document).on('click', '.third_button', function() {
            var checked_rent_type = $('.rental__timing label').find("input:radio:checked").val();

            if (checked_rent_type != 'daily' && checked_rent_type != 'hourly') {
                $('.rental_timing_error').removeClass('hidden').html('Please your rental type either hourly or daily.');
                return false;
            } else {
                $('.rental_timing_error').addClass('hidden');
            }
            if (checked_rent_type == 'hourly') {
                // for hourly
                if (isNaN($('input[name="price_hourly"]').val()) || $('input[name="price_hourly"]').val() < 1) {
                    $('.h_price_error').removeClass('hidden').html('Please put hourly rental price.');
                    return false;
                } else {
                    $('.h_price_error').addClass('hidden');
                }
                var max_hour = $('select[name="hours"]').val();
                console.log(max_hour);
                if (isNaN(max_hour) || max_hour < 1) {
                    $('.h_hour_error').removeClass('hidden').html('Please select maximum possible rental hour per customer.');
                    return false;
                } else {
                    $('.h_hour_error').addClass('hidden');
                }
                var start_hour = $('input[name="start_hour"]').val();
                if (start_hour == null || start_hour == '') {
                    $('.start_hour_error').removeClass('hidden').html('Please select time from where customer can rent.');
                    return false;
                } else {
                    $('.start_hour_error').addClass('hidden')
                }
                var end_hour = $('input[name="end_hour"]').val();
                if (end_hour == null || end_hour == '') {
                    $('.end_hour_error').removeClass('hidden').html('Please select time till where customer can rent.');
                    return false;
                } else {
                    $('.end_hour_error').addClass('hidden')
                }

            } else if (checked_rent_type == 'daily') {
                // for daily
                if (isNaN($('input[name="price_daily"]').val()) || $('input[name="price_daily"]').val() < 1) {
                    $('.day_price_error').removeClass('hidden').html('Please put daily rental price.');
                    return false;
                } else {
                    $('.day_price_error').addClass('hidden');
                }
                var days = $('select[name="days"]').val();
                if (isNaN(days) || days < 1) {
                    $('.day_limit_error').removeClass('hidden').html('Please select maximum possible rental days per customer.');
                    return false;
                } else {
                    $('.day_limit_error').addClass('hidden');
                }
            }
            var start_date = $('input[name="available_start_date"]').val();
            if (!Date.parse(start_date)) {
                $('.start_date_error').removeClass('hidden').html('Please select date from where customer can get service.');
                return false;
            } else {
                $('.start_date_error').addClass('hidden');
            }
            var end_date = $('input[name="available_end_date"]').val();
            if (!Date.parse(end_date)) {
                $('.end_date_error').removeClass('hidden').html('Please select date till the date customer can get service.');
                return false;
            } else {
                $('.end_date_error').addClass('hidden');
            }



            $(this).parents('.add-rental-row').hide().next().fadeIn();
        })

    })

    $(document).ready(function() {
        $('input[name="address"]').keydown(function() {
            if ($('input[name="address"]').val().length > 10) {
                $('.address_error').addClass('hidden');
            } else {
                $('.address_error').removeClass('hidden');
            }
        })
        $('input[name="country"]').keydown(function() {
            if ($('input[name="country"]').val().length > 4) {
                $('.country_error').addClass('hidden');
            } else {
                $('.country_error').removeClass('hidden');
            }
        })
        $('input[name="state"]').keydown(function() {
            if ($('input[name="state"]').val().length > 4) {
                $('.state_error').addClass('hidden');
            } else {
                $('.state_error').removeClass('hidden');
            }
        })

        $('input[name="city"]').keydown(function() {
            if ($('input[name="city"]').val().length > 4) {
                $('.city_error').addClass('hidden');
            } else {
                $('.city_error').removeClass('hidden');
            }
        })
        $('input[name="zip"]').keydown(function() {
            if ($('input[name="zip"]').val().length > 4) {
                $('.zip_error').addClass('hidden');
            } else {
                $('.zip_error').removeClass('hidden');
            }
        })
        $(document).on('click', '.forth_button', function() {

            if ($('input[name="address"]').val() == null || $('input[name="address"]').val() == '' || $('input[name="address"]').val().length < 10) {
                $('.address_error').removeClass('hidden').html('Please enter complete address.');
                return false;
            } else {
                $('.address_error').addClass('hidden');
            }


            if ($('input[name="country"]').val() == null || $('input[name="country"]').val() == '' || $('input[name="country"]').val().length < 4) {
                $('.country_error').removeClass('hidden').html('Please enter valid country name.');
                return false;
            } else {
                $('.country_error').addClass('hidden')
            }
            if ($('input[name="state"]').val() == null || $('input[name="state"]').val() == '' || $('input[name="state"]').val().length < 4) {
                $('.state_error').removeClass('hidden').html('Please enter valid state name.');
                return false;
            } else {
                $('.state_error').addClass('hidden')
            }


            if ($('input[name="city"]').val() == null || $('input[name="city"]').val() == '' || $('input[name="city"]').val().length < 4) {
                $('.city_error').removeClass('hidden').html('Please enter valid city.');
                return false;
            } else {
                $('.city_error').addClass('hidden')
            }



            if ($('input[name="zip"]').val() == null || $('input[name="zip"]').val() == '' || $('input[name="zip"]').val().length < 4) {
                $('.zip_error').removeClass('hidden').html('Please enter valid zip address.');
                return false;
            } else {
                $('.zip_error').addClass('hidden')
            }
            $(this).parents('.add-rental-row').hide().next().fadeIn();

        })
    })

    $(document).ready(function() {

        $('textarea[name="including"]').keydown(function() {
            if ($('textarea[name="including"]').val().length > 5) {
                $('.including_error').addClass('hidden');
            } else {
                $('.including_error').removeClass('hidden');
            }
        })
        $('textarea[name="excluding"]').keydown(function() {
            if ($('textarea[name="excluding"]').val().length > 5) {
                $('.excluding_error').addClass('hidden');
            } else {
                $('.excluding_error').removeClass('hidden');
            }
        })

        $('textarea[name="rental_tips"]').keydown(function() {
            if ($('textarea[name="rental_tips"]').val().length > 5) {
                $('.rental_tips_error').addClass('hidden');
            } else {
                $('.rental_tips_error').removeClass('hidden');
            }
        })


        $('textarea[name="cancellation_policy"]').keydown(function() {
            if ($('textarea[name="cancellation_policy"]').val().length > 5) {
                $('.cancellation_policy_error').addClass('hidden');
            } else {
                $('.cancellation_policy_error').removeClass('hidden');
            }
        })


        $('textarea[name="guest_requirements"]').keydown(function() {
            if ($('textarea[name="guest_requirements"]').val().length > 5) {
                $('.guest_requirements_error').addClass('hidden');
            } else {
                $('.guest_requirements_error').removeClass('hidden');
            }
        })
        $('form').submit(function(e) {
            if ($('textarea[name="including"]').val() == null || $('textarea[name="including"]').val() == '' || $('textarea[name="including"]').val().length < 5) {
                $('.including_error').removeClass('hidden').html('Please enter including information.');
                $("html, body").animate({
                    scrollTop: 0
                }, 2000);
                return false;
            } else {
                $('.including_error').addClass('hidden');
            }

            if ($('textarea[name="excluding"]').val() == null || $('textarea[name="excluding"]').val() == '' || $('textarea[name="excluding"]').val().length < 5) {
                $('.excluding_error').removeClass('hidden').html('Please enter excluding information.');
                $("html, body").animate({
                    scrollTop: 0
                }, 2000);
                return false;

            } else {
                $('.excluding_error').addClass('hidden');
            }

            if ($('textarea[name="rental_tips"]').val() == null || $('textarea[name="rental_tips"]').val() == '' || $('textarea[name="rental_tips"]').val().length < 5) {
                $('.rental_tips_error').removeClass('hidden').html('Please enter rental_tips information.');
                $("html, body").animate({
                    scrollTop: 0
                }, 2000);
                return false;
            } else {
                $('.rental_tips_error').addClass('hidden');
            }

            if ($('textarea[name="cancellation_policy"]').val() == null || $('textarea[name="cancellation_policy"]').val() == '' || $('textarea[name="cancellation_policy"]').val().length < 5) {
                $('.cancellation_policy_error').removeClass('hidden').html('Please enter cancellation_policy information.');
                $("html, body").animate({
                    scrollTop: 0
                }, 2000);
                return false;
            } else {
                $('.cancellation_policy_error').addClass('hidden');
            }

            if ($('textarea[name="guest_requirements"]').val() == null || $('textarea[name="guest_requirements"]').val() == '' || $('textarea[name="guest_requirements"]').val().length < 5) {
                $('.guest_requirements_error').removeClass('hidden').html('Please enter guest_requirements information.');
                $("html, body").animate({
                    scrollTop: 0
                }, 2000);

                return false;
            } else {
                $('.guest_requirements_error').addClass('hidden');
            }


        })
    })




    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: html_error,
            confirmButtonText: 'Close',
            timer: 10000
        });
    }

    function DataSuccessInDatabase(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Done',
            html: message,
            confirmButtonText: 'Close',
            timer: 3000,

            timerProgressBar: true,
            onBeforeOpen: () => {
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('b')
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval);

            }
        });
    }
</script>
@endsection