@extends('layouts.app')
@section('styless')
<link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" />
@endsection
@section('content')
<section class="home-content">
    <div class="container">
        <div class="my-rental-page">
            <div class="my-rental-head flexbox">
                <div class="my-rental-left">
                    <h1>My rentals</h1>
                    <p>Category description are furnished and serviced by professional management companies. </p>
                </div>

                <div class="my-rental-btn">
                    <input type="button" value="Being a Host" class="active">
                    <input type="button" value="Being a Guest">
                </div>
            </div>
            @include('admin/notification')
            <div class="my-rental-content">

                <div class="my-rental-row">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Rental Name</th>
                                <th>No of Booking</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($rentals_host) && $rentals_host->count())
                            @foreach($rentals_host as $key => $rental)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td> {{ @$rental->name}} </td>
                                <td>
                                    <?php $total =  @$rental->totalbooking->count() ?>
                                    <button class="btn btn-success">{{ $total }}</button>
                                    @if($total > 0)
                                    <a href="{{ route('listBooking', $rental->slug) }}" class="btn btn-info">View All</a>
                                    @endif
                                </td>

                                <td>
                                    <ul class="action_buttons">
                                        <li>
                                            <a href="{{route('rentalDetail', @$rental->slug ) }}" class="btn btn-info"><i class=" fa fa-eye"></i> Detail</a>
                                        </li>

                                        <li>

                                            @if($rental->status == 1)
                                            <a href="javascript:;" class="btn btn-danger remove__rental" data-id="{{ @$rental->id }}">
                                                <i class=" fa fa-trash"></i> Cancel
                                            </a>
                                            @else
                                            <button type="button" class="btn btn-info btn-sm"><i class="fa fa-info"></i>Canceled</button>
                                            @endif
                                        </li>
                                    </ul>


                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                </div>

                <div class="my-rental-row">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Reference No</th>
                                <th>Rental Name</th>
                                <th>Posted By</th>
                                <th>From</th>
                                <th> To</th>
                                <th>Booked Date</th>
                                <th>Order Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($rentals_guest) && $rentals_guest->count())
                            @foreach($rentals_guest as $key => $rental_as_guest)

                            <tr>
                                <td> {{ $rental_as_guest->reference_id }} </td>
                                <td> {{ @$rental_as_guest->name}} </td>
                                <td> {{ @$rental_as_guest->display_name}} </td>
                                <td>

                                    @if($rental_as_guest->rental_type == 'hourly')
                                    {{ date('Y-m-d h:i a', @$rental_as_guest->start_date)}}
                                    @else
                                    {{ date('Y-m-d', @$rental_as_guest->start_date)}}
                                    @endif
                                </td>
                                <td>
                                    @if($rental_as_guest->rental_type == 'hourly')
                                    {{ date('Y-m-d h:i a', @$rental_as_guest->end_date)}}
                                    @else
                                    {{ date('Y-m-d', @$rental_as_guest->end_date)}}
                                    @endif
                                </td>
                                <td>{{ @$rental_as_guest->created_at }}</td>
                                <td>{{ @$rental_as_guest->rental_status}}</td>
                                <td>
                                    <ul class="action_buttons">


                                        <li>
                                            @if((($rental_as_guest->start_date - time())/(24*60*60) >= 1) && ($rental_as_guest->rental_status == 'Confirmed' || $rental_as_guest->rental_status == 'Pending'))
                                            <a href="javacscript:;" class="btn btn-danger remove_order" data-id="{{@$rental_as_guest->booking_id}}"> <i class=" fa fa-trash"></i> Cancel</a>
                                            @elseif((@$rental_as_guest->rental_status == 'Cancelling') && ($rental_as_guest->refunded =='Pending'))
                                            <a href="javacscript:;" class="btn btn-info" disabled> Proccessed to Cancel</a>
                                            @elseif(($rental_as_guest->rental_status == 'Canceled' || $rental_as_guest->rental_status == 'Rejected'))
                                            <a href="javacscript:;" class="btn btn-secondary" disabled>Canceled</a>
                                            @endif
 



                                        </li>
                                    </ul>
                                    <ul class="action_buttons">
                                        <li>
                                            <a href="{{route('rentalDetail', @$rental_as_guest->slug ) }}" class="btn btn-info"><i class=" fa fa-eye"></i> Detail</a>
                                        </li>
                                        <li>
                                            <button class="open-button btn btn-primary" onclick="openForm()"> <i class="fa fa-comment"></i> Chat</button>
                                        </li>
                                    </ul>

                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    {{ $rentals_guest->links()}}


                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .action_buttons {
        display: flex;
        padding-left: 0;
    }

    .action_buttons li {
        padding: 0 !important;
        display: inline-flex;
        margin-right: 5px;
    }

    .action_buttons .btn {
        padding: 5px 10px;
        line-height: 20px;
    }

    .action_buttons .btn:active,
    .action_buttons .btn:focus {
        outline: none;
        box-shadow: none;
    }

    /* body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;} */

    /* Button used to open the chat form - fixed at the bottom of the page */
    /* .open-button {
  background-color: #555;
  color: white; */
    /* padding: 16px 20px; */
    /* border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
} */

    /* The popup chat - hidden by default */
    .chat-popup {
        display: none;
        position: fixed;
        bottom: 0;
        right: 15px;
        border: 3px solid #f1f1f1;
        z-index: 9;
    }

    /* Add styles to the form container */
    .form-container {
        max-width: 300px;
        padding: 10px;
        background-color: white;
    }

    /* Full-width textarea */
    .form-container textarea {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;
        resize: none;
        min-height: 200px;
    }

    /* When the textarea gets focus, do something */
    .form-container textarea:focus {
        background-color: #ddd;
        outline: none;
    }

    /* Set a style for the submit/send button */
    .form-container .btn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        margin-bottom: 10px;
        opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .form-container .cancel {
        background-color: red;
    }

    /* Add some hover effects to buttons */
    .form-container .btn:hover,
    .open-button:hover {
        opacity: 1;
    }
</style>

@endsection
@section('script')
<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
<script>
    $(document).on('click', '.remove_order', function() {
        Swal.fire({
            title: 'Are you sure want to delete this Rental Order ?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('cancelRentalOrder') }}",
                    method: "POST",
                    data: {
                        id: $(this).data('id'),
                        _token: "{{ @csrf_token() }}"
                    },
                    success: (response) => {
                        if (response.status == false) {
                            FailedResponseFromDatabase(response.message);
                        }
                        if (response.status == true) {
                            DataSuccessInDatabase(response.message);
                            setTimeout(() => {
                                location.reload();
                            }, 3000);

                        }
                    }
                })

            }
        })
    })

    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            position: 'top-end',
            html: html_error,
            timer: 10000
        });
    }
    $(document).on('click', '.remove__rental', function() {
        Swal.fire({
            title: 'Are you sure want to delete this Rental ?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('removeRental') }}",
                    method: "POST",
                    data: {
                        id: $(this).data('id'),
                        _token: "{{ @csrf_token() }}"
                    },
                    success: (response) => {
                        if (response.status == false) {
                            FailedResponseFromDatabase(response.message);
                        }
                        if (response.status == true) {
                            DataSuccessInDatabase(response.message);
                            setTimeout(() => {
                                location.reload();
                            }, 3000);

                        }
                    }
                })

            }
        })
    })

    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            position: 'top-end',
            html: html_error,
            timer: 10000
        });
    }

    function DataSuccessInDatabase(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Done',
            html: message,
            confirmButtonText: 'Close',
            timer: 3000,

            timerProgressBar: true,
            onBeforeOpen: () => {
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('b')
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval);

            }
        });
    }

    function openForm() {
        document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
        document.getElementById("myForm").style.display = "none";
    }
</script>
@endsection