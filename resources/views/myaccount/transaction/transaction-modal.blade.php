<ul>
    <li><span>Reference.No</span> #{{ @$booking_info->reference_id }}</li>
    <li><span>Rental Name</span> {{ @$booking_info->rentalInfo->name }}</li>
    <li>
        <span>Date of Booking</span> 
        From
        @if(@$booking_info->rental_type == 'hourly')
        {{ date('M d Y h:i a',  @$booking_info->start_date)}}
        @else 
        {{ date('M d Y',  @$booking_info->start_date)}}
        @endif
        <br> 
        To 
      
        @if(@$booking_info->rental_type == 'hourly')
        {{ date('M d Y h:i a', @$booking_info->end_date)}}
        @else 
        {{ date('M d Y', @$booking_info->end_date)}}
        @endif
</li>
</ul>
<ul>
    <li><span>Duration</span>
    {{$booking_info->booked_duration}} 
     {{ (@$booking_info->rental_type =='hourly') ? 'hour' : 'day'}}{{ ($booking_info->booked_duration  > 1) ? 's' : ''}}
    </li>
    <li>
    <span>
        Price ${{ @$booking_info->price}} <br>
        Tax {{ @$booking_info->tax_percent}}% <br> 
        Service Fee {{ @$booking_info->service_fee_percent}}%
    </span> 
        ${{ @$booking_info->price * $booking_info->booked_duration }}<br> 
        ${{$booking_info->tax_amount}}<br> 
        ${{$booking_info->fee_amount}}
    </li>
    <li><span>Amount</span> ${{ $booking_info->total_amount }}</li>
</ul>