@extends('layouts.app')

@section('content')
<link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" />
<section class="home-content">
    <div class="container">
        @include('admin.notification')
        <div class="payment-page">
            @include('myaccount.side-menu')
            <div class="payment-right">
                <form action="{{route('updateBankDetail')}}" method="post" id="bankDetailForm" class="bankDetailForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form__wrapper">
                        @include('myaccount.include.personal-detail')
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
<script>
    $(document).on('submit', 'form.bankDetailForm', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('updatePersonalDetail') }}",
            method: 'POST',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    DataSuccessInDatabase(response.message);
                    $('form').addClass('updateAddressDetail').removeClass('bankDetailForm');
                    $('.form__wrapper').html(response.html);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
            }
        })
    })

    $(document).on('submit', 'form.updateAddressDetail', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('updateAddressDetail') }}",
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    DataSuccessInDatabase(response.message);
                    $('form').removeClass('updateAddressDetail').addClass('uploadFile');
                    $('.form__wrapper').html(response.html);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
            }
        })
    })

    function showFront(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
        }
        reader.onload = function(e) {
            $('#front_image_show').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }

    function showBack(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
        }
        reader.onload = function(e) {
            $('#back_image_show').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }


    $(document).on('submit', 'form.uploadFile', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('uploadIdproof') }}",
            method: 'POST',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    DataSuccessInDatabase(response.message);
                    $('form').removeClass('uploadFile').addClass('updateBusindessDetail');
                    $('.form__wrapper').html(response.html);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
            }
        })
    })

    $(document).on('submit', 'form.updateBusindessDetail', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('updateBusindessDetail') }}",
            method: 'POST',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    DataSuccessInDatabase(response.message);
                    $('form').removeClass('updateBusindessDetail').addClass('updateBankDetail');
                    $('.form__wrapper').html(response.html);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
            }
        })
    })
    $(document).on('submit', '.updateBankDetail', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('updateBankDetail') }}",
            method: 'POST',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    DataSuccessInDatabase(response.message);
                    $('form').removeClass('updateBankDetail').addClass('submitAlliformation');
                    $('.form__wrapper').html(response.html);
                } else if (response.status == false) {
                    FailedResponseFromDatabase(response.message);
                }
            }
        })
    })
    $(document).on('submit', '.submitAlliformation', function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('submitAllBankInfo') }}",
            method: 'POST',
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success: (response) => {
                if (response.status == true) {
                    alert(response.message);
                    $('form').removeClass('submitAlliformation');
                    $('.btn-submit').attr('disabled', 'disabled');
                    $('.form__wrapper').html(response.html);
                } else if (response.status == false) {
                    alert(response.message);
                }
            }
        })

    })

    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            position: 'top-end',
            html: html_error,
            timer: 10000
        });
    }

    function DataSuccessInDatabase(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Done',
            html: message,
            confirmButtonText: 'Close',
            timer: 3000,

            timerProgressBar: true,
            onBeforeOpen: () => {
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('b')
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval);
            }
        });
    }
</script>
@endsection