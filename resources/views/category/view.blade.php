@extends('layouts.app')

@section('content')
<section class="home-content">
    <div class="container">
        <h3>Explore <?php echo $category->name;?></h3>

        <div class="featured-rentals-sec">
            <ul>
                <?php foreach ($rentals as $rental) { ?>
                <li>
                    <div class="featured-img">
                        <a href="/rental/<?php echo $rental->slug; ?>">
                            <img src="<?php echo $rental->image; ?>" alt="">
                        </a>
                    </div>

                    <div class="featured-content">
                        <h4><?php echo $rental->city; ?></h4>

                        <div class="stars flexbox">
                            <img src="/images/star.png" alt="">
                            <div class="star-rating">
                                4.5/5.0 (19 reviews)
                            </div>
                        </div>

                        <h3><?php echo $rental->name; ?></h3>
                        <p>From : <?php echo $rental->price; ?></p>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>
@endsection
