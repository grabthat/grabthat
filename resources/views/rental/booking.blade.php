@extends('layouts.app')
@section('content')
<style>
    .booking-page {
        padding: 70px 0;
    }

    .booking-right ul li {
        display: inline-block;
    }

    .booking-right ul li:nth-child(odd) {
        width: 60%;
    }

    .booking-right ul li:nth-child(even) {
        width: 30%;
    }
</style>
<section class="home-content">
    <div class="container">
        <form action="{{ route('checkout') }}" method="post">
            <div class="booking-page flexbox">
                @csrf
                <div class="booking-left">
                    <h1>Confirm Your Booking</h1>
                    <p>Category description are furnished and serviced by professional management companies.</p>
                    <h3>Order Details</h3>
                    <ul class="booking-table">

                        <li>
                            <ul>
                                <li>Order ID</li>
                                <li>{{ $booking_detail['order_id'] }}</li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li>Rental Name</li>
                                <li>{{ $item_info->name }}</li>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <li>Booking Date (from) </li>
                                <li>
                                    {{date('F d, Y',  @$booking_detail['start_date'])}} <br>
                                    @if(@$booking_detail['rental_type'] == 'hourly')
                                    {{date('h:i a', @$booking_detail['start_date'] )}}
                                    @endif
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li>Booking Date (from) </li>
                                <li>
                                    {{date('F d, Y',  @$booking_detail['end_date'] )}} <br>
                                    @if(@$booking_detail['rental_type'] == 'hourly')
                                    {{date('h:i a',@$booking_detail['end_date']  )}}
                                    @endif

                                </li>
                            </ul>
                        </li>
                        
                        <li>
                            <ul>
                                <li>
                                    Booking {{ @$booking_detail['rent_type']}}
                                </li>
                                <li>
                                    {{ @$booking_detail['duration']}}
                                    {{ @$booking_detail['length']}}

                                </li>
                            </ul>
                        </li>
                    </ul>

                    <h3>Billing Details</h3>

                    <h4>{{ $user->display_name }}</h4>
                    <input type="text" class="form-control" placeholder="city" name="city" value="{{ $user->city }}"><br>
                    <input type="text" class="form-control" placeholder="state" name="state" value="{{ $user->state }}"><br>
                    <input type="text" class="form-control" placeholder="country" name="country" value="{{ $user->country }}"><br>
                    <input type="phone" class="form-control" placeholder="zipcode" name="zipcode" value="{{ $user->zip }}"><br>
                    <input type="email" class="form-control" placeholder="email" name="email" value="{{ $user->email }}"><br>
                    <input type="number" class="form-control" placeholder="phone no" name="phone" value="{{ $user->phone }}"><br>
                </div>

                <div class="booking-right">
                    <input type="hidden" name="rental_id" value="{{ @$item_info->id }}">
                    <input type="hidden" name="user_id" value="{{ @$user->id }}">
                    <input type="hidden" name="start_date" value="{{ @$booking_detail['start_date'] }}">
                    <input type="hidden" name="end_date" value="{{ @$booking_detail['end_date'] }}">

                      
                    <input type="hidden" name="total_amount" value="{{ @$booking_detail['total_cost'] }}">

                    <input type="hidden" name="reference_id" value="{{ @$booking_detail['order_id']}}">
                    <h3>Your Total Payable</h3>
                    <ul>
                        <li>
                            Cost Per {{ @$booking_detail['rent_type']}}
                        </li>
                        <li> USD {{ @$booking_detail['price'] }} </li>
                        <li> Selected {{ @$booking_detail['length']}}: </li>
                        <li> {{ @$booking_detail['duration']}} {{ @$booking_detail['length']}}</li>
                        <li> Total amount : </li>
                        <li> USD {{ $booking_detail['total_amount'] }}</li>
                        <li> Tax ({{ $booking_detail['tax_percent'] }}%) : </li>
                        <li> USD {{ $booking_detail['tax_amount'] }}</li>
                        <li> Service Fee ({{ $booking_detail['sc_percent'] }} %) :</li>
                        <li> USD {{ $booking_detail['sc_amount']}}</li>
                        <li> Amount to pay : </li>
                        <li> USD {{ $booking_detail['total_cost'] }}</li>
                    </ul>
                    <input type="hidden" name="order_id" value="{{ $booking_detail['order_id']}}">
                    <button type="submit" value="Checkout Now" class="checkout-btn">Checkout Now</button>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection
 
@section('script')
<script>
 
$(window).on('load', function(){
var data = {
"start_date" : "{{$booking_detail['start_date']}}",
"end_date" : "{{$booking_detail['end_date']}}",
 
"rental_type" : "{{$booking_detail['rental_type']}}",
"length" : "{{$booking_detail['length']}}",
"rent_type" : "{{$booking_detail['rent_type']}}",
"duration" : "{{$booking_detail['duration']}}",
"price" : "{{$booking_detail['price']}}",
"tax_percent" : "{{$booking_detail['tax_percent']}}",
"tax_amount" : "{{$booking_detail['tax_amount']}}",
"sc_percent" : "{{$booking_detail['sc_percent']}}",
"sc_amount" : "{{$booking_detail['sc_amount']}}",
"total_amount" : "{{$booking_detail['total_amount']}}",
"total_cost" : "{{$booking_detail['total_cost']}}",
"order_id" : "{{$booking_detail['order_id']}}",
'rental_id' : "{{$booking_detail['rental_id']}}",
'seller_id' :  "{{$booking_detail['seller_id']}}"
}

 

return localStorage.setItem("bookinginfo", JSON.stringify(data));
})
</script>
@endsection