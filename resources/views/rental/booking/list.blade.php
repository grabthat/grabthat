@extends('layouts.app')
@section('styless')
<link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" />
@endsection
@section('content')
<section class="home-content">
    <div class="container">
        <div class="my-rental-page">
            <div class="my-rental-head flexbox">
                <h1 style="font-size: 25px">Rental Name : {{ @$rental_info->name}}</h1>
            </div>
            @include('admin/notification')
            <div class="my-rental-content">

                <div class="my-rental-row">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>User Name</th>
                                <th> Booking For</th>
                                <th> Booked On</th>
                                <th>Booking status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($all_booking) && $all_booking->count())
                            @foreach($all_booking as $key => $booking_data)
                            <tr>
                                <td>{{ $booking_data->reference_id }}</td>
                                <td> {{ @$booking_data->UserInfo->first_name}} {{ @$booking_data->UserInfo->last_name}} </td>
                                <td>
                                From :
                                    @if(@$booking_data->rental_type == 'hourly')
                                     {{ date('M d  Y, h:i a', @$booking_data->start_date)}}   
                                    @else 
                                    {{ date('M d  Y,  ', @$booking_data->start_date)}}  
                                    @endif
                                    <br>
                                    To:
                                    @if(@$booking_data->rental_type == 'hourly')
                                     {{ date('M d  Y, h:i a', @$booking_data->end_date)}}   
                                    @else 
                                    {{ date('M d  Y, ', @$booking_data->end_date)}}  
                                    @endif
                                </td>
                                <td>
                                {{   @$booking_data->created_at}}  
                                </td>
                                <td>
                                
                                    <span class="btn btn-sm btn-{{ (@$booking_data->rental_status == 'Cancelling') ? 'danger' : ((@$booking_data->rental_status == 'Confirmed') ? 'info' : 'warning')}}" >
                                        {{@$booking_data->rental_status}}
                                    </span>
                                </td>

                                <td>
                                    <ul class="action_buttons">
                                        @if($booking_data->rental_status == 'Confirmed' &&  ($booking_data->start_date < time()))
                                        <li>
                                            <button type="button" class="btn btn-info delivered" data-id="{{ @$booking_data->id}}">Mark as  Delivered</button>
                                        </li>
                                        @endif
                                        @if($booking_data->rental_status == 'Delivered')
                                        <li>
                                            <button type="button" class="btn btn-info" data-id="{{ @$booking_data->id}}">  Delivered</button>
                                        </li>
                                        @endif
                                        @if(($booking_data->rental_status == 'Pending'))
                                        <li>
                                            <button type="button" class="btn btn-success confirm_rental" data-id="{{ @$booking_data->id}}"> <i class="fa fa-send"></i> Confirm</button>
                                        </li>
                                        @endif

                                        @if(($booking_data->rental_status != 'Canceled') && ($booking_data->rental_status != 'Delivered') && ($booking_data->start_date > time()))
                                        <li>
                                            <button class="btn btn-danger CancelCustomerBooking" data-id="{{ @$booking_data->id}}"> <i class=" fa fa-trash"></i> Cancel</button>
                                        </li>
                                        @endif
                                    </ul>
                                    <ul class="action_buttons">

                                        <li>
                                            <a href="javascript:;" class="btn btn-info buyer_detail" data-id="{{ @$booking_data->id}}"><i class=" fa fa-eye"></i> Detail</a>
                                        </li>
                                        <li>
                                            <button class="open-button btn btn-primary" onclick="openForm()"> <i class="fa fa-comment"></i> Chat</button>
                                        </li>
                                    </ul>

                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    {{$all_booking->links()}}
                </div>
                <div class="chat-popup" id="myForm">
                    <form action="/action_page.php" class="form-container">
                        <h1>Chat</h1>

                        <label for="msg"><b>Message</b></label>
                        <textarea placeholder="Type message.." name="msg" required></textarea>

                        <button type="submit" class="btn">Send</button>
                        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Popup -->
    <div class="payment-popup">
        <div class="popup-content">
            <h3>Transaction Details</h3>
            <div class="signin-form">
                <div class="payment-details-box" id="paymentInfo">

                </div>
                <div class="popup-btn">
                    <input type="button" class="popup-close" value="Close">
                </div>
            </div>
        </div>
    </div>
    <div class="dark-bg"></div>
    <!-- Popup -->
</section>
<style>
    .action_buttons {
        display: flex;
        padding-left: 0;
    }

    .action_buttons li {
        padding: 0 !important;
        display: inline-flex;
        margin-right: 5px;
    }

    .action_buttons .btn {
        padding: 5px 10px;
        line-height: 20px;
    }

    .action_buttons .btn:active,
    .action_buttons .btn:focus {
        outline: none;
        box-shadow: none;
    }

    /* body {font-family: Arial, Helvetica, sans-serif;}
    * {box-sizing: border-box;} */

    /* Button used to open the chat form - fixed at the bottom of the page */
    /* .open-button {
    background-color: #555;
    color: white; */
    /* padding: 16px 20px; */
    /* border: none;
    cursor: pointer;
    opacity: 0.8;
    position: fixed;
    bottom: 23px;
    right: 28px;
    width: 280px;
    } */

    /* The popup chat - hidden by default */
    .chat-popup {
        display: none;
        position: fixed;
        bottom: 0;
        right: 15px;
        border: 3px solid #f1f1f1;
        z-index: 9;
    }

    /* Add styles to the form container */
    .form-container {
        max-width: 300px;
        padding: 10px;
        background-color: white;
    }

    /* Full-width textarea */
    .form-container textarea {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        border: none;
        background: #f1f1f1;
        resize: none;
        min-height: 200px;
    }

    /* When the textarea gets focus, do something */
    .form-container textarea:focus {
        background-color: #ddd;
        outline: none;
    }

    /* Set a style for the submit/send button */
    .form-container .btn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        border: none;
        cursor: pointer;
        width: 100%;
        margin-bottom: 10px;
        opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .form-container .cancel {
        background-color: red;
    }

    /* Add some hover effects to buttons */
    .form-container .btn:hover,
    .open-button:hover {
        opacity: 1;
    }
</style>
<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
<script>
    function openForm() {
        document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
        document.getElementById("myForm").style.display = "none";
    }
</script>
<script>
    $(document).on('click', '.CancelCustomerBooking', function(){
        var id = $(this).data('id');
        // alert(id);

        Swal.fire({
            title: 'Are you sure want to Cancel this booking Order ?',
            text: "You won't be able to revert this!",
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Not Now',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('CancelCustomerBooking') }}",
                    method: "POST",
                    data: {
                        id: id,
                        _token: "{{ @csrf_token() }}"
                    },
                    success: (response) => {
                        if (response.status == false) {
                            FailedResponseFromDatabase(response.message);
                        }
                        if (response.status == true) {
                            DataSuccessInDatabase(response.message);
                            setTimeout(() => {
                                location.reload();
                            }, 3000);

                        }
                    }
                })

            }
        })
    })
    $(document).on('click', '.confirm_rental', function() {
        var id = $(this).data('id');
        // alert(id);

        Swal.fire({
            title: 'Are you sure want to confirm this booking Order ?',
          
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Not Now',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('confirmRental') }}",
                    method: "POST",
                    data: {
                        id: id,
                        _token: "{{ @csrf_token() }}"
                    },
                    success: (response) => {
                        if (response.status == false) {
                            FailedResponseFromDatabase(response.message);
                        }
                        if (response.status == true) {
                            DataSuccessInDatabase(response.message);
                            setTimeout(() => {
                                location.reload();
                            }, 3000);

                        }
                    }
                })

            }
        })



    })
    $('.buyer_detail').click(function() {
        var booking_id = $(this).data('id');
        $.ajax({
            url: "{{ route('buyerdetail') }}",
            method: 'GET',
            data: {
                id: booking_id,
            },
            success: (response) => {
                if (response.status == true) {
                    $('#paymentInfo').html(response.data);
                    $('.payment-popup').show().animate({
                        top: '0'
                    }, 400);
                    $('.dark-bg').fadeIn();

                } else if (response.status == false) {
                    alert(response.message);
                }

            }
        })
    });
    $(document).on('click', '.delivered', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        Swal.fire({
            title: 'Are you sure want to confirm this booking as Delivered ?',
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Not Now',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('DeliveredRental') }}",
                    method: "POST",
                    data: {
                        id: id,
                        _token: "{{ @csrf_token() }}"
                    },
                    success: (response) => {
                        if (response.status == false) {
                            FailedResponseFromDatabase(response.message);
                        }
                        if (response.status == true) {
                            DataSuccessInDatabase(response.message);
                            setTimeout(() => {
                                location.reload();
                            }, 3000);

                        }
                    }
                })

            }
        })

    })

    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            position: 'top-end',
            html: html_error,
            timer: 10000
        });
    }

    function DataSuccessInDatabase(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Done',
            html: message,
            confirmButtonText: 'Close',
            timer: 3000,

            timerProgressBar: true,
            onBeforeOpen: () => {
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('b')
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval);

            }
        });
    }
    $('.popup-close').click(function() {
        $('.location-box, .calender-popup, .payment-popup').animate({
            top: '-100px'
        }, 400, function() {
            $(this).hide();
            $('.dark-bg').fadeOut();
        });
    });
</script>
@endsection
@section('script')
@endsection