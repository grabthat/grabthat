@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="{{ asset('/css/datepicker.css') }}">
<link href="{{asset('/css/sweetalert.css')}}" rel="stylesheet" />
<link href="{{asset('/css/timepicker.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="{{asset('/css/daterangepicker.css')}}" />

<style>
    .datepicker.input-daterange ul {
        padding: 0;
        margin: 0;
        display: flex;
        border: 0;
    }

    .txt-style {
        border: none;
        margin-bottom: 0;
    }

    .txt-style:focus,
    .txt-style:active {
        border: none;
        box-shadow: none;
    }

    .datepicker.input-daterange ul li {
        margin: 0;
        padding: 0;
        border: 0;
    }

    .submit__button {
        padding: 5px 20px;
        border: none;
        font-size: 19px;
        font-weight: 600;
        margin-left: 5px;
    }

    .submit__button:hover {
        background: red;
        transition: 300ms ease all;
    }

    .hidden {
        display: none;
    }

    input.txt-style.date2,
    input.txt-style.date1 {
        margin: 0;
    }
</style>


<section class="inner-page-content">
    <div class="container">
        <div class="details-page">
            <form action="{{route('bookNow', $result->slug) }}" method="POST" enctype="multipart/form-data">

                {{@csrf_field()}}
                <div class="details-page-nav">
                    <ul>
                        <li class="active"><span id="details-info-sec">Overview</span></li>
                        <li><span id="details-photo-sec">Photos({{ $images->count()}})</span></li>
                        <li><span id="details-location-sec">Location</span></li>
                        <li><span id="details-availability">Check Availability</span></li>
                        <li><span id="details-remember">Things To Remember</span></li>
                        <li><span id="details-reviews">Reviews</span></li>
                    </ul>
                </div>

                <div class="details-info-sec flexbox">
                    <div class="details-info-content">
                        <h1><?php echo $result->name; ?></h1>

                        <h2>Maximum Duration of Rental : {{ @$result->duration}} {{ (@$result->rental_type =='hourly') ? 'hour' : 'day' }}{{ (@$result->duration >1) ? 's' : '' }}</h2>

                        <h3>Other Details :</h3>
                        <p>{!! html_entity_decode($result->description)!!}</p>

                        <h3>This rental includes :</h3>
                        {!! html_entity_decode($result->including) !!}

                        <h3>This rental excludes :</h3>
                        {!! html_entity_decode( $result->excluding) !!}
                    </div>

                    <div class="listed-by">
                        <h4>Rental Listed By</h4>

                        <div>
                            <a href="/user/<?php echo $user->username; ?>"><img src="<?php echo $user->image; ?>" alt=""></a>

                            <h3>
                                <a href="{{route('sellerDetail', @$user->username)}}">
                                    <?php echo $user->display_name; ?>
                                </a>
                            </h3>

                            <?php if ($user->designation) { ?>
                                <p><?php echo $user->designation; ?></p>
                            <?php } ?>

                            <p> <?php echo $user->city; ?>, <?php echo $user->state; ?>, <?php echo $user->country; ?></p>
                            <p><?php echo $user->dob; ?> <?php echo $user->gender; ?></p>
                        </div>
                    </div>
                </div>

                <?php if (count($images) > 0) { ?>
                    <div class="details-secs details-photo-sec">
                        <h3>Photos</h3>
                        <ul>
                            <?php foreach ($images as $row) { ?>
                                <li class="mb-3"><img src="<?php echo $row->image; ?>" alt=""></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } else { ?>
                    <div class="details-secs details-photo-sec">
                        <h3>Photos</h3>
                        <div>
                            No photos yet
                        </div>
                    </div>

                <?php } ?>
                <div class="details-secs details-location-sec">
                    <h3>Locate Yourself</h3>

                    <p>We'lll be at the heart of Old Delhi i.e. it’s Chandni Chowk. You can grab a metro and exit from Lal Quila metro station Gate No. 1. We’ll be standing right near the exit gate.</p>
                    <div class="details-map">
                        <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $result->latitude; ?>,<?php echo $result->longitude; ?>&amp;key=AIzaSyBnC3pZjxxNQg5mpxV9x_FJOUdTLb6aAJM" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

                    </div>
                </div>

                <div class="details-bottom-sec details-availability">
                    <div class="details-left">
                        <h3>Availability</h3>
                        <div id="availabiltiydatpicker"></div>
                    </div>

                    <div class="details-right">
                        <ul>
                            <li class="flexbox">

                                <div class='datepicker '>
                                    <input type="text" name="daterange" class="txt-style date_range_select" value="{{date('m/d/Y')}} -  {{date('m/d/Y')}}" />
                                    @if($result->rental_type == 'daily')
                                    <small>To select single day press 2 times on same date.</small>
                                    @endif
                                    @if($result->rental_type == 'hourly')
                                    <input type="text" name="start_time" placeholder="Time From" id="start_time" class="txt-style timepicker profile-col">
                                    <input type="text" name="end_time" placeholder="Time To" id="end_time" class="txt-style end_time profile-col">
                                    <small style="padding-left:10px">Ex:5:00 am </small><br>
                                    <small style="padding-left:10px">Enter Time between {{date('h:i a',@$result->available_start_date)}} to {{date('h:i a',  @$result->available_end_date)}}</small>
                                    @endif
                                    <div class="datepicker-header"></div>
                                </div>
                                <input type="hidden" name="rental_type" value="{{$result->rental_type  }}">
                                <input type="hidden" name="reference_id" value="{{ $reference_id }}">

                                <div class="choose-btn">Check Avalability </div>
                            </li>
                            <li class="selected_detail hidden">

                            </li>
                        </ul>
                    </div>
                </div>

                <div class="details-bottom-sec details-remember">
                    <div class="details-left">
                        <h3>Things To Remember</h3>
                    </div>

                    <div class="details-right">
                        <ul>
                            <li>
                                <h4>Cancellation policy</h4>
                                {!! html_entity_decode( $result->cancellation_policy) !!}
                            </li>
                            <li>
                                <h4>Guest Requirements</h4>
                                {!! html_entity_decode($result->guest_requirements) !!}
                            </li>
                            <li>
                                <h4>More Tips</h4>
                                {!! html_entity_decode($result->rental_tips) !!}
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="details-bottom-sec details-reviews">
                    <div class="details-left">
                        <h3>Guest Reviews</h3>

                        <div class="stars flexbox">
                            <div class="star-rating">
                                @if(isset($rating->average))
                                @foreach(range(1,5) as $i)
                                @if($rating->average >0)
                                @if($rating->average >0.5)
                                <i class="fa fa-star star-color"></i>
                                @else
                                <i class="fa fa-star-half-o star-color"></i>
                                @endif
                                @else
                                <i class="fa  fa-star-o star-color"></i>
                                @endif
                                <?php $rating--; ?>
                                @endforeach
                                @else
                                @endif
                                <!-- Rating in numbers -->
                                @if(@$rating->average != '')
                                @php echo @$rating->average.'/5.0'; @endphp
                                @else

                                @endif
                                <!-- Reviews -->
                                @if(@$review_count != '')
                                @php echo '(' .$review_count.' reviews)'; @endphp
                                @else

                                @endif

                            </div>
                        </div>

                        @if (Auth::check())
                        @if(Auth::user()->id != $result->user_id)
                        <div class="review-btn">
                            <a href="{{ route('writeReview',  $result->slug) }}">Write a review</a>
                        </div>
                        @endif
                        @else
                        <div class="review-btn">
                            <a class="review-btn" href="/login/" ?>Kindly login to continue</a>
                        </div>
                        @endif
                    </div>

                    <div class="details-right">
                        <ul>
                            @if(isset($reviews))
                            <?php foreach ($reviews as $review) { ?>
                                <li>
                                    <div class="review-top">
                                        <div class="review-img">
                                            <img src="<?php echo $review->image; ?>" alt="">
                                        </div>

                                        <div class="review-info">
                                            <h4><?php echo $review->display_name; ?> - <span><?php echo FormatDate($review->created, 'd F'); ?></span></h4>
                                            <div class="review-start">

                                                @if(isset($review->rating))
                                                @foreach(range(1,5) as $i)
                                                @if($review->rating >0)
                                                @if($review->rating >0.5)
                                                <i class="fa fa-star star-color"></i>
                                                @else
                                                <i class="fa fa-star-half-o star-color"></i>
                                                @endif
                                                @else
                                                <i class="fa  fa-star-o star-color"></i>
                                                @endif
                                                <?php $rating--; ?>
                                                @endforeach

                                                @endif

                                            </div>
                                        </div>

                                        @if(isset($review->description))
                                        <p>Title: <?php echo $review->title; ?></p>
                                        <p>Description: <?php echo $review->description; ?></p>
                                        @endif
                                    </div>

                                </li>
                            <?php } ?>
                            @endif

                        </ul>
                    </div>
                </div>

                @if( !Auth::check() )

                <div class="details-bottom-stripe">
                    <div class="container flexbox">
                        <div class="review-top">
                            <div class="review-img">
                                <img src="<?php echo $user->image; ?>" alt="">
                            </div>
                            <div class="review-info">
                                <h6>This rental is offered by</h6>
                                <div class="review-start">
                                    <h4><?php echo $user->display_name; ?></h4>
                                </div>
                            </div>
                        </div>

                        <div class="details-bottom-stripe-right">
                            <h3>From <?php echo $result->price; ?></h3>

                            @if (Auth::check() )

                            <button class="submit__button" disabled type="submit">Book Now</button>

                            @else
                            <a href="/login/">Book Now</a>
                            @endif
                        </div>
                    </div>
                </div>
                @else
                @if( Auth::check() && Auth::user()->id != $result->user_id)
                <div class="details-bottom-stripe">
                    <div class="container flexbox">
                        <div class="review-top">
                            <div class="review-img">
                                <img src="<?php echo $user->image; ?>" alt="">
                            </div>
                            <div class="review-info">
                                <h6>This rental is offered by</h6>
                                <div class="review-start">
                                    <h4><?php echo $user->display_name; ?></h4>
                                </div>
                            </div>
                        </div>

                        <div class="details-bottom-stripe-right">
                            <h3>From <?php echo $result->price; ?></h3>

                            @if (Auth::check() )

                            <button class="submit__button" type="submit">Book Now</button>

                            @else
                            <a href="/login/">Book Now</a>
                            @endif
                        </div>
                    </div>
                </div>
                @endif
                @endif


            </form>

        </div>
    </div>
</section>

@endsection

@section('script')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="{{asset('/js/daterangepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/timepicker.js')}}"></script>

<script src="{{asset('/js/sweetalert2.min.js')}}"></script>
@if($result->rental_type == 'daily')
<script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: 'Search',
                "autoApply": true
            },
            minDate: "{{date('Y/m/d', @$result->available_start_date)}}",
            maxDate: "{{date('Y/m/d', @$result->available_end_date)}}",
            startDate: "{{date('Y/m/d',  @$result->available_start_date)}}",
            endDate: "{{date('Y/m/d', @$result->available_end_date)}}"
        });
    });
</script>
@endif
@if($result->rental_type == 'hourly')
<script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            // timePicker: true,
            // timePicker24Hour: true,
            singleDatePicker: true,
            maxSpan: {
                days: 0,
                hour: 23
            },
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: "Search"
            },
            minDate: "{{date('Y/m/d',  @$result->available_start_date)}}",
            maxDate: "{{date('Y/m/d',  @$result->available_end_date)}}",
            startDate: "{{date('Y/m/d',  @$result->available_start_date)}}",
            endDate: "{{date('Y/m/d',  @$result->available_end_date)}}"
        });
    });
</script>
@endif


<script>
    $('input[name="daterange"]').on('change', function() {
        $('.choose-btn').addClass('check_avalability').removeClass('durationLocked');

    })
    $(document).on('click', '.check_avalability', function() {
        var dates = $('.date_range_select').val();
        var start_time = $('#start_time').val();
        var end_time = $('#end_time').val();
        var hours = "{{@$result->hours}}";
        // if(new Date(end_time) - new Date(start_time) > hours){
        //     FailedResponseFromDatabase(['You can  only book this item maximum for '+hours+' hours only.']);
        //     return false;
        // }
        $.ajax({
            url: "{{route('checkavailability')}}",
            method: 'GET',
            data: {
                dates: dates,
                start_time: start_time,
                end_time: end_time,
                item_id: "{{$result->id}}",
                hour: "{{$result->rental_type}}",
                limit: hours
            },
            dataType: 'JSON',
            success: (response) => {
                if (response.status == true) {
                    $('.selected_detail').removeClass('hidden').html(response.amount_detail);
                    DataSuccessInDatabase(response.message);
                    $('.choose-btn').html('Lock These Dates').addClass('lockthis').removeClass('durationLocked check_avalability');
                } else if (response.status == false) {
                    $('.selected_detail').addClass('hidden').html('');
                    FailedResponseFromDatabase(response.message);
                    $('.choose-btn').html('Check Avalability').removeClass('lockthis').addClass('check_avalability');
                }
            }
        })
    })
    $(document).on('click', '.submit__button', function() {
        if ($('.choose-btn').hasClass('durationLocked') == false) {
            alert('Please Select duration and check availability.');
            return false;  
        } 
    })
    $(document).on('click', '.lockthis', function() {
        $('.choose-btn').addClass('durationLocked').removeClass('lockthis');
        DataSuccessInDatabase("Great! The dates are being attached in your booking process");
    })

    function FailedResponseFromDatabase(message) {
        html_error = "";
        $.each(message, function(index, message) {
            html_error += '<p class ="error_message text-left"> <span class="fa fa-times"></span> ' + message + '</p>';
        });
        Swal.fire({
            position: 'top-end',
            html: html_error,
            timer: 10000
        });
    }

    function DataSuccessInDatabase(message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Done',
            html: message,
            confirmButtonText: 'Close',
            timer: 3000,

            timerProgressBar: true,
            onBeforeOpen: () => {
                timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('b')
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval);
            }
        });
    }
</script>
@endsection