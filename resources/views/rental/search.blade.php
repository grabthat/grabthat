@extends('layouts.app')

@section('content')
<section class="inner-page-content">
    <div class="container">
        <div class="search-result-page">
            <div class="search-top flexbox">
                <div class="filter-icon">
                    <img src="/images/filter-icon.png" alt=""> Add Filters
                </div>

                <div class="search-txt">
                    <form action="/search" method="get">
                        <input type="hidden" name="search" value="1">
                        <input type="search" name="key" value="{{ $key }}"
                               placeholder="Subseed Pasley Farmer in New Delhi">
                    </form>
                </div>

                <div class="search-btns">
                    <div class="search-result-sort sel-sty">
                        <label>Sort by</label>
                        <select name="sort" class="my-dropdown">
                            <option value="">-- Any --</option>
                            <option value="new" <?php echo (@$_REQUEST['sort'] == 'new') ? "selected" : ""; ?>>
                                Most Recent
                            </option>
                            <option value="pricelow" <?php echo (@$_REQUEST['sort'] == 'pricelow') ? "selected" : ""; ?>>
                                Low to High
                            </option>
                            <option value="pricehigh" <?php echo (@$_REQUEST['sort'] == 'pricehigh') ? "selected" : ""; ?>>
                                High to Low
                            </option>
                        </select>
                    </div>

                    <div class="show-map">
                        Show On Map
                        <div class="how-map-btn">
                            <i class="fa fa-toggle-off toggle1" aria-hidden="true"></i>
                            <i class="fa fa-toggle-on toggle2" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>

            <h3>{{ $range }}</h3>
        </div>
    </div>
    <div class="search-res">
        <div class="search-map">
            <div id="map" style="height:100%;">Loading map...</div>
        </div>

        <div class="filter-box">
            <div class="filter-top">
                <h3><img src="/images/filter-icon.png" alt=""> Search by Fliters</h3>
                <div class="filter-close">
                    <img src="/images/cross.jpg" alt="">
                </div>
            </div>

            <div class="filter-reset">
                <span class="pointer"><a href="/search?search=1&key={{ $key }}">Reset All</a></span>
            </div>

            <div class="filter-content">
                <form id="filterForm" action="/search" method="get">
                    <input type="hidden" name="search" value="1">
                    <input type="hidden" name="key" value="{{ $key }}">
                    <input type="hidden" name="sort" value="{{ $sort }}">

                    <div class="filter-row">
                        <p>Booking Confirmation</p>

                        <div class="filter-field custom-radio">
                            <label>
                                <input type="radio" name="confirmation" value="2"
                                <?php echo (@$_REQUEST['confirmation'] == 2) ? "checked" : ""; ?>>
                                <span></span> Instant
                            </label>
                            <label>
                                <input type="radio" name="confirmation" value="1"
                                <?php echo (@$_REQUEST['confirmation'] == 1) ? "checked" : ""; ?>>
                                <span></span> Manual
                            </label>
                        </div>
                    </div>

                    <div class="filter-row">
                        <p>Rental Type</p>

                        <div class="filter-field custom-radio">
                            <label>
                                <input type="radio" name="duration" value="1"
                                <?php echo (@$_REQUEST['duration'] == 1) ? "checked" : ""; ?>>
                                <span></span> Hourly
                            </label>
                            <label>
                                <input type="radio" name="duration" value="2"
                                <?php echo (@$_REQUEST['duration'] == 2) ? "checked" : ""; ?>>
                                <span></span> Weekdays
                            </label>
                        </div>
                    </div>

                    <div class="filter-row">
                        <p>Categories</p>

                        <div class="filter-field custom-checkbox">
                            <?php foreach ($categories as $category) { ?>
                            <label><input type="checkbox" name="category[]" value="<?php echo $category->id; ?>"
                                <?php echo (isset($_REQUEST['category']) && in_array($category->id, @$_REQUEST['category'])) ? "checked" : ""; ?>>
                                <span></span> <?php echo $category->name; ?>
                            </label>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="filter-row">
                        <p>Price</p>

                        <div class="filter-field">
                            <div class="filter-price">
                                <input type="range" name="price"
                                       value="<?php echo isset($_REQUEST['price']) ? (int)$_REQUEST['price'] : 0; ?>"
                                       min="0" max="{{ $pricerange->max }}" step="1">
                            </div>
                        </div>
                    </div>

                    <div class="filter-row">
                        <p>Location</p>

                        <div class="filter-field custom-checkbox">
                            <?php foreach ($cities as $row) { ?>
                            <label><input type="checkbox" name="location[]" value="<?php echo $row->city; ?>"
                                <?php echo (isset($_REQUEST['location']) && in_array($row->city, @$_REQUEST['location'])) ? "checked" : ""; ?>>
                                <span></span> <?php echo ucwords($row->city); ?>
                            </label>
                            <?php } ?>
                        </div>
                        <input type="text" class="txt-style">
                    </div>
                </form>
            </div>
        </div>

        <div class="container">
            <div class="search-content">
                <?php echo (count($rentals) == 0) ? "No record(s) found." : ""; ?>

                <?php foreach ($rentals as $rental) { ?>
                <div class="result-box">
                    <div class="result-img">
                        <img src="<?php echo $rental->image; ?>" alt="">
                        <span><i class="fa fa-camera" aria-hidden="true"></i> See Photo</span>
                    </div>

                    <div class="result-info">
                        <h4><?php echo $rental->name; ?></h4>

                        <div class="stars flexbox">
                            <img src="/images/star.png" alt="">
                            <div class="star-rating">
                                4.5/5.0 (19 reviews)
                            </div>
                        </div>

                        <div class="property-st">
                        <span class="property-st-verified">
                            <i class="fa fa-check" aria-hidden="true"></i> Verified Property
                        </span>
                        </div>

                        <div class="result-price"><?php echo $rental->price; ?></div>

                        <p><a href="/rental/<?php echo $rental->slug; ?>">
                                View Details <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </a></p>
                    </div>
                </div>
                <?php } ?>
            </div>

            {!! $rentals->render() !!}
        </div>
    </div>
</section>


<script type="text/javascript">
    var locations = <?php echo $locations; ?>;
    //var locations = [["Agra", "India"]];

    var geocoder, infowindow, map, locationSelect;
    var markers = [];
    var markerCluster;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 2,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();

        markerCluster = new MarkerClusterer(map, markers, {
            imagePath: '/images/markers/m', maxZoom: 15
        });

        for (var i = 0; i < locations.length; i++) {
            makeCallback(i);
        }
    }

    function makeCallback(i) {
        var newLat = locations[i]['lat'];
        var newLng = locations[i]['long'];

        var marker = new google.maps.Marker({
            map: map, position: new google.maps.LatLng(newLat, newLng)
        });

        markers.push(marker);
        markerCluster.addMarker(marker);

        var content = '<div id="iw-container">' +
            '<div class="iw-content">' +
            '<div class="iw-title">' + locations[i]['name'] + '</div>' +
            '<p>' + locations[i]['city'] + ', ' + locations[i]['country'] + '</p>' +
            '<a href="" class="btn btn-sm btn-custom3">View Rental</a>' +
            '</div><div class="iw-image">' +
            '<a href=""><img src="' + locations[i]['image'] + '" alt="" width="100"></a>' +
            '</div></div>';

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
</script>

<script src="/script/markerclusterer.js"></script>
<script async defer src="//maps.googleapis.com/maps/api/js?key=<?php echo env('GOOGLE_MAP_API_KEY'); ?>&callback=initMap"></script>
@endsection
