@extends('layouts.app')

@section('content')
<section class="home-content">
    <div class="container">
        <div class="write-review-page">
            <h1>Submit Your Feedback</h1>
            <p>We are happy that you had taken our services. We hope you had a great time.
                <br>
                With your feedback we will try to improve our services for a better customer experience!
            </p>

            @if(Session::has('success'))
                <p align="center"><b>{{ Session::get('success') }}</b></p>
            @endif

            <div class="write-reviwe-form">
                <form action="" method="post">
                    @csrf

                    <div class="form-row">
                        <label>Tour You Have Taken</label>
                        <input type="text" class="write-txt-style" value="<?php echo $result->name; ?>" readonly
                               disabled>
                    </div>

                    <div class="form-row">
                        <label>Feedback Title</label>
                        <input type="text" name="title" class="write-txt-style">
                    </div>

                    <div class="form-row">
                        <label>Feedback Description</label>
                        <textarea name="description" class="write-txt-style"></textarea>
                    </div>

                    <div class="form-row rating-row">
                        <label>Overall Rating</label>
                        <select name="rating">
                            <?php for($i = 5; $i >= 1; $i--) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-btn">
                        <input type="submit" class="submit-review" value="Submit Review">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
