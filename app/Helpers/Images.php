<?php

namespace App\Helpers;

class Images
{
    public function __construct()
    {
        //
    }

    public function Process($images, $directory)
    {
        if (!function_exists('imagecreatefrompng')) {
            return array('status' => 'error', 'msg' => 'PNG function does not found.');
        }

        if (!function_exists('imagecreatefromjpeg')) {
            return array('status' => 'error', 'msg' => 'JPEG function does not found.');
        }

        if (!function_exists('imagecreatefromgif')) {
            return array('status' => 'error', 'msg' => 'GIF function does not found.');
        }

        $files = array();
        $failed = array();

        $upload_dir = public_path() . '/upload' . $directory . '/';

        if (!is_dir($upload_dir)) {
            mkdir($upload_dir, 0755, true);
        }

        foreach ($images['name'] as $k => $v) {
            $extension = pathinfo($images['name'][$k], PATHINFO_EXTENSION);
            $extension = strtolower($extension);

            if ($this->AllowedExtension($extension)) {
                $tmpFile = md5(uniqid() . time()) . '.' . $extension;

                move_uploaded_file($images['tmp_name'][$k], $upload_dir . $tmpFile);

                $resizeObj = new ImageResize($upload_dir . $tmpFile);

                $resizeObj->resizeImage(200, 150, 'auto');
                $resizeObj->saveImage($upload_dir . 'small_' . $tmpFile);

                $resizeObj->resizeImage(400, 300, 'auto');
                $resizeObj->saveImage($upload_dir . 'medium_' . $tmpFile);

                $resizeObj->resizeImage(800, 600, 'auto');
                $resizeObj->saveImage($upload_dir . 'large_' . $tmpFile);

                $files[] = $tmpFile;
            }
        }

        return array('files' => $files, 'failed' => $failed);
    }

    private function SetFileName($string, $extension = '')
    {
        $string = strtolower($string);
        $string = preg_replace("/[^a-zA-Z0-9\/_.|+ -]/", '', $string);
        $string = preg_replace("/[\/_|+ -]+/", '-', $string);
        $string = trim($string);

        if ($string == "-" || $string == "") {
            $string = 'image.' . $extension;
        }

        $string = basename($string, ".$extension");
        $string = $string . '-' . time() . '.' . $extension;

        return $string;
    }

    private function MakeFileNameClean($string)
    {
        $string = strtolower($string);
        $string = preg_replace("/[^a-zA-Z0-9 _.|+ -]/", '', $string);
        $string = trim($string);

        return $string;
    }

    private function AllowedExtension($extension)
    {
        $allowedExtensions = array('jpg', 'jpeg', 'gif', 'png');

        if (in_array($extension, $allowedExtensions)) {
            return true;
        }

        return false;
    }
}

Class ImageResize
{
    private $image;
    private $width;
    private $height;
    private $imageResized;

    function __construct($fileName)
    {
        $this->image = $this->openImage($fileName);
        $this->width = imagesx($this->image);
        $this->height = imagesy($this->image);
    }

    public function resizeImage($newWidth, $newHeight = 0, $option = 'auto')
    {
        $optionArray = $this->getDimensions($newWidth, $newHeight, $option);
        $optimalWidth = $optionArray['optimalWidth'];
        $optimalHeight = $optionArray['optimalHeight'];

        $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
        imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);

        if ($option == 'crop') {
            $this->crop($optimalWidth, $optimalHeight, $newWidth, $newHeight);
        }
    }

    public function saveImage($savePath, $imageQuality = 100)
    {
        $extension = $this->getExtension($savePath);

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                if (imagetypes() & IMG_JPG) {
                    imagejpeg($this->imageResized, $savePath, $imageQuality);
                    chmod($savePath, 0755);
                }
                break;
            case 'gif':
                if (imagetypes() & IMG_GIF) {
                    imagegif($this->imageResized, $savePath);
                    chmod($savePath, 0755);
                }
                break;
            case 'png':
                $scaleQuality = round(($imageQuality / 100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;
                if (imagetypes() & IMG_PNG) {
                    imagepng($this->imageResized, $savePath, $invertScaleQuality);
                    chmod($savePath, 0755);
                }
                break;
            default:
                break;
        }
        imagedestroy($this->imageResized);
    }

    private function openImage($fileName)
    {
        $extension = $this->getExtension($fileName);

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $img = imagecreatefromjpeg($fileName);
                break;
            case 'gif':
                $img = imagecreatefromgif($fileName);
                break;
            case 'png':
                $img = imagecreatefrompng($fileName);
                break;
            default:
                $img = false;
                break;
        }

        return $img;
    }

    private function getDimensions($newWidth, $newHeight, $option)
    {
        $optimalWidth = '';
        $optimalHeight = '';

        switch ($option) {
            case 'exact':
                $optimalWidth = $newWidth;
                $optimalHeight = $newHeight;
                break;
            case 'portrait':
                $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                $optimalHeight = $newHeight;
                break;
            case 'landscape':
                $optimalWidth = $newWidth;
                $optimalHeight = $this->getSizeByFixedWidth($newWidth);
                break;
            case 'auto':
                $optionArray = $this->getSizeByAuto($newWidth, $newHeight);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                break;
            case 'crop':
                $optionArray = $this->getOptimalCrop($newWidth, $newHeight);
                $optimalWidth = $optionArray['optimalWidth'];
                $optimalHeight = $optionArray['optimalHeight'];
                break;
        }

        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function getSizeByFixedHeight($newHeight)
    {
        $ratio = $this->width / $this->height;
        $newWidth = $newHeight * $ratio;
        return $newWidth;
    }

    private function getSizeByFixedWidth($newWidth)
    {
        $ratio = $this->height / $this->width;
        $newHeight = $newWidth * $ratio;
        return $newHeight;
    }

    private function getSizeByAuto($newWidth, $newHeight)
    {
        if ($this->height < $this->width) {
            $optimalWidth = $newWidth;
            $optimalHeight = $this->getSizeByFixedWidth($newWidth);
        } elseif ($this->height > $this->width) {
            $optimalWidth = $this->getSizeByFixedHeight($newHeight);
            $optimalHeight = $newHeight;
        } else {
            if ($newHeight < $newWidth) {
                $optimalWidth = $newWidth;
                $optimalHeight = $this->getSizeByFixedWidth($newWidth);
            } else if ($newHeight > $newWidth) {
                $optimalWidth = $this->getSizeByFixedHeight($newHeight);
                $optimalHeight = $newHeight;
            } else {
                $optimalWidth = $newWidth;
                $optimalHeight = $newHeight;
            }
        }
        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function getOptimalCrop($newWidth, $newHeight)
    {
        $heightRatio = $this->height / $newHeight;
        $widthRatio = $this->width / $newWidth;

        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        } else {
            $optimalRatio = $widthRatio;
        }

        $optimalHeight = $this->height / $optimalRatio;
        $optimalWidth = $this->width / $optimalRatio;

        return array('optimalWidth' => $optimalWidth, 'optimalHeight' => $optimalHeight);
    }

    private function crop($optimalWidth, $optimalHeight, $newWidth, $newHeight)
    {
        $cropStartX = ($optimalWidth / 2) - ($newWidth / 2);
        $cropStartY = ($optimalHeight / 2) - ($newHeight / 2);
        $crop = $this->imageResized;
        $this->imageResized = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($this->imageResized, $crop, 0, 0, $cropStartX, $cropStartY, $newWidth, $newHeight, $newWidth, $newHeight);
    }

    private function getExtension($fileName)
    {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $extension = strtolower($extension);
        return $extension;
    }
}
