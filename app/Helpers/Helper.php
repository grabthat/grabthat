<?php

use GeoIp2\Database\Reader;

defined('NOCACHE') || define('NOCACHE', rand(0, 99999));
defined('PAGELIMIT') || define('PAGELIMIT', 25);
defined('HOME_CAT_LIMIT') || define('HOME_CAT_LIMIT', 4);
defined('HOME_RENTAL_LIMIT') || define('HOME_RENTAL_LIMIT', 4);
defined('GEO_SESSION_KEY') || define('GEO_SESSION_KEY', 'GEO_SESSION');

if (!function_exists('echopre')) {
    function echopre($array)
    {
        echo '<pre>';
        print_r($array);
        die;
    }
}

if (!function_exists('Slug')) {
    function Slug($string)
    {
        $string = strtolower($string);
        $string = preg_replace("/[^a-zA-Z0-9\/_.|+ -]/", '', $string);
        $string = preg_replace("/[\/_|+ -]+/", '-', $string);

        if ($string == "-") {
            $string = "";
        }

        return $string;
    }
}

if (!function_exists('CleanText')) {
    function CleanText($string)
    {
        $string = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $string);
        $string = preg_replace("/<!--.*?-->/ms", "", $string);
        //$string = strip_tags($string);
        $string = str_replace(array("\n", "\r", "\t", "<br>", "<br />"), '', $string);
        //$string = str_replace("\\", "\\\\", $string);
        //$string = str_replace("'", "\'", $string);
        $string = trim($string);
        return $string;
    }
}

if (!function_exists('StatusControl')) {
    function StatusControl($value = 0, $required = 'required')
    {
        $data = GetStatus();
        return SelectElement($data, 'status', $value, $required);
    }
}

if (!function_exists('GetStatus')) {
    function GetStatus($key = '')
    {
        $data = array(0 => 'Inactive', 1 => 'Active');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}

if (!function_exists('YesNo')) {
    function YesNo($value = 0, $name = '', $required = 'required')
    {
        $data = GetYesNo();
        return SelectElement($data, $name, $value, $required);
    }
}

if (!function_exists('GetYesNo')) {
    function GetYesNo($key = '')
    {
        $data = array(0 => 'No', 1 => 'Yes');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}

if (!function_exists('Gender')) {
    function Gender($value = 0, $required = 'required')
    {
        $data = GetGender();
        return SelectElement($data, 'gender', $value, $required);
    }
}

if (!function_exists('GetGender')) {
    function GetGender($key = 0)
    {
        $data = array('1' => 'Male', '2' => 'Female');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}

if (!function_exists('CompanyRole')) {
    function CompanyRole($value = 0, $required = 'required')
    {
        $data = GetCompanyRole();
        return SelectElement($data, 'company_role', $value, $required);
    }
}

if (!function_exists('GetCompanyRole')) {
    function GetCompanyRole($key = 0)
    {
        $data = array('' => '-- Select --', '1' => 'Employer', '2' => 'Employee');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}

if (!function_exists('RentalType')) {
    function RentalType($value = 0, $required = 'required')
    {
        $data = GetRentalType();
        return SelectElement($data, 'rental-type', $value, $required);
    }
}

if (!function_exists('GetRentalType')) {
    function GetRentalType($key = 0)
    {
        $data = array('' => '-- Select --', '1' => 'Item', '2' => 'Skill');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}


if (!function_exists('createElement')) {
    function createElement($data = array(), $name = '', $value = '', $required = 'required')
    {
        $elm = '<select name="' . $name . '" class="form-control" ' . $required . '>';

        // if (empty($value) && !is_numeric($value)) $value = -1;

        foreach ($data as $k => $v) {
            // dd($v);
            $elm .= '<option value="' . $k . '" ' . (($value == $k) ? "selected" : "") . '>' . $v . '</option>';
        }

        $elm .= '</select>';
        return $elm;
    }
}

if (!function_exists('RentalDuration')) {
    function RentalDuration($value = 0, $required = 'required')
    {
        // dd($value);
        $data = GetRentalDuration();
        return createElement($data, 'rental_type', $value, $required);
    }
}




if (!function_exists('GetRentalDuration')) {
    function GetRentalDuration($key = 0)
    {
        $data = array('' => '-- Select type --', 'hourly' => 'Hourly', 'daily' => 'Daily');
        // dd($data);
        return isset($data[$key]) ? $data[$key] : $data;
    }
}





if (!function_exists('RentalConfirmation')) {
    function RentalConfirmation($value = 0, $required = 'required')
    {
        $data = GetRentalConfirmation();
        return SelectElement($data, 'confirmation', $value, $required);
    }
}

if (!function_exists('GetRentalConfirmation')) {
    function GetRentalConfirmation($key = 0)
    {
        $data = array('' => '-- Select --', '1' => 'Need Approval', '2' => 'Instant booking');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}

if (!function_exists('RentalAvailability')) {
    function RentalAvailability($value = 0, $required = 'required')
    {
        $data = GetRentalAvailability();
        return SelectElement($data, 'availability', $value, $required);
    }
}

if (!function_exists('GetRentalAvailability')) {
    function GetRentalAvailability($key = 0)
    {
        $data = array('1' => 'Select Dates', '2' => 'Temporarily Suspended', '3' => 'Permanently Closed');
        return isset($data[$key]) ? $data[$key] : $data;
    }
}

if (!function_exists('SelectElement')) {
    function SelectElement($data = array(), $name = '', $value = '', $required = 'required')
    {
        $elm = '<select name="' . $name . '" class="form-control" ' . $required . '>';

        if (empty($value) && !is_numeric($value)) $value = -1;

        foreach ($data as $k => $v) {
            $elm .= '<option value="' . $k . '" ' . (($value == $k) ? "selected" : "") . '>' . $v . '</option>';
        }

        $elm .= '</select>';
        return $elm;
    }
}

if (!function_exists('RadioElement')) {
    function RadioElement($data = array(), $name = '', $value = 0, $required = 'required')
    {
        $elm = '';

        foreach ($data as $k => $v) {
            $control = $name . $k;

            $elm .= '<div class="custom-control custom-radio custom-control-inline">';
            $elm .= '<input type="radio" name="' . $name . '" class="custom-control-input" id="' . $control . '" value="' . $k . '" ' . (($k == $value) ? "checked" : "") . '>';
            $elm .= '<label class="custom-control-label" for="' . $control . '">' . $v . '</label>';
            $elm .= '</div>';
        }

        return $elm;
    }
}

if (!function_exists('RadioElementFront')) {
    function RadioElementFront($data = array(), $name = '', $value = 0, $required = 'required')
    {
        $elm = '<div class="profile-row custom-radio">';

        foreach ($data as $k => $v) {
            $elm .= '<label><input type="radio" name="' . $name . '" value="' . $k . '" ' . (($k == $value) ? "checked" : "") . '><span></span> ' . $v . '</label>';
        }

        $elm .= '</div>';
        return $elm;
    }
}

if (!function_exists('GetRenalPrice')) {
    function GetRenalPrice($duration, $price, $price_only = false)
    {
        $checkDuration ="";
        if ($duration == 2) {
            $day = date('N', time());
            $price = json_decode($price, true);
            $price = isset($price[$day]) ? $price[$day] : '';
            $checkDuration =" /Night/Person";
        }else if($duration == 1){
            $checkDuration =" /Hour/Person";
        }

        if ($price_only) {
            return $price ? $price : '';
        }

        return $price ? '&#8377; ' . $price .$checkDuration: '';
    }
}

if (!function_exists('FormatDate')) {
    function FormatDate($date, $format = 'd M Y')
    {
        return date($format, strtotime($date));
    }
}

if (!function_exists('SetCategoryArray')) {
    function SetCategoryArray($categories)
    {
        $category = array(
            'category' => array(), 'parent' => array()
        );

        foreach ($categories as $row) {
            $category['category'][$row['id']] = $row;
            $category['parent'][(int)$row['parent']][] = $row['id'];
        }

        return $category;
    }
}

$categoryList = array();

if (!function_exists('GetCategoryArray')) {
    function GetCategoryArray($parent, $category, $seperator = '', $slug = '')
    {
        global $categoryList;

        if (isset($category['parent'][$parent])) {
            foreach ($category['parent'][$parent] as $cat_id) {
                if (!isset($category['parent'][$cat_id])) {
                    $categoryList[] = array(
                        'id' => $cat_id,
                        'slug' => $slug . $category['category'][$cat_id]['slug'],
                        'name' => $seperator . $category['category'][$cat_id]['name'],
                        'on_homepage' => $category['category'][$cat_id]['on_homepage'],
                        'image' => $category['category'][$cat_id]['image'],
                        'status' => $category['category'][$cat_id]['status']
                    );
                }

                if (isset($category['parent'][$cat_id])) {
                    $categoryList[] = array(
                        'id' => $cat_id,
                        'slug' => $slug . $category['category'][$cat_id]['slug'],
                        'name' => $seperator . $category['category'][$cat_id]['name'],
                        'on_homepage' => $category['category'][$cat_id]['on_homepage'],
                        'image' => $category['category'][$cat_id]['image'],
                        'status' => $category['category'][$cat_id]['status']
                    );

                    GetCategoryArray($cat_id, $category, $seperator . $category['category'][$cat_id]['name'] . ' &ndash; ', $slug . $category['category'][$cat_id]['slug'] . '/');
                }
            }
        }

        return $categoryList;
    }
}

if (!function_exists('GetCategoryImage')) {
    function GetCategoryImage($image = '', $size = 'small')
    {
        if ($image == '') {
            return '/images/categories-img.jpg';
            //return '/images/category.png';
        }

        return '/upload/category/' . $size . '_' . $image;
    }
}

if (!function_exists('GetRentalImage')) {
    function GetRentalImage($image = '', $id = 0, $size = 'small')
    {
        if ($image == '') {
            return '/images/featured-rentals-img.jpg';
        }

        return '/upload/rental/' . $id . '/' . $size . '_' . $image;
    }
}

if (!function_exists('GetUserImage')) {
    function GetUserImage($image = '', $size = 'small')
    {
        if ($image == '') {
            return '/images/user.png';
        }

        return '/upload/user/' . $size . '_' . $image;
    }
}

if (!function_exists('GetAge')) {
    function GetAge($date = '')
    {
        if ($date == '') {
            return '';
        }

        return floor((time() - strtotime($date)) / 31556926) . ', ';
    }
}

if (!function_exists('DelFolder')) {
    function DelFolder($directory = '')
    {
        $dirname = public_path() . '/upload' . $directory;

        if (is_dir($dirname)) {
            array_map('unlink', glob("$dirname/*.*"));
            rmdir($dirname);
        }

        return;
    }
}

if (!function_exists('DelImage')) {
    function DelImage($directory, $image = '')
    {
        $dirname = public_path() . '/upload' . $directory;
        array_map('unlink', glob("$dirname/*$image"));

        return;
    }
}

if (!function_exists('GetGeoLocation')) {
    function GetGeoLocation($ipaddress = '')
    {
        if ($ipaddress == '') $ipaddress = $_SERVER['REMOTE_ADDR'];

        $db = '/usr/share/GeoIP/GeoLite2-City.mmdb';

        if (!file_exists($db)) return array();

        include public_path() . '/geoip2.phar';

        $return = array();

        try {
            $reader = new Reader($db);
            $record = $reader->city($ipaddress);

            $return['ip_address'] = $ipaddress;
            $return['country_code'] = $record->country->isoCode;
            $return['country'] = $record->country->name;
            $return['city'] = $record->city->name;
            $return['postal'] = $record->postal->code;
            $return['latitude'] = $record->location->latitude;
            $return['longitude'] = $record->location->longitude;
        } catch (Exception $ex) {
            echopre($ex->getMessage());
        }

        return $return;
    }
}

/*
function CountryNameList()
{
    $list = array(1 => 'Afghanistan', 2 => 'Albania', 3 => 'Algeria', 4 => 'Andorra', 5 => 'Angola', 6 => 'Anguilla', 7 => 'Antigua & Barbuda', 8 => 'Argentina', 9 => 'Armenia', 10 => 'Australia', 11 => 'Austria', 12 => 'Azerbaijan', 13 => 'Bahamas', 14 => 'Bahrain', 15 => 'Bangladesh', 16 => 'Barbados', 17 => 'Belarus', 18 => 'Belgium', 19 => 'Belize', 20 => 'Benin', 21 => 'Bermuda', 22 => 'Bhutan', 23 => 'Bolivia', 24 => 'Bosnia & Herzegovina', 25 => 'Botswana', 26 => 'Brazil', 27 => 'Brunei Darussalam', 28 => 'Bulgaria', 29 => 'Burkina Faso', 30 => 'Myanmar/Burma', 31 => 'Burundi', 32 => 'Cambodia', 33 => 'Cameroon', 34 => 'Canada', 35 => 'Cape Verde', 36 => 'Cayman Islands', 37 => 'Central African Republic', 38 => 'Chad', 39 => 'Chile', 40 => 'China', 41 => 'Colombia', 42 => 'Comoros', 43 => 'Congo', 44 => 'Costa Rica', 45 => 'Croatia', 46 => 'Cuba', 47 => 'Cyprus', 48 => 'Czech Republic', 49 => 'Democratic Republic of the Congo', 50 => 'Denmark', 51 => 'Djibouti', 52 => 'Dominica', 53 => 'Dominican Republic', 54 => 'Ecuador', 55 => 'Egypt', 56 => 'El Salvador', 57 => 'Equatorial Guinea', 58 => 'Eritrea', 59 => 'Estonia', 60 => 'Ethiopia', 61 => 'Fiji', 62 => 'Finland', 63 => 'France', 64 => 'French Guiana', 65 => 'Gabon', 66 => 'Gambia', 67 => 'Georgia', 68 => 'Germany', 69 => 'Ghana', 70 => 'United Kingdom', 71 => 'Greece', 72 => 'Grenada', 73 => 'Guadeloupe', 74 => 'Guatemala', 75 => 'Guinea', 76 => 'Guinea-Bissau', 77 => 'Guyana', 78 => 'Haiti', 79 => 'Honduras', 80 => 'Hungary', 81 => 'Iceland', 82 => 'India', 83 => 'Indonesia', 84 => 'Iran', 85 => 'Iraq', 86 => 'Israel and the Occupied Territories', 87 => 'Italy', 88 => 'Ivory Coast (Cote d\'Ivoire)', 89 => 'Jamaica', 90 => 'Japan', 91 => 'Jordan', 92 => 'Kazakhstan', 93 => 'Kenya', 94 => 'Kosovo', 95 => 'Kuwait', 96 => 'Kyrgyz Republic(Kyrgyzstan)', 97 => 'Laos', 98 => 'Latvia', 99 => 'Lebanon', 100 => 'Lesotho', 101 => 'Liberia', 102 => 'Libya', 103 => 'Liechtenstein', 104 => 'Lithuania', 105 => 'Luxembourg', 106 => 'Republic of Macedonia', 107 => 'Madagascar', 108 => 'Malawi', 109 => 'Malaysia', 110 => 'Maldives', 111 => 'Mali', 112 => 'Malta', 113 => 'Martinique', 114 => 'Mauritania', 115 => 'Mauritius', 116 => 'Mayotte', 117 => 'Mexico', 118 => 'Moldova, Republic of', 119 => 'Monaco', 120 => 'Mongolia', 121 => 'Montenegro', 122 => 'Montserrat', 123 => 'Morocco', 124 => 'Mozambique', 125 => 'Namibia', 126 => 'Nepal', 127 => 'Netherlands', 128 => 'New Zealand', 129 => 'Nicaragua', 130 => 'Niger', 131 => 'Nigeria', 132 => 'Korea, Democratic Republic of(North Korea)', 133 => 'Norway', 134 => 'Oman', 135 => 'Pacific Islands', 136 => 'Pakistan', 137 => 'Panama', 138 => 'Papua New Guinea', 139 => 'Paraguay', 140 => 'Peru', 141 => 'Philippines', 142 => 'Poland', 143 => 'Portugal', 144 => 'Puerto Rico', 145 => 'Qatar', 146 => 'Reunion', 147 => 'Romania', 148 => 'Russian Federation', 149 => 'Rwanda', 150 => 'Saint Kitts and Nevis', 151 => 'Saint Lucia', 152 => 'Saint Vincent\'s & Grenadines', 153 => 'Samoa', 154 => 'Sao Tome and Principe', 155 => 'Saudi Arabia', 156 => 'Senegal', 157 => 'Serbia', 158 => 'Seychelles', 159 => 'Sierra Leone', 160 => 'Singapore', 161 => 'Slovak Republic (Slovakia)', 162 => 'Slovenia', 163 => 'Solomon Islands', 164 => 'Somalia', 165 => 'South Africa', 166 => 'Korea, Republic of (South Korea)', 167 => 'South Sudan', 168 => 'Spain', 169 => 'Sri Lanka', 170 => 'Sudan', 171 => 'Suriname', 172 => 'Swaziland', 173 => 'Sweden', 174 => 'Switzerland', 175 => 'Syria', 176 => 'Tajikistan', 177 => 'Tanzania', 178 => 'Thailand', 179 => 'Timor Leste', 180 => 'Togo', 181 => 'Trinidad & Tobago', 182 => 'Tunisia', 183 => 'Turkey', 184 => 'Turkmenistan', 185 => 'Turks & Caicos Islands', 186 => 'Uganda', 187 => 'Ukraine', 188 => 'United Arab Emirates', 189 => 'United States of America', 190 => 'Uruguay', 191 => 'Uzbekistan', 192 => 'Venezuela', 193 => 'Vietnam', 194 => 'Virgin Islands (UK)', 195 => 'Virgin Islands (US)', 196 => 'Yemen', 197 => 'Zambia', 198 => 'Zimbabwe');
    return $list;
}
*/

if (!function_exists('GetGeoLatAndLong')) {
    function GetGeoLatAndLong($city = '', $country = 0)
    {
        $address = $city . ', ' . $country;
        $address = urlencode($address);

        $url = 'https://maps.google.com/maps/api/geocode/json?key=' . env('GOOGLE_MAP_API_KEY') . '&address=' . $address;

        $result = CurlPost($url, false);
        $result = json_decode($result, true);
        return $result;
        $data = array();

        if ($result['status'] == 'OK') {
            $data['lat'] = $result['results'][0]['geometry']['location']['lat'];
            $data['long'] = $result['results'][0]['geometry']['location']['lng'];
        }

        return $data;
    }
}

if (!function_exists('CurlPost')) {
    function CurlPost($url, $new_thead = true)
    {
        if (!function_exists('curl_version')) {
            die('Curl is not installed');
            return;
        }

        $defaults = array(
            CURLOPT_POST => 0,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0
        );

        if ($new_thead) {
            $defaults[CURLOPT_FORBID_REUSE] = 1;
            $defaults[CURLOPT_TIMEOUT] = 1;
            $defaults[CURLOPT_FRESH_CONNECT] = 1;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $defaults);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}