<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdrawn extends Model
{
    protected $table = 'withdrawns';
    protected $fillable = [
        'user_id', 
        'amount' , 
        'requested_date' , 
        'completed_date' , 
        'withdraw_status',
        'cashout_id',
        'balance_transaction',
        'arrival_date',
        'stripe_bank_id',
        'method',
        'source_type',
        'currency',
        'type'

    ];
}

 