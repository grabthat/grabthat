<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RentalImages extends Model
{
    protected $table = 'rental_images';
    public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
