<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rentals extends Model
{
    protected $table = 'rentals';
    public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


    public function totalbooking(){
    	return $this->hasMany('App\Bookings','rental_id');
    }
    public function rentReview(){
        return $this->hasMany('App\Reviews', 'rental_id', 'id');
    }


    public function FetchRentals($type = '', $city = '')
    {
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d');

        $rentals = Rentals::select('rentals.id', 'rentals.name', 'rentals.slug', 'rentals.duration', 'rentals.price', 'rentals.image', 'rentals.city', 'users.display_name', 'categories.name as category')
            ->leftJoin('categories', 'categories.id', '=', 'rentals.category')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id')
            ->where('rentals.status', 1)
            ->where('draft', 0)
            ->where('rentals.city', $city);

        if ($type == 'popular') {

        }

        if ($type == 'today') {
            $rentals = $rentals
                ->where('rentals.available_start_date', '<=', $start_date)
                ->where('rentals.available_end_date', '>=', $end_date);
        }

        if ($type == 'tomorrow') {
            $tomorrow_date = date('Y-m-d 00:00:00', strtotime('+1 day'));

            $rentals = $rentals
                ->whereRaw('? between rentals.available_start_date and rentals.available_end_date', $tomorrow_date);
                //->where('rentals.available_start_date', '<=', $tomorrow_date)
                //->where('rentals.available_end_date', '>', $tomorrow_date);
        }

        $rentals = $rentals
            ->limit(HOME_RENTAL_LIMIT)
            ->orderBy('rentals.id', 'desc')
            ->get();

        foreach ($rentals as $k => $v) {
            $price = GetRenalPrice($rentals[$k]->duration, $rentals[$k]->price);

            if ($price == '') {
                unset($rentals[$k]);
                continue;
            }

            $rentals[$k]->price = $price;
            $rentals[$k]->image = GetRentalImage($rentals[$k]->image, $rentals[$k]->id, 'large');
        }

        return $rentals;
    }

}
