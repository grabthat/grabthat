<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    protected $table = 'reviews';
    public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
