<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table = 'taxes';
    // public $timestamps = false;

    protected $fillable = [ 'user_id', 'tax', 'service_fee'];
}
