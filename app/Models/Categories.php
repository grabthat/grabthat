<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
    public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
