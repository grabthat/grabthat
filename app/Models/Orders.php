<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    // public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

}
