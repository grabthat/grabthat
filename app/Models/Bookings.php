<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookings extends Model
{
    protected $table = 'bookings';
    // public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function UserInfo(){
        return $this->hasOne('App\User','id', 'user_id');
    }
    public function SellerInfo(){
        return $this->hasOne('App\User','id', 'seller_id');
    }
    public function rentalInfo(){
        return $this->hasOne('App\Rentals','id', 'rental_id');
    }

}
