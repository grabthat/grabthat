<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->group == 99999) {
            return $next($request);
        }else if ($request->user()->group == 88888){
            return redirect()->route('customer');
        } else {
            return redirect()->route('login');
        }
    }
}
