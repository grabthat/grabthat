<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GeoIPLocation
{
    protected $except = [];

    public function handle(Request $request, \Closure $next)
    {
        $ip_address = $_SERVER['REMOTE_ADDR'];

        $value = Session::get(GEO_SESSION_KEY);

        if (empty($value)) {
            $result = GetGeoLocation($ip_address);
            Session::put(GEO_SESSION_KEY, $result);
        } else {
            if (isset($value['ip_address']) && $value['ip_address'] != $ip_address) {
                $result = GetGeoLocation($ip_address);
                Session::put(GEO_SESSION_KEY, $result);
            }
        }

        return $next($request);
    }
}
