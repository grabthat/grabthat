<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\User;
use App\Rentals;
use App\Categories;
use App\RentalImages;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Cookie;

class CheckoutController extends Controller
{
    public function __construct(Rentals $rental, Bookings $booking)
    {
        $this->middleware('auth');
        $this->rental  = $rental;
        $this->booking  = $booking;
    }

    public function index(Request $request)
    {
        $order_id = isset($request->order_id) ? (int) $request->order_id : 0;
        $rental_id = isset($request->rental_id) ? (int) $request->rental_id : 0;
        $booking_date = isset($request->booking_date) ? trim($request->booking_date) : '';
        $total_amount = isset($request->total_amount) ? (int) $request->total_amount : 0;

        $result = Rentals::where('id', $rental_id)->where('status', 1)->where('draft', 0)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }

        $data = array();
        $data['rental_id'] = $rental_id;
        $data['order_id'] = $order_id;
        $data['booking_date'] = $booking_date;
        $data['total_amount'] = $total_amount;
        $data['result'] = $result;

        return view('checkout/index', $data);
    }

    protected function checkout(Request $request)
    {

        $item_info = $this->rental->where(['status' => 1, 'id' => $request->rental_id, 'draft' =>  0])->first();
        

        if (!$item_info) {
            return redirect()->to('/');
        }
        if ($item_info->rental_type == 'hourly') {
            // condtion hourly
            
            
            $date =  $request->start_date;
            
            $full_start_date =  $request->start_date;
            $full_end_date =   $request->end_date ;
            // dd(date('Y-m-d h:i a', $full_start_date));
            $db_start_time = date('H:i:s', $item_info->available_start_date);
            $db_end_time = date('H:i:s', $item_info->available_end_date);

           
            $start_time = date('H:i:s',  $full_start_date);
            $end_time = date('H:i:s',  $full_end_date);
             

             
            
            if ($date < date(time())) {
                $request->session()->flash('error',  'Selected date can not be  past date.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            

            // checking date  invalid to start and end date 
            if ($item_info->available_start_date > $date ||  $item_info->available_end_date < $date) {
                $request->session()->flash('error',  'Service not available on this date.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            
            // echo $start_time . '   <br>'; 
            // echo $db_start_time . '   <br>'; 
            // dd($request->all());
            if ($start_time < $db_start_time) {
                $request->session()->flash('error',  'Start time should be equal or greater than ' . date('h:i a', $item_info->available_start_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            // dd($request->all());
            
            if ($start_time > $db_end_time) {
                $request->session()->flash('error',  'Start time should be lower than ' . date('h:i a', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }

            if ($end_time < $item_info->db_start_time) {
                $request->session()->flash('error',  'End time should be   greater than ' . date('h:i a', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }

            if ($end_time > $db_end_time) {
                $request->session()->flash('error',  'End time should be equal or lower than ' . date('h:i a', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }


            if ($full_start_date >= $full_end_date) {
                $request->session()->flash('error',  'Selected start time can not  be equal or greater than ending time.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            if ($full_end_date <= $full_start_date) {
                $request->session()->flash('error',  'Selected end time can not  be equal or lower than start time.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            $duration  = $limit =  $full_end_date - $full_start_date;
            $db_duration = $item_info->duration * 60 * 60; // converting to second
            if ($limit >   $db_duration || $limit < 3600) {
                $request->session()->flash('error',  'Selected  hours gap should be 1 hour to ' . $item_info->hours . ' hours.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            $duration =  number_format($duration / (60 * 60), 1);
 
            $booking_info = [
                'rental_id' => $item_info->id,
                'order_id' => null,
                'user_id' => $request->user_id,
                'start_date' => $full_start_date,
                'end_date' =>  $full_end_date,
                'total_amount' => $request->total_amount,
                'rental_status' => 'Booking',
                'reference_id' => $request->order_id,
                'tax' => $request->tax,
                'service_fee' => $request->fee
            ];

            $booked_dates = $this->booking
                ->where('rental_id', $item_info->id)
                ->where('start_date', '<=', $full_start_date)
                ->where('end_date', '>=', $full_end_date)
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=',  'Rejected')
                ->get();
                
            if ($booked_dates->count() > 0) {
                $request->session()->flash('error',  'Sorry!, This item is already booked in these dates. Please choose other date and check availibility.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            // dd(date('Y-m-d H:i:s', $booking_info['start_date']));
            // dd(date('Y-m-d H:i:s', $booking_info['end_date']));
        } else {
            // daily
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $one_day = 24*60*60;
            $duration = ($request->end_date -$request->start_date)+$one_day ;
            $db_duration = $item_info->duration *$one_day;
            
            if ($start_date > $item_info->available_end_date) {
                $request->session()->flash('error',  'Selected start date should  be equal or lower than.' . date('Y-m-d', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($start_date < $item_info->available_start_date) {
                $request->session()->flash('error',  'Selected start date should  be equal or greater than.' . date('Y-m-d', $item_info->available_start_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($end_date > $item_info->available_end_date) {
                $request->session()->flash('error',  'Selected end date should  be equal or lower than.' . date('Y-m-d', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($end_date <   $item_info->available_start_date) {
                $request->session()->flash('error',  'Selected end date should  be equal or greater than.' . date('Y-m-d', $item_info->available_start_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
           
            if ($duration > $db_duration || $duration < 1) {
                $request->session()->flash('error',  'Selected  Days gap should be 1 day to ' . $item_info->duration . ' days.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            $duration = $duration /($one_day);
            $booking_info = [
                'rental_id' => $item_info->id,
                'order_id' => null,
                'user_id' => $request->user_id,
                'start_date' => $start_date,
                'end_date' =>  $end_date,
                'total_amount' => $request->total_amount,
                'rental_status' => 'Booking',
                'reference_id' => $request->order_id,
                'tax' => $request->tax,
                'service_fee' => $request->fee
            ];
            $booked_dates = $this->booking
                ->where('rental_id', $item_info->id)
                ->where('start_date', '<=', $start_date)
                ->where('end_date', '>=', $end_date)
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=',  'Rejected')
                ->get();
                
            if ($booked_dates->count() > 0) {
                $request->session()->flash('error',  'Sorry!, This item is already booked in these dates. Please choose other date and check availibility.');
                return redirect()->route('bookNow', $item_info->slug);
            }
        }

        $order_id = isset($request->order_id) ?  $request->order_id : 0;

        $rental_id = isset($request->rental_id) ?   $request->rental_id : 0;
        $booking_date = isset($request->booking_date) ? trim($request->booking_date) : '';
        $total_amount = isset($request->total_amount) ?   $request->total_amount : 0;

        $data = [];
        $data['rental_id'] = $rental_id;
        $data['order_id'] = $order_id;
        $data['booking_date'] = $booking_date;
        $data['total_amount'] = $total_amount;
        $data['result'] = $item_info;
       

        return view('checkout/index', $data);
       
    }
}
