<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Rentals;
use App\RentalImages;
use App\Helpers\Images;
use App\Role;
use App\User;
use App\Models\Tax;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AdmincpController extends Controller
{
    protected $userData;

    public function __construct(Tax $tax)
    {
        // $this->middleware('auth');
        $this->tax = $tax;
        // $this->middleware(function ($request, $next) {
        //     $this->userData = Auth::user();
        //     $request->user()->authorizeRoles(['Admin']);
        //     return $next($request);

        // });
    }

    public function index(Request $request)
    {
        $data = array();
        return view('admin/index', $data);
    }




    protected function RemoveUser(Request $request)
    {
        $id = isset($request->id) ? (int) $request->id : 0;
        if ($id > 0) {
            User::destroy($id);
            return redirect()->route('alluserlist');
        }
    }




    protected function alluserlist(Request $request)
    {


        $action = isset($request->action) ? trim($request->action) : '';
        $id = isset($request->id) ? (int) $request->id : 0;



        $users = User::select('users.id', 'username', 'display_name', 'email', 'verified')
            // ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('group', 88888)
            ->orderBy('users.id', 'desc');



        $users = $users->get();

        $data = array();
        $data['users'] = $users;

        return view('admin.users.users', $data);
    }





    protected function searchUser(Request $request)
    {
        $action = isset($request->action) ? trim($request->action) : '';
        if ($action == 'search') {
            $name = isset($request->name) ? trim($request->name) : '';
            $status = $request->status;
            $users = User::select('users.id', 'username', 'display_name', 'email', 'verified')
                ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_id', '2')
                ->orderBy('users.id', 'desc');
            if ($name != '') {
                $users = $users->where(function ($query) use ($name) {
                    $query->where('username', '=', $name)->orWhere('email', 'like', '%' . $name . '%');
                });
            }

            if ($status != '') {
                $users = $users->where('verified', $status);
            }
        }
        $users = $users->get();

        $data = array();
        $data['users'] = $users;

        return view('admin.users.users', $data);
    }




    // public function users(Request $request)
    //  {
    //     $action = isset($request->action) ? trim($request->action) : '';
    //     $id = isset($request->id) ? (int) $request->id : 0;

    //     if ($action == 'delete' && $id > 0) {
    //         User::destroy($id);

    //         return redirect()->to('/admincp/users');
    //     }

    //     $users = User::select('users.id', 'username', 'display_name', 'email', 'verified')
    //         ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
    //         ->where('role_id', '2')
    //         ->orderBy('users.id', 'desc');

    //     if ($action == 'search') {
    //         $name = isset($request->name) ? trim($request->name) : '';
    //         $status = $request->status;

    //         if ($name != '') {
    //             $users = $users->where(function ($query) use ($name) {
    //                 $query->where('username', '=', $name)->orWhere('email', 'like', '%' . $name . '%');
    //             });
    //         }

    //         if ($status != '') {
    //             $users = $users->where('verified', $status);
    //         }
    //     }

    //     $users = $users->get();

    //     $data = array();
    //     $data['users'] = $users;

    //     return view('admin/users', $data);
    // }
    protected function addNewUser(Request $request)
    {
        return view('admin.users.user');
    }






    protected function storeUser(Request $request)
    {
        $method = $request->method();
        $id = isset($request->id) ? (int) $request->id : 0;
        if ($method == 'POST') {
            $is_unique_email = User::where('email', '=', $request->email);
            $is_unique_user = User::where('username', '=', $request->email);

            if ($id > 0) {
                $is_unique_email->where('id', '<>', $id);
                $is_unique_user->where('id', '<>', $id);
            }

            $totalUnique = (int) $is_unique_email->get()->count() + (int) $is_unique_user->get()->count();

            if ($totalUnique == 0) {
                $modal = new User();

                if ($id > 0) {
                    $modal = User::find($id);
                }

                $modal->first_name = trim($request->first_name);
                $modal->last_name = trim($request->last_name);
                $modal->username = trim($request->username);
                $modal->display_name = $modal->first_name . ' ' . $modal->last_name;
                $modal->email = trim($request->email);
                $modal->phone = trim($request->phone);
                $modal->dob = $request->dob;
                $modal->gender = (int) $request->gender;
                $modal->country = trim($request->country);
                $modal->state = trim($request->state);
                $modal->city = trim($request->city);
                $modal->zip = trim($request->zip);
                $modal->group = 88888;

                $modal->company_role = (int) $request->company_role;
                $modal->company = trim($request->company);
                $modal->designation = trim($request->designation);
                $modal->verified = ($request->verified == 1) ? 1 : 0 ;

                if ($request->password != '') {
                    $modal->password = bcrypt($request->password);
                }

                if ($modal->dob == '') {
                    $modal->dob = NULL;
                }

                if (isset($_FILES['image'])) {
                    $ImageObj = new Images();
                    $files = $ImageObj->Process($_FILES['image'], '/user');

                    if (isset($files['files']) && !empty($files['files'])) {
                        $modal->image = $files['files'][0];
                    }
                }

                try {
                    $modal->save();

                    // if ($id == 0) {
                    //     $user = User::find($modal->id);
                    //     $user->roles()->attach(Role::where('name', 'Guest')->first());
                    // }
                } catch (\Exception $ex) {
                    echopre($ex->getMessage());
                }
            }

            return redirect()->route('alluserlist');
        }
    }

    protected function editUser(Request $request, $id)
    {

        $id = isset($request->id) ? (int) $request->id : 0;
        $data = array();
        if ($id > 0) {
            $result = User::where('id', $id)->first();
            if (!isset($result->id)) {
                return redirect()->to('/admincp/users');
            }
            $result->image = GetUserImage($result->image);

            $data['user'] = $result;
        }
        return view('admin.users.user', $data);
    }

    protected function UserProfie(Request $request, $id)
    {

        $id = isset($request->id) ? (int) $request->id : 0;



        $data = array();

        if ($id > 0) {
            $result = User::where('id', $id)->first();

            if (!isset($result->id)) {
                return redirect()->route('allUserList');
            }

            $result->image = GetUserImage($result->image);

            $data['user'] = $result;
        }


        return view('admin.users.user-view', $data);
    }




    // public function user(Request $request)
    // {
    //     $method = $request->method();

    //     $action = isset($request->action) ? trim($request->action) : '';
    //     $id = isset($request->id) ? (int) $request->id : 0;



    //     $data = array();

    //     if (($action == 'edit' || $action == 'view') && $id > 0) {
    //         $result = User::where('id', $id)->first();

    //         if (!isset($result->id)) {
    //             return redirect()->to('/admincp/users');
    //         }

    //         $result->image = GetUserImage($result->image);

    //         $data['user'] = $result;
    //     }

    //     if ($action == 'view') {
    //         return view('admin/user-view', $data);
    //     }
    // }









    public function AdminremoveRental(Request $request, $id)
    {

        $id = isset($request->id) ? (int) $request->id : 0;

        if ($id > 0) {
            Rentals::destroy($id);
            return redirect()->route('allRentalList');
        }
    }

    public function allRentalList(Request $request)
    {
   
        $id = isset($request->id) ? (int) $request->id : 0;



        $rentals = Rentals::select(
            'rentals.id',
            'rentals.name',
            'rentals.duration',
            'rentals.draft',
            'rentals.confirmation',
            'rentals.status',
            'rentals.rental_type',
            'users.display_name',
            'categories.name as category'
        )
            ->leftJoin('categories', 'categories.id', '=', 'rentals.category')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id');

 
        $rentals = $rentals->orderBy('id', 'DESC')->get();

        $data = array();
        $data['rentals'] = $rentals;

        return view('admin.rental.rentals', $data);
    }




    public function rentals(Request $request)
    {
        $action = isset($request->action) ? trim($request->action) : '';
        $id = isset($request->id) ? (int) $request->id : 0;



        $rentals = Rentals::select('rentals.id', 'rentals.name', 'rentals.duration', 'rentals.draft', 'rentals.confirmation', 'rentals.status', 'users.display_name', 'categories.name as category')
            ->leftJoin('categories', 'categories.id', '=', 'rentals.category')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id');

        if ($action == 'search') {
            $name = isset($request->name) ? trim($request->name) : '';
            $status = $request->status;
            $draft = $request->draft;

            if ($name != '') {
                $rentals = $rentals->where('rentals.name', 'like', '%' . $name . '%');
            }

            if ($status > 0) {
                $rentals = $rentals->where('rentals.status', $status);
            }

            if ($draft != '') {
                $rentals = $rentals->where('rentals.draft', $draft);
            }
        }

        $rentals = $rentals->orderBy('id', 'DESC')->get();

        $data = array();
        $data['rentals'] = $rentals;

        return view('admin/rentals', $data);
    }





    public function addrental(Request $request)
    {
        $data = [];
        $categories = Categories::orderBy('parent', 'ASC')->orderBy('name', 'ASC')->get();
        $categories = SetCategoryArray($categories);
        $categories = GetCategoryArray(0, $categories);
        $data['categories'] = $categories;
        $timestamp = strtotime('next Sunday');
        for ($i = 0; $i < 7; $i++) {
            $days[] = strftime('%A', $timestamp);
            $timestamp = strtotime('+1 day', $timestamp);
        }
        return view('admin.rental.add-rental', $data);
    }






    // public function storeRental(Request $request)
    // {
    //     dd($request);
    // }
    public function updateRental(Request $request)
    {

        $id = isset($request->id) ? (int) $request->id : 0;
        // dd($request->all());

        $this->validate($request, [
            "id" => "nullable|exists:rentals,id",
            "name" => "required|string",
            "category" => "required|numeric|exists:categories,id",

            'rental_type' => 'required|in:daily,hourly',
            "description" => "nullable|string",
            "including" => "nullable|string",
            "excluding" => "nullable|string",
            "availability" => "required|in:1,2,3",
            "available_start_date" => "required|date_format:Y-m-d",
            "available_end_date" => "required|date_format:Y-m-d",

            "address" => "required|string|",
            "country" => "required|string|",
            "state" => "required|string|",
            "city" => "required|string|",
            "zip" => "nullable|string|",
            "rental_tips" => "nullable|string|",
            "guest_requirements" => "nullable|string|",
            "cancellation_policy" => "nullable|string|",
            "meta_keyword" => "nullable|string|",
            "meta_description" => "nullable|string|",
            "status" => "required",
            "image.*" => 'nullable|image|mimes:png,jpg,jpeg|max:2000',
            "images.*" => 'nullable|image|mimes:png,jpg,jpeg|max:1000',
        ]);

        if ($request->rental_type == 'hourly') {
            $this->validate($request, [
                "price_hourly" => 'required|numeric|min:1',
                "hours" => "required|numeric|min:1|max:24",
                "start_hour" => "required",
                "end_hour" => "required",
            ]);
        } else if ($request->rental_type == 'daily') {
            $this->validate($request, [
                "price_daily" => 'required|numeric|min:1',
                "days" => "required|numeric|min:1|max:31",
            ]);
        }


        $modal = new Rentals();

        if ($id > 0) {
            $modal = Rentals::find($id);
        }

        $modal->name = trim($request->name);

        if ($id == 0) {
            $modal->slug = Slug($request->name);
        }

        $modal->category = $request->category;
        $modal->rental_type = ($request->rental_type == 'daily') ? 'daily' : 'hourly';
        // $modal->duration = ($request->duration) ? $request->duration : null;

        $modal->description = htmlentities($request->description);
        $modal->including = htmlentities($request->including);
        $modal->excluding = htmlentities($request->excluding);

        $modal->confirmation = $request->confirmation;

        $modal->availability = $request->availability;
        $modal->available_start_date = strtotime($request->available_start_date . ' ' . $request->start_hour);
        $modal->available_end_date = strtotime($request->available_end_date . ' ' . $request->end_hour);


        $modal->address = trim($request->address);
        $modal->country = trim($request->country);
        $modal->state = trim($request->state);
        $modal->city = trim($request->city);
        $modal->zip = trim($request->zip);

        $modal->rental_tips = htmlentities($request->rental_tips);
        $modal->guest_requirements = htmlentities($request->guest_requirements);
        $modal->cancellation_policy = htmlentities($request->cancellation_policy);

        $modal->meta_keyword = trim($request->meta_keyword);
        $modal->meta_description = trim($request->meta_description);

        $modal->status = $request->status;
        $modal->draft = isset($request->draft) ? 1 : 0;

        if ($id == 0) {
            $modal->user_id = Auth::user()->id;
            $modal->created_at = date('Y-m-d H:i:s', time());
        }

        $modal->updated_at = date('Y-m-d H:i:s', time());



        if ($modal->rental_type == 'hourly') {
            $modal->price = $request->price_hourly;
            $modal->duration = $request->hours;
        }


        if ($modal->rental_type == 'daily') {
            $modal->price = $request->price_daily;
            $modal->duration = $request->days;
        }

        if ($modal->availability == 1) {
            $modal->available_start_date = $modal->available_start_date;
            $modal->available_end_date = $modal->available_end_date;
        } else {
            $modal->available_start_date = NULL;
            $modal->available_end_date = NULL;
        }

        $totalUnique = Rentals::where('slug', 'like', $modal->slug . '%');

        if ($id > 0) {
            $totalUnique->where('id', '<>', $id);
        }

        $totalUnique = $totalUnique->get()->count();

        if ($totalUnique > 0) {
            $modal->slug = $modal->slug . '-' . ($totalUnique);
        }
        // dd($modal);
        try {
            $modal->save();
        } catch (\Exception $ex) {
            echopre($ex->getMessage());
        }

        $id = $modal->id;

        if (isset($_FILES['image'])) {
            $ImageObj = new Images();
            $files = $ImageObj->Process($_FILES['image'], '/rental/' . $id);

            if (isset($files['files']) && !empty($files['files'])) {
                $modal = Rentals::find($id);
                $modal->image = $files['files'][0];
                $modal->save();
            }
        }

        if (isset($_FILES['images'])) {
            $ImageObj = new Images();
            $files = $ImageObj->Process($_FILES['images'], '/rental/' . $id);

            if (isset($files['files']) && !empty($files['files'])) {
                Rentals::find($id);

                $data = array();

                foreach ($files['files'] as $image) {
                    $data[] = array(
                        'rental_id' => $id, 'image' => $image
                    );
                }

                RentalImages::insert($data);
            }
        }

        return redirect()->route('allRentalList');
    }

    public function editRental(Request $request, $id)
    {
        $data = array();
        $id = isset($request->id) ? (int) $request->id : 0;
        if ($id > 0) {
            $result = Rentals::where('id', $id)->first();

            if (!isset($result->id)) {
                return redirect()->to('/admincp/rentals');
            }

            if ($result->image) {
                $result->image = '/upload/rental/' . $result->id . '/small_' . $result->image;
            }

            if ($result->duration == 2) {
                $result->price = json_decode($result->prices, true);
            }

            $images = RentalImages::where('rental_id', $id)->get()->toArray();

            foreach ($images as $k => $v) {
                $images[$k]['image'] = '/upload/rental/' . $id . '/small_' . $images[$k]['image'];
            }

            $data['rental'] = $result;
            $data['images'] = $images;
        }
        $categories = Categories::orderBy('parent', 'ASC')->orderBy('name', 'ASC')->get();

        $categories = SetCategoryArray($categories);
        $categories = GetCategoryArray(0, $categories);

        $data['categories'] = $categories;
        return view('admin.rental.add-rental', $data);
    }

    public function AdminrentalSearch(Request $request)
    {
        $rentals = Rentals::select(
            'rentals.id',
            'rentals.name',
            'rentals.duration',
            'rentals.draft',
            'rentals.confirmation',
            'rentals.status',
            'rentals.rental_type',
            'users.display_name',
            'categories.name as category'
        )
            ->leftJoin('categories', 'categories.id', '=', 'rentals.category')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id');
        $action = isset($request->action) ? trim($request->action) : '';
        if ($action == 'search') {
            $name = isset($request->name) ? trim($request->name) : '';
            $status = $request->status;
            $draft = $request->draft;

            if ($name != '') {
                $rentals = $rentals->where('rentals.name', 'like', '%' . $name . '%');
            }

            if ($status > 0) {
                $rentals = $rentals->where('rentals.status', $status);
            }

            if ($draft != '') {
                $rentals = $rentals->where('rentals.draft', $draft);
            }
        }

        $rentals = $rentals->orderBy('id', 'DESC')->get();

        $data = array();
        $data['rentals'] = $rentals;

        return view('admin.rental.rentals', $data);
    }





    public function updateProfile(Request $request)
    {
        $method = $request->method();

        $id = $user_id = Auth::user()->id;

        if ($method == 'POST') {
            $totalUnique = User::where('email', '=', $request->email);

            if ($id > 0) {
                $totalUnique->where('id', '<>', $id);
            }

            $totalUnique = $totalUnique->get()->count();

            if ($totalUnique == 0) {
                $modal = User::find($id);

                $modal->first_name = trim($request->first_name);
                $modal->last_name = trim($request->last_name);
                $modal->display_name = $modal->first_name . ' ' . $modal->last_name;
                $modal->email = trim($request->email);
                $modal->phone = trim($request->phone);
                $modal->dob = $request->dob;
                $modal->gender = (int) $request->gender;
                $modal->designation = trim($request->designation);
                $modal->country = trim($request->country);
                $modal->state = trim($request->state);
                $modal->city = trim($request->city);
                $modal->zip = trim($request->zip);

                if ($request->password != '') {
                    $modal->password = bcrypt($request->password);
                }

                if ($modal->dob == '') {
                    $modal->dob = NULL;
                }

                if (isset($_FILES['image'])) {
                    $ImageObj = new Images();
                    $files = $ImageObj->Process($_FILES['image'], '/user');

                    if (isset($files['files']) && !empty($files['files'])) {
                        $modal->image = $files['files'][0];
                    }
                }

                try {
                    $modal->save();
                } catch (\Exception $ex) {
                    echopre($ex->getMessage());
                }
            }

            return redirect()->route('adminProfile');
        }
    }









    public function profile(Request $request)
    {
        $method = $request->method();
        $id = $user_id = Auth::user()->id;
        $result = User::where('id', $user_id)->first();

        if (!isset($result->id)) {
            return redirect()->route('admin');
        }

        $data['user'] = $result;
        return view('admin/profile', $data);
    }


    public function addSetting(Request $request)
    {
        $tax_info = $this->tax->first();
        // dd($tax_info);
        return view('admin/setting', compact('tax_info'));
    }





    public function Updatesettings(Request $request)
    {
        $message = [
            'required' => 'The :attribute field can not be  empty.',
            'numeric' => 'The :attribute amount should be only number which auto defines as percentage.',
            'min'   => 'The :attribute  should be eqaul or greater than 1%.',
            'max' => 'The :attribute should be less than 100%.'
        ];
        Validator::make($request->all(), [
            'tax' => 'required|numeric|min:1|max:100',
            'service_fee' => 'required|numeric|min:1|max:100',
        ], $message)->validate();
        $this->tax = $this->tax->first();
        $data = [
            'user_id' => Auth::user()->id,
            'tax' => $request->tax,
            'service_fee' => $request->service_fee
        ];
        if ($this->tax == null) {
            $success = Tax::create([
                'user_id' => Auth::user()->id,
                'tax' => $request->tax,
                'service_fee' => $request->service_fee
            ]);
        } else {
            $this->tax->fill($data);
            $success = $this->tax->save();
        }
        if ($success) {
            $request->session()->flash('success', 'Setting updated successfully');
            return redirect()->route('addSetting');
        } else {
            $request->session()->flash('error', 'Opp!, Sorry there was problem while updating the settings.');
            return redirect()->back();
        }
    }
}
