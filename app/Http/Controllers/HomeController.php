<?php

namespace App\Http\Controllers;

use App\Rentals;
use App\Categories;
use Illuminate\Http\Request;
use Auth;
use Session;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('auth', ['except' => 'index']);
    }

    public function index(Request $request)
    {
        if ($request->user()->group == 99999) {
            
            return redirect()->route('admin');
        }else if ($request->user()->group == 88888){
            // dd($request);
            return redirect()->route('customer');
        }
 
        return redirect()->route('home');
 
        
    }

 

    public function privacyPolicy()
    {

    }

    public function frontTermsCondition()
    {

    }

    
}
