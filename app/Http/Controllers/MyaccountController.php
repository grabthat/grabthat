<?php

namespace App\Http\Controllers;

use App\User;
use App\Rentals;
use App\Categories;
use App\Helpers\Images;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Bookings;
use App\Models\BankDetail;
use App\Models\Withdrawn;
use App\RentalImages;
use App\Review;
use DB;

class MyaccountController extends Controller
{
    public function __construct(Rentals $rental, Bookings $booking, Withdrawn $payout)
    {
        $this->middleware('auth');
        $this->rental = $rental;
        $this->booking = $booking;
        $this->payout = $payout;
    }

    public function index()
    {
    }



    public function FrontUserProfile(Request $request, $username = null)
    {
        // dd($username);
        // if (Auth::user()->username == $username) {
        //     return redirect()->to('/profile');
        // }


        $user_id = Auth::user()->id;

        $result = User::where('id', $user_id)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }

        $result->dob = GetAge($result->dob);
        $result->gender = GetGender($result->gender);
        $result->image = GetUserImage($result->image, 'medium');
        $result->joined = FormatDate($result->created_at, 'M Y');

        if ($result->company) {
            $result->designation = $result->designation . ' at ' . $result->company;
        }

        $data['user'] = $result;

        return view('myaccount/profile', $data);
    }





    public function profile(Request $request, $username = '')
    {
        // dd($username);
        if (Auth::user()->username == $username) {
            return redirect()->to('/profile');
        }

        $user_id = Auth::user()->id;

        $result = User::where('id', $user_id)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }

        $result->dob = GetAge($result->dob);
        $result->gender = GetGender($result->gender);
        $result->image = GetUserImage($result->image, 'medium');
        $result->joined = FormatDate($result->created_at, 'M Y');

        if ($result->company) {
            $result->designation = $result->designation . ' at ' . $result->company;
        }

        $data['user'] = $result;

        return view('myaccount/profile', $data);
    }




    public function updateProfile(Request $request)
    {
        $user_id = Auth::user()->id;
        $method = $request->method();
        $modal = User::find($user_id);

        $modal->first_name = trim($request->first_name);
        $modal->last_name = trim($request->last_name);
        $modal->display_name = $modal->first_name . ' ' . $modal->last_name;
        //$modal->email = trim($request->email);
        $modal->phone = trim($request->phone);
        //$modal->dob = $request->dob;
        $modal->gender = (int) $request->gender;
        $modal->country = trim($request->country);
        $modal->state = trim($request->state);
        $modal->city = trim($request->city);
        $modal->zip = trim($request->zip);

        if ($request->dd && $request->mm && $request->yy) {
            $modal->dob = $request->yy . '-' . $request->mm . '-' . $request->dd;
        }

        $modal->company_role = (int) $request->company_role;
        $modal->company = trim($request->company);
        $modal->designation = trim($request->designation);

        if ($request->password != '') {
            $modal->password = bcrypt($request->password);
        }

        if (isset($_FILES['image'])) {
            $old_image =  Auth::user()->image;
            if ($old_image && !empty($old_image) && file_exists(public_path() . '/upload/user/' . $old_image)) {
                DelImage('/user', $old_image);
            }

            $ImageObj = new Images();
            $files = $ImageObj->Process($_FILES['image'], '/user');

            if (isset($files['files']) && !empty($files['files'])) {
                $modal->image = $files['files'][0];
            }
        }

        try {
            $modal->save();
        } catch (\Exception $ex) {
            echopre($ex->getMessage());
        }
        return redirect()->route('FrontUserProfile');
    }





    public function editprofile(Request $request)
    {
        $user_id = Auth::user()->id;
        $method = $request->method();
        $result = User::where('id', $user_id)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }

        if ($result->dob) {
            $result->dob = explode('-', $result->dob);
        }

        $result->image = GetUserImage($result->image, 'medium');

        $data['user'] = $result;
        $data['year'] = date('Y', time());

        return view('myaccount/edit-profile', $data);
    }













    public function listMyAllRentals(Request $request)
    {
        $user = Auth::user();
        $rentals_host = $this->rental
            ->with(['totalbooking'])
            ->select('id', 'name', 'slug', 'created_at', 'status')
            ->where(['user_id' => $user->id])
            ->get();
        // dd($rentals_host[1]->totalbooking);



        $rentals_guest = Rentals::select(
            'rentals.id',
            'rentals.name',
            'rentals.slug',
            'rentals.created_at',
            'bookings.start_date',
            'bookings.end_date',
            'bookings.total_amount',

            'bookings.rental_status',
            'bookings.reference_id',
            'bookings.refunded',
            'bookings.refund_request',
            'bookings.id as booking_id',
            'bookings.rental_type',
            'users.display_name'
        )
            ->leftJoin('bookings', 'bookings.rental_id', '=', 'rentals.id')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id')
            ->where('bookings.user_id', Auth::user()->id)
            ->orderBy('bookings.id', 'desc')
            ->paginate(5);

        // dd($rentals_guest);

        $data['rentals_host'] = $rentals_host;
        $data['rentals_guest'] = $rentals_guest;

        return view('myaccount/my-rentals', $data);
    }






    protected function myRentalsDetail(Request $request, $id)
    {
        $rental_info = Rentals::find($id);
        // dd($rental_info);
        if (!$rental_info) {
            $request->session()->flash('error', 'Rental Information not found.');
            return redirect()->back();
        }
        $categories = Categories::select('id', 'name')
            ->where('status', 1)->where('parent', '=', NULL)
            ->orderBy('name', 'ASC')->get();

        // $data['categories'] = $categories;
        return view('myaccount/add-rental', compact('rental_info', 'categories'));
    }
    public function UpdateRental(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            "id" => "nullable|exists:rentals,id",
            "name" => "required|string",
            "category" => "required|numeric|exists:categories,id",

            'rental_type' => 'required|in:daily,hourly',
            //"duration" => "required|numeric|in:1,2",
            "description" => "nullable|string",
            "including" => "nullable|string",
            "excluding" => "nullable|string",
            // "availability" => "required|in:1,2,3",
            "available_start_date" => "required|date_format:Y-m-d",
            "available_end_date" => "required|date_format:Y-m-d",

            "address" => "required|string|",
            "country" => "required|string|",
            "state" => "required|string|",
            "city" => "required|string|",
            "zip" => "nullable|string|",
            "rental_tips" => "nullable|string|",
            "guest_requirements" => "nullable|string|",
            "cancellation_policy" => "nullable|string|",
            "meta_keyword" => "nullable|string|",
            "meta_description" => "nullable|string|",
            // "status" => "required",
            "image.*" => 'nullable|image|mimes:png,jpg,jpeg|max:2000',
            "images.*" => 'nullable|image|mimes:png,jpg,jpeg|max:1000',
        ]);


        // dd($request->all());

        if ($request->rental_type == 'hourly') {
            $this->validate($request, [
                "price_hourly" => 'required|numeric|min:1',
                "hours" => "required|numeric|min:1|max:24",
                "start_hour" => "required",
                "end_hour" => "required",
            ]);
        } else if ($request->rental_type == 'daily') {
            $this->validate($request, [
                "price_daily" => 'required|numeric|min:1',
                "days" => "required|numeric|min:1|max:31",
            ]);
        }


        $modal = new Rentals();

        $modal->name = trim($request->name);
        $modal->slug = Slug($request->name);

        $modal->category = $request->category;
        $modal->rental_type = ($request->rental_type == 'daily') ? 'daily' : 'hourly';


        // $modal->rental_type = $request->rental_type;
        // $modal->hours = ($request->hours) ? $request->hours : 24;

        $modal->description = htmlentities($request->description);
        $modal->including = htmlentities($request->including);
        $modal->excluding = htmlentities($request->excluding);

        $modal->confirmation = $request->confirmation;

        $modal->availability = $request->availability;


        $modal->address = trim($request->address);
        $modal->country = trim($request->country);
        $modal->state = trim($request->state);
        $modal->city = trim($request->city);
        $modal->zip = trim($request->zip);

        $modal->rental_tips = htmlentities($request->rental_tips);
        $modal->guest_requirements = htmlentities($request->guest_requirements);
        $modal->cancellation_policy = htmlentities($request->cancellation_policy);

        $modal->meta_keyword = ''; //trim($request->meta_keyword);
        $modal->meta_description = ''; //trim($request->meta_description);

        $modal->status = 1;
        $modal->draft = isset($request->draft) ? 1 : 0;

        $modal->user_id = Auth::user()->id;
        $modal->created_at = date('Y-m-d H:i:s', time());
        $modal->updated_at = date('Y-m-d H:i:s', time());
        if ($modal->rental_type == 'hourly') {
            $modal->duration  = $request->hours;
            $modal->available_start_date =   strtotime($request->available_start_date . ' ' . $request->start_hour);
            $modal->available_end_date =   strtotime($request->available_end_date . ' ' . $request->end_hour);
            $modal->price = $request->price_hourly;
            // $modal->start_hour = $start_hour;
            // $modal->end_hour = $end_hour;
        }
        if ($modal->rental_type == 'daily') {
            $modal->available_start_date = strtotime($request->available_start_date . '00:00:00');
            $modal->available_end_date = strtotime($request->available_end_date . '00:00:00');
            // $modal->hours == NULL;
            $modal->duration  = $request->days;
            $modal->price = $request->price_daily;
        }



        $modal->availability = 1;
        // if ($modal->availability == 1) {
        //     $modal->available_start_date = $modal->available_start_date;
        //     $modal->available_end_date = $modal->available_end_date;
        // } else {
        //     $modal->available_start_date = NULL;
        //     $modal->available_end_date = NULL;
        // }

        $totalUnique = Rentals::where('slug', 'like', $modal->slug . '%');
        $totalUnique = $totalUnique->get()->count();

        if ($totalUnique > 0) {
            $modal->slug = $modal->slug . '-' . ($totalUnique);
        }
        // dd($modal);

        try {
            $modal->save();
        } catch (\Exception $ex) {
            echopre($ex->getMessage());
        }

        $id = $modal->id;

        if (isset($_FILES['image'])) {
            $ImageObj = new Images();
            $files = $ImageObj->Process($_FILES['image'], '/rental/' . $id);

            if (isset($files['files']) && !empty($files['files'])) {
                $modal = Rentals::find($id);
                $modal->image = $files['files'][0];
                $modal->save();
            }
        }

        if (isset($_FILES['images'])) {
            $ImageObj = new Images();
            $files = $ImageObj->Process($_FILES['images'], '/rental/' . $id);

            if (isset($files['files']) && !empty($files['files'])) {
                Rentals::find($id);

                $data = array();

                foreach ($files['files'] as $image) {
                    $data[] = array(
                        'rental_id' => $id, 'image' => $image
                    );
                }
                RentalImages::insert($data);
            }
        }

        return redirect()->route('FrontUserRentals');
    }




    public function addrental(Request $request)
    {
        $bank_verifcation  =  BankDetail::where('seller_id', $request->user()->id)
            ->first();
        if (!$bank_verifcation) {
            $request->session()->flash('error', 'You have not setup your bank detail to get withdraw. Please update your bank detail first.');
            return redirect()->route('myBankDetail');
        }
        if ($bank_verifcation && $bank_verifcation->bank_status == 'non-verified') {
            $request->session()->flash('error', 'You have not setup your complete bank detail. Please update your bank detail first to make transaction.');
            return redirect()->route('myBankDetail');
        }

        $categories = Categories::select('id', 'name')
            ->where('status', 1)->where('parent', '=', NULL)
            ->orderBy('name', 'ASC')->get();
        $data['categories'] = $categories;
        return view('myaccount/add-rental', $data);
    }


    protected function Balance()
    {
        $user = Auth::user();

        $bankdetail  =  BankDetail::where('seller_id', $user->id)->first();
        if (isset($bankdetail) && $bankdetail->bank_status == 'verified') {


            $delivered  = $this->booking
                ->select(
                    'bookings.reference_id',
                    'bookings.id as booking_id',
                    'bookings.seller_id',
                    'bookings.start_date',
                    'bookings.end_date',
                    'bookings.price',
                    'bookings.rental_name',
                    'bookings.booked_duration',
                    'bookings.rental_status',
                    'bookings.created_at',
                    'bookings.amount_transfered',
                    'orders.charge_id',
                    'orders.reference_id as order_linked_id',
                    'bank_details.stripe_account',
                    'bank_details.stripe_bank_account'
                )
                ->leftJoin('orders', 'orders.reference_id', '=', 'bookings.reference_id')
                ->leftJoin('bank_details', 'bank_details.seller_id', '=', 'bookings.seller_id')
                ->where('bookings.seller_id', $user->id)
                ->where('bookings.rental_status', 'Delivered')
                ->where('bookings.created_at', '<', date('Y-m-d H:i:s', strtotime('-7 days')))
                ->where('bookings.amount_transfered', 'no')
                ->where('bookings.start_date', '<=', time())
                ->get();
            // dd($delivered);
            if ($delivered->count()) {
                require_once public_path() . '/stripe-php/init.php';
                \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
                $balance  = \Stripe\Balance::retrieve()->jsonSerialize();
                if ($balance['available'][1] >  $balance['available'][0]) {
                    $currency = 'USD';
                } else {
                    $currency = 'GBP';
                }
                // $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();
                foreach ($delivered as $delivered_data) {
                    // dd($delivered_data->price);
                    if ($currency == 'GBP') {
                        $total_amount = (int) ($delivered_data->price * $delivered_data->booked_duration) * 0.76 * 100;
                    } else if ($currency == 'USD') {
                        $total_amount = (int) ($delivered_data->price * $delivered_data->booked_duration) * 100;
                    }
                    $transfered = \Stripe\Transfer::create([
                        "amount" => $total_amount,
                        "currency" => $currency,
                        "destination" => $delivered_data->stripe_account,
                        "transfer_group" => $delivered_data->reference_id,
                        'description' =>  $delivered_data->rental_name,
                    ]);
                    $success = $transfered->jsonSerialize();
                    if ($transfered) {
                        $booking_update = $this->booking->find($delivered_data->booking_id);
                        $booking_update->amount_transfered =  'yes';
                        $done = $booking_update->save();
                    }
                }
            }
        }
        $withdrawn_cash = $this->payout->select(DB::raw('sum(amount) as old_cash_outh'))
            ->where('user_id', $user->id)
            ->first();
            // dd($withdrawn_cash->old_cash_outh);

        $stripe_transfered = $this->booking
        ->select(DB::raw('sum(total_amount - (((price*booked_duration)/100)*tax_percent * ((price*booked_duration)/100)*service_fee_percent)) as balance'))
            ->where('seller_id', $user->id)
            // ->where('rental_status', 'Delivered')
            ->where('rental_status', '!=', 'Rejected')
            ->where('rental_status', '!=', 'Canceled')
            ->where('rental_status', '!=', 'Pending')
            // ->where('created_at', '<', date('Y-m-d H:i:s', strtotime('-7 days')))
            ->where('amount_transfered', 'no')
            ->where('refunded', '!=', 'yes')
            ->first();
        // dd($stripe_transfered);
        $available_balance  =  $this->booking->select(DB::raw('sum(total_amount - (((price*booked_duration)/100)*tax_percent * ((price*booked_duration)/100)*service_fee_percent)) as balance'))
            ->where('seller_id', $user->id)
            ->where('rental_status', '!=', 'Rejected')
            ->where('rental_status', '!=', 'Canceled')
            ->where('rental_status', '!=', 'Pending')
            ->where('refunded', '!=', 'yes')
            ->first();


        // dd($available_balance);
        return $balance = [
            'available_balance' => $available_balance->balance - $withdrawn_cash->old_cash_outh,
            'withdrawn_cash' => $withdrawn_cash,
            'stripe_transfered' => $stripe_transfered
        ];
    }

    public function mypayments(Request $request)
    {
        $user = Auth::user();


        $balance =  $this->Balance();
        // dd($balance);
        // dd($amount);
        $income = $this->booking
            ->select(
                'id',
                'seller_id',
                'reference_id',
                'rental_id',
                'user_id',
                'total_amount',
                'tax_percent',
                'created_at',
                'service_fee_percent',
                DB::raw('(total_amount - (((price*booked_duration)/100)*tax_percent * ((price*booked_duration)/100)*service_fee_percent)) as rental_income')
            )
            ->where('seller_id', $user->id)
            ->paginate(10);

        $expenses = $this->booking->select(
            'id',
            'seller_id',
            'reference_id',
            'rental_id',
            'user_id',
            'total_amount',
            'tax_percent',
            'created_at',
            'user_id'
        )
            ->where('user_id', $user->id)
            ->paginate(10);
        // dd($expenses);
        $data = $this->booking
            ->where('seller_id', Auth::user()->id)
            ->where('rental_status', '!=', 'Rejected')
            ->where('rental_status', '!=', 'Canceled')
            ->where('rental_status', '!=', 'Pending')
            ->where('refunded', '!=', 'yes')
            ->sum('total_amount');
        // dd($data);

        // $booked_data = $this->orders->where([''])
        return view('myaccount/my-payments', compact('data', 'balance', 'income', 'expenses'));
    }
    // my withdraw funds
    public function myWithdrawFunds(Request $request)
    {

        $data['user'] = '';
        $balance =  $this->Balance();

        return view('myaccount/mywithdrawfunds', compact('data', 'balance'));
    }

    // my withdraw funds
    public function myBankDetails(Request $request)
    {

        $bank_info  =  BankDetail::where('seller_id', $request->user()->id)->first();

        // if (!$bank_verifcation) {
        //     $bank_verifcation = new BankDetail();
        //     require_once public_path() . '/stripe-php/init.php';
        //     \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        //     $getIP = \Request::getClientIp();
        //     $account = \Stripe\Account::create([
        //         'country' => 'US',
        //         'type' => 'custom',
        //         'email' => $request->user()->email,
        //         'tos_acceptance' => [
        //             'date' => time(),
        //             'ip' => $getIP
        //         ],
        //         'requested_capabilities' => ['card_payments', 'transfers'],
        //     ])->jsonSerialize();
        //     // dd($account['id']);
        //     $bank_verifcation->stripe_account = $account['id'];
        //     $bank_verifcation->currency_type = 'USD';
        //     $bank_verifcation->bank_status = 'non-verified';
        //     $bank_verifcation->seller_id = $request->user()->id;
        //     $success = $bank_verifcation->save();

        //     $account = \Stripe\Account::retrieve( $account['id'])->jsonSerialize();
        // }
        // if ($bank_verifcation &&  $bank_verifcation->bank_status == 'non-verified') {
        //     require_once public_path() . '/stripe-php/init.php';
        //     \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        //     $account = \Stripe\Account::retrieve( $bank_verifcation->stripe_account)->jsonSerialize();
        // }
        $data['user'] = '';
        $balance =  $this->Balance();
         
        $personal_detail = User::find($request->user()->id);

        return view('myaccount/bank_details', compact('data',  'bank_info', 'balance',  'personal_detail'));
    }
}
