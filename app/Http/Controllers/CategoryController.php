<?php

namespace App\Http\Controllers;

use App\Rentals;
use App\Categories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Helpers\Images;
use App\Role;
use App\User;
 
use Validator;
 
class CategoryController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function CategorySearch(Request $request)
    {
        $categories = Categories::orderBy('parent', 'ASC')->orderBy('name', 'ASC');
        $name = isset($request->name) ? trim($request->name) : '';
        $status = $request->status;

        if ($name != '') {
            $categories = $categories->where('name', 'like', '%' . $name . '%');
        }

        if ($status != '') {
            $categories = $categories->where('status', $status);
        }

        $categories = $categories->get();
        $categories = SetCategoryArray($categories);
        $categories = GetCategoryArray(0, $categories);
        $data = array();
        $data['categories'] = is_array($categories) ? $categories : array();

        return view('admin.category.categories', $data);
    }

    public function StoreCategory(Request $request)
    {

        $modal = new Categories();
        $id = isset($request->id) ? (int) $request->id : 0;
        if ($id > 0) {
            $modal = Categories::find($id);
        }

        $modal->name = $request->name;
        $modal->slug = Slug($request->name);
        $modal->on_homepage = $request->on_homepage;
        $modal->description = $request->description;
        $modal->meta_keyword = $request->meta_keyword;
        $modal->meta_description = $request->meta_description;
        $modal->status = $request->status;

        if ($request->parent > 0) {
            $modal->parent = $request->parent;
        }

        $totalUnique = Categories::where('slug', 'like', $modal->slug . '%');

        if ($id > 0) {
            $totalUnique->where('id', '<>', $id);
        }

        $totalUnique = $totalUnique->get()->count();

        if ($totalUnique > 0) {
            $modal->slug = $modal->slug . '-' . ($totalUnique);
        }
        // dd($modal);
        
        if (isset($_FILES['image'])) {
            $old_image =  $modal->image; 
            if($old_image && !empty($old_image) && file_exists(public_path().'/upload/category/'.$old_image)){
                DelImage('/category', $old_image);
            }

            $ImageObj = new Images();
            $files = $ImageObj->Process($_FILES['image'], '/category');

            if (isset($files['files']) && !empty($files['files'])) {
                $modal->image = $files['files'][0];
            }
        }

        try {
            $modal->save();
        } catch (\Exception $ex) {
            echopre($ex->getMessage());
        }

        return redirect()->route('allCategoryList');
    }





    protected function removeCategory(Request $request){
        
        $id = isset($request->id) ? (int) $request->id : 0;

        if ($id > 0) {
            Categories::destroy($id);
            return redirect()->route('allCategoryList');
        }
    }
    public function allCategoryList(Request $request)
    {
        $categories = Categories::orderBy('parent', 'ASC')->orderBy('name', 'ASC');
        $categories = $categories->get();

        $categories = SetCategoryArray($categories);
        $categories = GetCategoryArray(0, $categories);

        $data = array();
        $data['categories'] = is_array($categories) ? $categories : array();

        return view('admin.category.categories', $data);
    }

    public  function addCategory(Request $request){
        $id = isset($request->id) ? (int) $request->id : 0;
        $data = array();
        $categories = Categories::orderBy('parent', 'ASC')->orderBy('name', 'ASC')->get();
        $categories = SetCategoryArray($categories);
        $categories = GetCategoryArray(0, $categories);
        $data['categories'] = $categories;
        return view('admin.category.category', $data);
    }









    public function editCategory(Request $request, $id)
    {
         

       
        $id = isset($request->id) ? (int) $request->id : 0;

        

        $data = array();

        if (  $id > 0) {
            $result = Categories::where('id', $id)->first();

            if (!isset($result->id)) {
                return redirect()->to('/admincp/categories');
            }

            if ($result->image) {
                $result->image = '/upload/category/small_' . $result->image;
            }

            $data['category'] = $result;
        }

        $categories = Categories::orderBy('parent', 'ASC')->orderBy('name', 'ASC')->get();

        $categories = SetCategoryArray($categories);
        $categories = GetCategoryArray(0, $categories);

        $data['categories'] = $categories;

        return view('admin.category.category', $data);
    }
















    public function view(Request $request, $slug = '')
    {
        $slug = trim($slug);

        if ($slug == '') {
            return redirect()->to('/');
        }

        $result = Categories::where('slug', $slug)->where('status', 1)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }

        $rentals = Rentals::select('rentals.id', 'rentals.name', 'rentals.slug', 'rentals.duration', 'rentals.price', 'rentals.image', 'rentals.city', 'users.display_name')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id')
            ->where('rentals.status', 1)->where('draft', 0)
            ->where('rentals.category', $result->id)
            ->orderBy('rentals.available_start_date', 'asc')
            ->paginate(PAGELIMIT);

        foreach ($rentals as $k => $v) {
            $price = GetRenalPrice($rentals[$k]->duration, $rentals[$k]->price);

            if ($price == '') {
                unset($rentals[$k]);
                continue;
            }

            $rentals[$k]->price = $price;
            $rentals[$k]->image = GetRentalImage($rentals[$k]->image, $rentals[$k]->id, 'large');
        }

        $data['category'] = $result;
        $data['rentals'] = $rentals;

        return view('category/view', $data);
    }
}
