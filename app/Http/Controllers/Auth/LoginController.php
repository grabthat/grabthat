<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\SocialIdentity;
use App\Role;
use App\User;
use Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    

    protected $redirectTo = '/home';
    protected $username;

    public function __construct()
    {
        // dd(Auth::user());
        $this->middleware('guest')->except('logout');
    
        $this->username = $this->findUsername();
    }

    public function findUsername()
    {
        $login = request()->input('email');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldType => $login]);
        return $fieldType;
    }

    public function username()
    {
        return $this->username;
    }

    public function authenticated(Request $request, $user)
    {
        
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        // dd($user);
        if(session()->has('redirecturl')){
            $this->redirectPath = session()->get('redirecturl');
            // dd($this->redirectPath);
            return redirect($this->redirectPath);
        }
        return redirect()->intended($this->redirectPath());
    }

    public function logout(Request $request)
    {
        Auth::logout();
        Session::flush();
        Session::forget(GEO_SESSION_KEY);
        return redirect('/login');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect('/login');
        }

        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($providerUser, $provider)
    {
        $account = SocialIdentity::whereProviderName($provider)->whereProviderId($providerUser->getId())->first();

        if ($account) {
            return $account->user;
        } else {
            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'username' => $this->GenerateUsername($providerUser->getEmail()),
                    'first_name' => $providerUser->getName(),
                    'last_name' => '',
                    'display_name' => $providerUser->getName(),
                    'password' => '',
                    'phone' => '',
                    'verified' => 1,
                    'group' => 88888
                ]);
            }

            // $user->roles()->attach(Role::where('name', 'Guest')->first());

            $user->identities()->create([
                'provider_id' => $providerUser->getId(),
                'provider_name' => $provider,
            ]);

            return $user;
        }
    }

    protected function GenerateUsername($email = '')
    {
        $domainParts = explode("@", $email);
        return $domainParts[0] . "." . rand(1, 9999);
    }

}
