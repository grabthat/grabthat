<?php

namespace App\Http\Controllers;

use App\User;
use App\Rentals;
use App\Orders;
use App\Bookings;
use App\Models\BankDetail;
use App\Models\Tax;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Cookie;

class PaymentController extends Controller
{
    public function __construct(Rentals $rental, Bookings $booking, Tax $tax)
    {
        $this->middleware('auth');
        $this->rental = $rental;
        $this->booking = $booking;
        $this->tax = $tax;
    }

    public function index(Request $request)
    {
        // dd($request->all());
        $order_id = (int) $request->order_id;
        $item_info = $this->rental
            ->where(['id' => (int) $request->rental_id])
            ->where('status', 1)
            ->where('draft', 0)
            ->first();
        if (!$item_info) {
            $request->session()->flash('error', 'Rental Not found.');
            return redirect()->to('/');
        }
        if ($request->order_id) {
            if ($request->rental_type == 'hourly') {
                $date =  $request->start_date;

                $full_start_date =  $request->start_date;
                $full_end_date  =  $request->end_date;
                $db_start_time = date('H:i:s', $item_info->available_start_date);
                $db_end_time = date('H:i:s', $item_info->available_end_date);
                $start_time = date('H:i:s',  $full_start_date);
                $end_time = date('H:i:s',  $full_end_date);
                if ($date < date(time())) {
                    $request->session()->flash('error',  'Selected date can not be  past date.');
                    return redirect()->route('bookNow', $item_info->slug);
                }
                // checking date  invalid to start and end date 
                if ($item_info->available_start_date > $date ||  $item_info->available_end_date < $date) {
                    $request->session()->flash('error',  'Service not available on this date.');
                    return redirect()->route('bookNow', $item_info->slug);
                }
                if ($start_time < $db_start_time) {
                    $request->session()->flash('error',  'Start time should be equal or greater than ' . date('h:i a', $item_info->available_start_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }
                if ($start_time > $db_end_time) {
                    $request->session()->flash('error',  'Start time should be lower than ' . date('h:i a', $item_info->available_end_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }

                if ($end_time < $item_info->db_start_time) {
                    $request->session()->flash('error',  'End time should be   greater than ' . date('h:i a', $item_info->available_end_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }

                if ($end_time > $db_end_time) {
                    $request->session()->flash('error',  'End time should be equal or lower than ' . date('h:i a', $item_info->available_end_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }


                if ($full_start_date >= $full_end_date) {
                    $request->session()->flash('error',  'Selected start time can not  be equal or greater than ending time.');
                    return redirect()->route('bookNow', $item_info->slug);
                }
                if ($full_end_date <= $full_start_date) {
                    $request->session()->flash('error',  'Selected end time can not  be equal or lower than start time.');
                    return redirect()->route('bookNow', $item_info->slug);
                }
                $duration  = $limit =  $full_end_date - $full_start_date;
                $db_duration = $item_info->duration * 60 * 60; // converting to second
                if ($limit >   $db_duration || $limit < 3600) {
                    $request->session()->flash('error',  'Selected  hours gap should be 1 hour to ' . $item_info->hours . ' hours.');
                    return redirect()->route('bookNow', $item_info->slug);
                }
                $duration =  number_format($duration / (60 * 60), 1);
                $booked_dates = $this->booking
                    ->where('rental_id', $item_info->id)
                    ->where('start_date', '<=', $full_start_date)
                    ->where('end_date', '>=', $full_end_date)
                    ->where('rental_status', '!=', 'Canceled')
                    ->where('rental_status', '!=',  'Rejected')
                    ->get();
            } else {
                $start_date = $request->start_date;
                $end_date = $request->end_date;
                $one_day = 24 * 60 * 60;
                $duration = ($request->end_date - $request->start_date) + $one_day;
                $db_duration = $item_info->duration * $one_day;

                if ($start_date > $item_info->available_end_date) {
                    $request->session()->flash('error',  'Selected start date should  be equal or lower than.' . date('Y-m-d', $item_info->available_end_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }

                if ($start_date < $item_info->available_start_date) {
                    $request->session()->flash('error',  'Selected start date should  be equal or greater than.' . date('Y-m-d', $item_info->available_start_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }

                if ($end_date > $item_info->available_end_date) {
                    $request->session()->flash('error',  'Selected end date should  be equal or lower than.' . date('Y-m-d', $item_info->available_end_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }

                if ($end_date <   $item_info->available_start_date) {
                    $request->session()->flash('error',  'Selected end date should  be equal or greater than.' . date('Y-m-d', $item_info->available_start_date));
                    return redirect()->route('bookNow', $item_info->slug);
                }

                if ($duration > $db_duration || $duration < 1) {
                    $request->session()->flash('error',  'Selected  Days gap should be 1 day to ' . $item_info->duration . ' days.');
                    return redirect()->route('bookNow', $item_info->slug);
                }
                $duration = $duration / ($one_day);
                $booked_dates = $this->booking
                    ->where('rental_id', $item_info->id)
                    ->where('start_date', '<=', $start_date)
                    ->where('end_date', '>=', $end_date)
                    ->where('rental_status', '!=', 'Canceled')
                    ->where('rental_status', '!=',  'Rejected')
                    ->where('rental_status', '!=',  'Cancelling')
                    ->get();
            }
            if ($booked_dates->count() > 0) {
                $request->session()->flash('error',  'Sorry!, This item is already booked in these dates. Please choose other date and check availibility.');
                return redirect()->route('bookNow', $item_info->slug);
            }






            // dd('heel');
            $tax_info = $this->tax->first();
            if (!$tax_info) {
                $tax =  0;
                $service_fee =   0;
            } else {
                $tax = ($tax_info->tax) ? $tax_info->tax : 0;
                $service_fee = ($tax_info->service_fee) ? $tax_info->service_fee : 0;
            }

            $total_amount =  $request->duration * $item_info->price;
            $sc_amount  = ($service_fee * $total_amount) / 100;
            $tax_amount  = ($tax * $total_amount) / 100;
            $total_amount = $total_amount + $sc_amount + $tax_amount;


            $this->booking->reference_id  = (int) $request->order_id;
            $this->booking->booked_duration  = $request->duration;
            $this->booking->rental_id  = $item_info->id;
            $this->booking->user_id  = Auth::user()->id;
            $this->booking->seller_id  = $item_info->user_id;
            $this->booking->rental_name = $item_info->name;
            $this->booking->start_date  = (int) $request->start_date;
            $this->booking->end_date = (int) $request->end_date;
            $this->booking->refund_request = 'no';
            $this->booking->refunded = 'no';
            $this->booking->refunded_at = NULL;
            $this->booking->total_amount  = $request->total_amount;
            $this->booking->price  = $item_info->price;
            $this->booking->tax_percent  = $tax;
            $this->booking->tax_amount  = $tax_amount;
            $this->booking->service_fee_percent  = $service_fee;
            $this->booking->fee_amount  = $sc_amount;
            $this->booking->currency_type  = 'USD';
            $this->booking->rental_type  = $request->rental_type;
            $this->booking->rental_status = 'Pending';
            $this->booking->booking_status  = 'Active';
            $this->booking->total_amount  = $total_amount;
            $success = $this->booking->save();
            $payment_id = $statusMsg = '';
            $ordStatus = 'error';
            $itemName = $item_info->name;
            $itemNumber = 1;
            $currency = 'USD';
            $itemPrice = $total_amount;
            // dd($itemPrice*100);
            if (!empty($_POST['stripeToken'])) {
                $token = $_POST['stripeToken'];
                $name = trim($_POST['name']);
                $email = trim($_POST['email']);
                $card_number = $_POST['card_number'];
                $card_exp_month = $_POST['card_exp_month'];
                $card_exp_year = $_POST['card_exp_year'];
                //$card_cvc = $_POST['card_cvc'];

                require_once public_path() . '/stripe-php/init.php';
                \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));

                $orderID = strtoupper(str_replace('.', '', uniqid('', true)));
                $seller_info = BankDetail::where('seller_id', $item_info->user_id)->first();

                try {
                    $customer = \Stripe\Customer::create(array(
                        'email' => $email, 'source' => $token
                    ));

                    // $fee  = $sc_amount + $tax_amount;
                    // // dd($fee);
                    // $sellers_amount  = $total_amount - $fee;

                    // $paymentIntent = \Stripe\PaymentIntent::create([
                    //     'amount' => $total_amount * 100,
                    //     'currency' => 'USD',
                    //     'payment_method_types' => ['card'],
                    //     'transfer_group' => $this->booking->reference_id,
                    // ]);

                    $charge = \Stripe\Charge::create(array(
                        'customer' => $customer->id,
                        'amount' => $total_amount * 100,
                        'currency' => 'USD',
                        'description' => $itemName,
                        'transfer_group' => $this->booking->reference_id,
                        'metadata' => array(
                            'order_id' => $orderID
                        )
                    ));


                    // $charge = \Stripe\Charge::create(array(
                    //     'customer' => $customer->id,
                    //     'amount' => $total_amount * 100,
                    //     'currency' => 'USD',
                    //     'description' => $itemName,
                    //     'transfer_group' => $this->booking->reference_id
                    //     // 'application_fee_amount' => 600,
                    //     // 'metadata' => [
                    //     //     'order_id' => $orderID
                    //     // ],

                    // ));
                    // $transfer = \Stripe\Transfer::create([
                    //     'amount' => $sellers_amount * 100,
                    //     'currency' => 'USD',
                    //     'destination' => $seller_info->stripe_account,
                    //     'transfer_group' => $this->booking->reference_id,
                    // ]);



                    // $charge = \Stripe\Charge::create([
                    //     "amount" => 1000,
                    //     "currency" => "gbp",
                    //     "source" => "tok_visa",
                    //     "application_fee_amount" => 123,
                    //     "transfer_data" => [
                    //       "destination" => "{{CONNECTED_STRIPE_ACCOUNT_ID}}",
                    //     ],
                    //   ]);
                } catch (\Exception $ex) {
                    echopre($ex->getMessage());
                }

                $chargeJson = $charge->jsonSerialize();
                // dd($chargeJson);

                if ($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1) {
                    $success = $this->booking->save();
                    $booking_id = $this->booking
                        ->where('reference_id', (int) $request->order_id)
                        ->where('seller_id', $item_info->user_id)
                        ->first();
                    if (!$success) {
                        return redirect()->to('/');
                    }
                    Cookie::forget('ref_id');

                    $charge_id = $chargeJson['id'];
                    $transactionID = $chargeJson['balance_transaction'];
                    $paidAmount = $chargeJson['amount'] / 100;
                    $paidCurrency = $chargeJson['currency'];
                    $payment_status = $chargeJson['status'];

                    $order_modal = new Orders();
                    $booking_modal = new Bookings();

                    $order_modal->reference_id = $request->order_id;
                    $order_modal->booking_id = $booking_id->id;
                    $order_modal->name = $name;
                    $order_modal->email = $email;

                    $order_modal->card_number = $card_number;
                    $order_modal->card_exp_month = $card_exp_month;
                    $order_modal->card_exp_year = $card_exp_year;

                    $order_modal->rental_name = $itemName;
                    $order_modal->quantity = $itemNumber;

                    $order_modal->user_id  = Auth::user()->id;

                    $order_modal->txn_id = $transactionID;
                    $order_modal->charge_id = $charge_id;
                    $order_modal->payment_status = $payment_status;
                    // $order_modal->created = date('Y-m-d H:i:s', time());

                    try {
                        $order_modal->save();
                    } catch (\Exception $ex) {
                        echopre($ex->getMessage());
                    }

                    $order_id = $order_modal->id;

                    $booking_modal->rental_id = $item_info->id;
                    $booking_modal->order_id = $order_id;
                    $booking_modal->user_id = Auth::user()->id;
                    $booking_modal->total_amount = $total_amount;
                    // $booking_modal->start_date = $booking_date;
                    // $booking_modal->end_date = $total_amount;

                    // try {
                    //     $booking_modal->save();
                    // } catch (\Exception $ex) {
                    //     echopre($ex->getMessage());
                    // }

                    if ($payment_status == 'succeeded') {
                        // $old_coockie = Cookie::get('ref_id');


                        $ordStatus = 'success';
                        $statusMsg = 'Your Payment has been Successful!';
                    } else {
                        $statusMsg = "Your Payment has Failed!";
                    }
                } else {
                    $statusMsg = "Transaction has been failed!";
                }
            } else {
                $statusMsg = "Error on form submission.";
            }

            $data = array();
            $data['order_id'] = $order_id;
            $data['ordStatus'] = $ordStatus;
            $data['statusMsg'] = $statusMsg;
            $data['transactionID'] = $transactionID;
            $data['paidAmount'] = $paidAmount;
            $data['paidCurrency'] = $paidCurrency;
            $data['payment_status'] = $payment_status;
            $data['itemName'] = $itemName;
            $data['itemPrice'] = $itemPrice;
            $data['currency'] = $currency;

            return view('payment/index', $data);
        } else {
            return redirect()->route('homepage');
        }
    }
}
