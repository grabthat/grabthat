<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AjaxController extends Controller
{
    public function __construct()
    { }
    public function unique(Request $request)
    {  
        $type = isset($request->type) ? $request->type : '';
        $value = isset($request->value) ? trim($request->value) : '';
        $id = isset($request->id) ? (int) $request->id : 0;
        if ($type == 'username') {
            $query = User::where('username', '=', $value);
        } elseif ($type == 'email') {
            $query = User::where('email', '=', $value);
        } else {
            die('0');
        }
        if ($id > 0) {

            $query->where('id', '<>', $id);
        }

        $unique = $query->get()->count();
        if ($unique == 0) {
            return 1;
        }
        return 0;
    }
  
    public function updatelocation(Request $request)
    {   $city_name = [];
        if($request->id == 1)
        {  
            if(!empty($request->val))
            {    
               // $city_name = explode(',', $request->val);
                Session::put('loc', $request->val);  
                Session::save();
                return response()->json(['data'=>1]);
            }
            else
            {  
                return response()->json(['data'=>0]);
            }

        }
        else
        {  
            $getIP = $request->getClientIp(); 
            $getlocation = GetGeoLocation($getIP);
            if(!empty($getlocation['city']))
            {
                Session::put('loc', $getlocation['city']);  
                Session::save();
                return response()->json(['data'=>1]);
            }
            else
            {
                return response()->json(['data'=>0]);
            }
        }
    }

}
