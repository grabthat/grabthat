<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\User;
use App\Rentals;
use App\Categories;
// use App\Models\PreBooking;
use App\RentalImages;
use App\Reviews;
use App\Models\Tax;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Cookie;
use Validator;

class RentalController extends Controller
{
    public function __construct(Bookings $booking, Rentals $rental, Tax $tax)
    {
        $this->tax = $tax;
        //$this->middleware('auth');
        $this->booking = $booking;
        $this->rental = $rental;
        // $this->pre_booking = $pre_booking;
    }

    public function index()
    {
        die;

        $data = array();

        return view('rental/index', $data);
    }

    public function rental(Request $request, $slug = '')
    {
        $slug = trim($slug);
        // $redirectUrl = 'rental/review/' . $slug;
        // session()->put('redirecturl', $redirectUrl);
        // session()->save();
        if ($slug == '') {
            return redirect()->route('homepage');
        }

        $result = Rentals::where('slug', $slug)->where('status', 1)->where('draft', 0)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }


        $price = GetRenalPrice($result->duration, $result->price);
        $result->price = $price;

        $today = date('Y-m-d- H:i:s', time());
        //if ($today > $result->available_start_date && $today < $result->available_end_date) {}

        $images = RentalImages::where('rental_id', $result->id)->get();
        foreach ($images as $k => $v) {
            $images[$k]->image = GetRentalImage($images[$k]->image, $result->id, 'large');
        }

        $data = array();
        $data['result'] = $result;
        $data['images'] = $images;

        if (isset($result->user_id)) {
            $user = User::select('id', 'username', 'display_name', 'dob', 'gender', 'designation', 'country', 'state', 'city', 'zip', 'image', 'created_at')
                ->where('id', $result->user_id)->where('verified', 1)->get()->first();

            $user->dob = GetAge($user->dob);
            $user->gender = GetGender($user->gender);
            $user->image = GetUserImage($user->image);

            $data['user'] = $user;
        }

        $data['result']->including = preg_replace('/^(.+)$/', '<li>$1</li>', $data['result']->including);
        $data['result']->including = str_replace(array('<p>', '</p>'), array('<li>', '</li>'), $data['result']->including);
        $data['result']->including = '<ul>' . $data['result']->including . '</ul>';

        $data['result']->excluding = preg_replace('/^(.+)$/', '<li>$1</li>', $data['result']->excluding);
        $data['result']->excluding = str_replace(array('<p>', '</p>'), array('<li>', '</li>'), $data['result']->excluding);
        $data['result']->excluding = '<ul class="rental-exclude">' . $data['result']->excluding . '</ul>';

        $reviews = Reviews::select('title', 'description', 'rating', 'created', 'display_name', 'image')
            ->leftJoin('users', 'users.id', '=', 'reviews.user_id')
            ->where('rental_id', $result->id)->get();
        $reviews_count = Reviews::where('rental_id', $result->id)->count();

        foreach ($reviews as $k => $v) {
            $reviews[$k]->image = GetUserImage($reviews[$k]->image, 'medium');
        }

        $data['reviews'] = $reviews;
        $data['review_count'] = $reviews_count;

        $rating = Reviews::selectRaw('count(1) as total, sum(rating) as rating')->where('rental_id', $result->id)->first();
        $rating->average = ($rating->total > 0) ? $rating->rating / $rating->total : 0;
        $rating->average = number_format($rating->average, 1);
        $data['rating'] = $rating;
        $order_id = time().mt_rand(11111, 99999);
        // dd($order_id);
        
        $data['reference_id'] = $order_id;
        
        // $old_ref_id = Cookie::get('ref_id');
        // if($old_ref_id == null || $old_ref_id == ''){
        //     Cookie::queue('ref_id', $order_id, 10);
        // } else {
        //     $data['reference_id'] = $old_ref_id;
        // }
        // dd($data);
        return view('rental/view', $data);
    }

    public function search(Request $request)
    {
        //$ll = GetGeoLatAndLong('Delhi', 'India');
        //echopre($ll);

        $search = isset($request->search) ? (int) $request->search : 0;
        $key = isset($request->key) ? trim($request->key) : '';
        $sort = isset($request->sort) ? trim($request->sort) : '';

        if ($search == 0 || $key == '') {
            return redirect()->to('/');
        }

        $data = array();
        $locations = array();

        $key = preg_replace('/\s+/', ' ', $key);
        $key = str_replace(' ', '-', $key);

        $data['key'] = $key;
        $data['sort'] = $sort;

        $from_date = date('Y-m-d 00:00:00', strtotime($request->grab));
        $till_date = date('Y-m-d 00:00:00', strtotime($request->return));

        $query = Rentals::where('rentals.status', 1)->where('draft', 0)->where('rentals.slug', 'like', '%' . $key . '%');

        if (isset($request->confirmation)) {
            $query = $query->where('confirmation', (int) $request->confirmation);
        }

        if (isset($request->duration)) {
            $query = $query->where('duration', (int) $request->duration);
        }

        if (isset($from_date)) {
            $query = $query->whereRaw('"' . $from_date . '" between `available_start_date` and `available_end_date`');
        }

        if (isset($till_date)) {
            $query = $query->whereRaw('"' . $till_date . '" between `available_start_date` and `available_end_date`');
        }

        if (isset($request->price) && $request->price > 0) {
            $query = $query->whereBetween('price', array(0, $request->price));
        }

        if (isset($request->category)) {
            $query = $query->whereIn('category', $request->category);
        }

        if (isset($request->location)) {
            $query = $query->whereIn('rentals.city', $request->location);
        }

        $rentals = $query->select('rentals.id', 'rentals.name', 'rentals.slug', 'rentals.duration', 'rentals.price', 'rentals.image', 'rentals.country', 'rentals.city', 'rentals.latitude', 'rentals.longitude', 'users.display_name', 'categories.name as category')
            ->leftJoin('categories', 'categories.id', '=', 'rentals.category')
            ->leftJoin('users', 'users.id', '=', 'rentals.user_id');

        if ($sort == 'new') {
            $rentals = $rentals->orderBy('rentals.id', 'desc');
        } elseif ($sort == 'pricelow') {
            $rentals = $rentals->orderBy('rentals.price', 'asc');
        } elseif ($sort == 'pricehigh') {
            $rentals = $rentals->orderBy('rentals.price', 'desc');
        } else {
            $rentals = $rentals->orderBy('rentals.available_start_date', 'asc');
        }

        $rentals = $rentals->paginate(PAGELIMIT);

        foreach ($rentals as $k => $v) {
            $price = GetRenalPrice($rentals[$k]->duration, $rentals[$k]->price);

            if ($price == '') {
                unset($rentals[$k]);
                continue;
            }

            $rentals[$k]->price = $price;
            $rentals[$k]->image = GetRentalImage($rentals[$k]->image, $rentals[$k]->id, 'large');

            $delta_lat = (mt_rand(0, 100) - 50) / 10000;
            $delta_long = (mt_rand(0, 100) - 50) / 10000;

            $locations[] = array(
                'name' => $rentals[$k]->name,
                'slug' => $rentals[$k]->slug,
                'image' => $rentals[$k]->image,
                'price' => $rentals[$k]->price,
                'country' => $rentals[$k]->country,
                'city' => $rentals[$k]->city,
                'lat' => $rentals[$k]->latitude + $delta_lat,
                'long' => $rentals[$k]->longitude + $delta_long,
            );
        }

        $data['rentals'] = $rentals;
        $data['locations'] = json_encode($locations);

        // Pagination
        $range_start = ($rentals->currentPage() - 1) * ($rentals->perpage() + 1);
        if ($range_start == 0) $range_start = 1;

        $range_end = ($range_start - 1) + $rentals->perpage();
        if ($range_end > $rentals->total()) $range_end = $rentals->total();

        $range = 'Showing ' . $range_start . ' - ' . $range_end . ' of ' . $rentals->total() . ' Searched Results';

        $data['range'] = $rentals->total() > 0 ? $range : '';

        // Categories
        $categories = Categories::select('id', 'name', 'slug')
            ->where('status', 1)->where('on_homepage', 1)->where('parent', '=', NULL)
            ->orderBy('name', 'ASC')->get();

        $data['categories'] = $categories;

        // Price Range
        $priceRange = Rentals::selectRaw('MAX(price) as max, MIN(price) as min')
            ->where('rentals.status', 1)
            ->where('draft', 0)
            //->where('rentals.slug', 'like', '%' . $key . '%')
            ->first();

        $data['pricerange'] = $priceRange;

        $cities = Rentals::selectRaw('DISTINCT(city) as city')
            ->where('rentals.status', 1)
            ->where('draft', 0)
            //->where('rentals.slug', 'like', '%' . $key . '%')
            ->get();

        $data['cities'] = $cities;

        return view('rental/search', $data);
    }

    public function review(Request $request, $slug = '')
    {
        if (session::has('redirecturl')) {
            session()->put('redirecturl', '');
            session()->save();
        }
        if (!Auth::check()) {
            return redirect()->to('/');
        }

        $slug = trim($slug);

        if ($slug == '') {
            return redirect()->to('/');
        }

        $result = Rentals::where('slug', $slug)->where('status', 1)->where('draft', 0)->first();

        if (!isset($result->id)) {
            return redirect()->to('/');
        }

        $method = $request->method();
        if ($method == 'POST') {
            $modal = new Reviews();

            $modal->rental_id = $result->id;
            $modal->user_id = Auth::user()->id;
            $modal->title = isset($request->title) ? trim($request->title) : '';
            $modal->description = isset($request->description) ? trim($request->description) : '';
            $modal->rating = isset($request->rating) ? (int) $request->rating : 5;
            $modal->created = date('Y-m-d H:i:s', time());

            try {
                $modal->save();
            } catch (\Exception $ex) {
                echopre($ex->getMessage());
            }

            return redirect()
                ->to('/rental/review/' . $result->slug)
                ->with('success', 'Review has been successfully added');
        }

        $data = array();
        $data['result'] = $result;

        return view('rental/review', $data);
    }








    public function booking(Request $request, $slug)
    {
        if (!Auth::check()) {
            return redirect()->to('/');
        }
        // if($request->method() == 'GET'){
        //     return redirect()->route('rentalDetail', $slug);
        // }

        $item_info = $this->rental->where(['status' => 1, 'slug' => $slug, 'draft' =>  0])->first();
        if (!$item_info) {
            return redirect()->to('/');
        }
        $order_id = $request->reference_id;
        $booking_detail = [];
        $tax_info = $this->tax->first();
        $tax  = 0;
        $service_charge = 0;
        if($tax_info){
            $tax = $tax_info->tax;
            $service_charge = $tax_info->service_fee;
        }
        $extra_charge = $tax + $service_charge;

        if ($request->rental_type == 'hourly') {
         
            $full_start_date =   strtotime($request->daterange . ' ' . $request->start_time);
            $full_end_date =   strtotime($request->daterange . ' ' . $request->end_time);

            $db_start_time = date('H:i:s', $item_info->available_start_date);
            $db_end_time = date('H:i:s', $item_info->available_end_date);


            $start_time = date('H:i:s',  $full_start_date);
            $end_time = date('H:i:s',  $full_end_date);
            
            $date = strtotime(str_replace(' ', '', str_replace('/', '-', $request->daterange)));

       
            

           
           

            if ($date< date(time())) {
                $request->session()->flash('error',  'Selected date can not be  past date.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            

            // checking date  invalid to start and end date 
            if ($item_info->available_start_date > $date ||  $item_info->available_end_date < $date) {
                $request->session()->flash('error',  'Service not available on this date.');
                return redirect()->route('bookNow', $item_info->slug);
            }

          

      

            if ($start_time < $db_start_time) {
                $request->session()->flash('error',  'Start time should be equal or greater than ' . date('h:i a', $item_info->available_start_date));
                return redirect()->route('bookNow', $item_info->slug);
            }


            if ($start_time > $db_end_time) {
                $request->session()->flash('error',  'Start time should be lower than ' . date('h:i a', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }

            if ($end_time < $item_info->db_start_time) {
                $request->session()->flash('error',  'End time should be   greater than ' . date('h:i a', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }

            if ($end_time > $db_end_time) {
                $request->session()->flash('error',  'End time should be equal or lower than ' . date('h:i a', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }


            if ($full_start_date >= $full_end_date) {
                $request->session()->flash('error',  'Selected start time can not  be equal or greater than ending time.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            if ($full_end_date <= $full_start_date) {
                $request->session()->flash('error',  'Selected end time can not  be equal or lower than start time.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            $duration  = $limit =  $full_end_date - $full_start_date;
            $db_duration = $item_info->duration * 60 * 60; // converting to second
            if ($limit >   $db_duration || $limit < 3600) {
                $request->session()->flash('error',  'Selected  hours gap should be 1 hour to ' . $item_info->hours . ' hours.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            $duration =  number_format($duration/(60*60), 1);

            

            $booked_dates = $this->booking
                ->where('rental_id', $item_info->id)
                ->where('start_date', '<=', $full_start_date)
                ->where('end_date', '>=', $full_end_date)
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=',  'Rejected')
                ->get();
                
            if ($booked_dates->count() > 0) {
                $request->session()->flash('error',  'Sorry!, This item is already booked in these dates. Please choose other date and check availibility.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            $total_amount =  $duration * $item_info->price;
            $sc_amount  = ($service_charge * $total_amount) / 100;
            $tax_amount  = ($tax * $total_amount) / 100;
            $booking_detail = [
                'start_date' => $full_start_date,
                'end_date'  => $full_end_date,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'rental_type' => 'hourly',
                'length' => ($limit > 1) ? 'hours' : 'hour',
                'rent_type' => 'hour',
                'duration' => $duration
            ];
            // dd(date('Y-m-d H:i:s' , $booking_detail['start_date']));

            
        } else if($request->rental_type == 'daily') {

            $start_date = strtotime(explode("-", $request->daterange)[0]);
            $end_date = strtotime(explode("-", $request->daterange)[1]);
            $one_day = 24*60*60;
            $duration = ($end_date - $start_date) +$one_day;
            $db_duration = $item_info->duration * $one_day;
          
            if ($start_date > $item_info->available_end_date) {
                $request->session()->flash('error',  'Selected start date should  be equal or lower than.' . date('Y-m-d', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($start_date < $item_info->available_start_date) {
                $request->session()->flash('error',  'Selected start date should  be equal or greater than.' . date('Y-m-d', $item_info->available_start_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($end_date > $item_info->available_end_date) {
                $request->session()->flash('error',  'Selected end date should  be equal or lower than.' . date('Y-m-d', $item_info->available_end_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($end_date < $item_info->available_start_date) {
                $request->session()->flash('error',  'Selected end date should  be equal or greater than.' . date('Y-m-d', $item_info->available_start_date));
                return redirect()->route('bookNow', $item_info->slug);
            }
            
            if ($duration > $db_duration || $duration < 1) {
                $request->session()->flash('error',  'Selected  Days gap should be 1 day to ' . $item_info->hours . ' days.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            

            // $booked_room = $this->room_booking
            // ->where('checkIn_date','<=',$checkOut_date)
            // ->where('checkOut_date','>=',$checkIn_date)
            // ->get()
            // ->toArray();

            $booked_dates = $this->booking
                ->where('rental_id', $item_info->id)
                ->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date)
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=',  'Rejected')
                ->get();
               
            
            if ($booked_dates->count() > 0) {
                $request->session()->flash('error',  'Sorry!, This item is already booked in these dates. Please choose other date and check availibility.');
                return redirect()->route('bookNow', $item_info->slug);
            }
            $duration = $duration/($one_day);
             
            $total_amount =  $duration * $item_info->price;
            $sc_amount  = ($service_charge * $total_amount) / 100;
            $tax_amount  = ($tax * $total_amount) / 100;

            $booking_detail = [
                'start_date' => $start_date,
                'end_date'  => $end_date,
                'rental_type' => 'daily',
                'length' => ($duration > 1) ? 'days' : 'day',
                'rent_type' => 'day',
                'duration' => $duration
            ];
            
        } else {
            return redirect()->route('bookNow', $item_info->slug);
        }



        
        $booking_detail['price'] = $item_info->price;
        $booking_detail['tax_percent'] = $tax;
        $booking_detail['tax_amount'] = $tax_amount;
        $booking_detail['sc_percent'] = $service_charge;
        $booking_detail['sc_amount'] = $sc_amount;
        $booking_detail['total_amount'] = $total_amount;
        $booking_detail['rental_id'] = $item_info->id;
        $booking_detail['seller_id'] = $item_info->user_id;
        $booking_detail['total_cost'] = round(($extra_charge / 100) * $total_amount) + $total_amount;
        // dd($order_id);
        // dd($booking_detail);
        if (isset($item_info->user_id)) {
            $user = User::select(
                'id',
                'display_name',
                'city',
                'state',
                'country',
                'zip',
                'email',
                'phone'
            )->where('id', Auth::user()->id)->get()->first();
        }

        // $temp_data = [
        //     'rental_id'  => $item_info->id,
        //     'user_id'   => $user->id,
        //     'start_date' => $booking_detail['start_date'],
        //     'end_date' => $booking_detail['end_date'],
        //     'start_hour' => ($start_time) ? $start_time : NULL,
        //     'end_hour' => ($end_time) ? $end_time : NULL,
        //     'total_amount' =>$booking_detail['total_cost'],
        //     'rental_status' =>'Booking',
        //     'reference_id' =>$order_id,
        // ];
        $booking_detail['order_id'] = $order_id;
            // dd($booking_detail);
        return view('rental/booking', compact('booking_detail', 'user', 'item_info'));
    }






    protected function checkavailability(Request $request)
    {
        //  dd($request->all());
        $data = $request->except('_token');
        $published_range = Rentals::where('id', $request->item_id)->first();
        if (!$published_range) {
            return response()->json(['status' => false, 'message' => ['Service Not found.']]);
        }
        if ($request->hour == 'hourly') {
            $validation = Validator::make($data, [
                'dates' => 'required|date_format:Y/m/d',
                'start_time' => 'required',
                'end_time' => 'required',
            ]);
            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $message) {
                    $errors[] = $message;
                }
                return response()->json(['status' => false, 'message' => $errors]);
            }
            // dd($request->all());
            $full_start_date =   strtotime($request->dates . ' ' . $request->start_time);
            $full_end_date =   strtotime($request->dates . ' ' . $request->end_time);

            $db_start_time = date('H:i:s', $published_range->available_start_date);
            $db_end_time = date('H:i:s', $published_range->available_end_date);
            $start_time = date('H:i:s',  $full_start_date);
        
            $end_time = date('H:i:s',  $full_end_date);
            $date = strtotime(str_replace(' ', '', str_replace('/', '-', $request->dates)));
            // checking date below today 
            // dd(date(time()));
            if ($date < date(time())) {
                return response()->json(['status' => false, 'message' => ['Selected date can not be  past date.']]);
            }

            
            // checking date  invalid to start and end date 
            if ($published_range->available_start_date > $date ||  $published_range->available_end_date < $date) {
                return response()->json(['status' => false, 'message' => ['Service not available on this date.']]);
            }

            // checkiing start time with lower to start time 
              
            if ($start_time < $db_start_time) {
                return response()->json(['status' => false, 'message' => ['Start time should be equal or greater than ' .date('h:i a', $published_range->available_start_date)]]);
            }
            // checking start time with greater than end time 
            if ($start_time > $db_end_time) {
                return response()->json(['status' => false, 'message' => ['Start time should be lower than ' . date('h:i a', $published_range->available_end_date)]]);
            }
            // checking end time lower than start time
            if ($end_time < $published_range->db_start_time) {
                return response()->json(['status' => false, 'message' => ['End time should be   greater than ' . date('h:i a', $published_range->available_end_date)]]);
            }
            // checking end time greater than end time
            if ($end_time > $db_end_time) {
                return response()->json(['status' => false, 'message' => ['End time should be equal or lower than ' . date('h:i a', $published_range->available_end_date)]]);
            }
            // checking  start date greater than end date
            if ($full_start_date >= $full_end_date) {
                return response()->json(['status' => false, 'message' => ['Selected start time can not  be equal or greater than ending time.']]);
            }
            // checking end date lower than start date
            if ($full_end_date <= $full_start_date) {
                return response()->json(['status' => false, 'message' => ['Selected end time can not  be equal or lower than start time.']]);
            }
 

            $limit = $full_end_date - $full_start_date;
            $db_duration = $published_range->duration * 60 * 60; // converting to second
            // dd($db_duration);


            if ($limit >   $db_duration) {
                return response()->json(['status' => false, 'message' => ['Selected  hours gap should be 1 hour to ' . $published_range->duration . ' hours.']]);
            }

            $limit = ($limit/(60*60));
      

            $booked_dates = $this->booking
                ->where('rental_id', $request->item_id)
                ->where('start_date', '<=', $full_start_date)
                ->where('end_date', '>=', $full_end_date)
                ->where('rental_type' , 'hourly')
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=',  'Rejected')
                ->get();
            $data = [
                'start_date' =>  date('Y-m-d h:i a', $full_start_date),
                'end_date' =>  date('Y-m-d h:i a', $full_end_date),
                'duration' => number_format($limit, 1),
                'duration_type' => ($limit > 1) ? 'hours' : 'hour',
                'cost'  => $published_range->price,
                'time_type' => 'hour'
            ];
            // dd($data);
        } else if ($request->hour == 'daily') {
            // dd($request->all());
            $validation = Validator::make($data, [
                'dates' => 'required',
            ]);

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $message) {
                    $errors[] = $message;
                }
                return response()->json(['status' => false, 'message' => $errors]);
            }

            $start_date = strtotime(explode("-", $request->dates)[0]);
            $end_date = strtotime(explode("-", $request->dates)[1]);

            $one_day = 24*60*60;
            $duration = ($end_date - $start_date) +$one_day;
            $db_duration = $published_range->duration * $one_day;
            
            if ($start_date > $published_range->available_end_date) {
                return response()->json(['status' => false, 'message' => ['Selected start date should  be equal or lower than.' . date('Y-m-d', $published_range->available_end_date)]]);
            }
            if ($start_date < $published_range->available_start_date) {
                return response()->json(['status' => false, 'message' => ['Selected start date should  be equal or greater than.' . date('Y-m-d ', $published_range->available_start_date)]]);
            }



            if ($end_date > $published_range->available_end_date) {
                return response()->json(['status' => false, 'message' => ['Selected end date should  be equal or lower than.' . date('Y-m-d', $published_range->available_end_date)]]);
            }
            if ($end_date < $published_range->available_start_date) {
                return response()->json(['status' => false, 'message' => ['Selected end date should  be equal or greater than.' . date('Y-m-d ', $published_range->available_start_date)]]);
            }
            // dd($start_date);
            // $date_diff = date_diff(date_create($end_date), date_create($start_date));
            
            // dd($db_duration);
            if($duration > $db_duration || $duration < $one_day){
                return response()->json(['status' => false, 'message' => ['Selected  Days gap should be 1 day to ' . $published_range->duration . ' days.']]);
            }
            $duration = $duration/($one_day);
            

            $booked_dates = $this->booking
                ->where('rental_id', $request->item_id)
                ->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date)
                ->where('rental_type' , 'daily')
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=',  'Rejected')
                ->get();

            $data = [
                'start_date' => date('Y-m-d', $start_date),
                'end_date' => date('Y-m-d', $end_date),
                'duration' => $duration,
                'duration_type' => ($duration > 1) ? 'days' : 'day',
                'cost'  => $published_range->price,
                'time_type' => 'day'
            ];
        }

        if ($booked_dates->count() > 0) {
            return response()->json(['status' => false, 'message' => ['Sorry!, This item is already booked in these dates. Please choose other date and check availibility.']]);
        } else {
            return response()->json([
                'status' => true,
                'message' => 'Congratulations!, This item is available in these date.',
                'amount_detail' => view('rental.booking.cart', compact('data'))->render(),
            ]);
        }
    }

    
}
