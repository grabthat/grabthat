<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\Models\BankDetail;
use App\Models\Withdrawn;
use App\User;
use Illuminate\Http\Request;
use STR;
use Validator;
use File;
use Image;
use Auth;
use DB;

class BankDetailController extends Controller
{
    public function updateBankDetailall(Request $request)
    {

        $this->validate($request, [
            "business_type" => "required|in:individual",
            "address_line_1" => "nullable|string",
            "address_line_2" => "nullable|string",
            "account_number" => "nullable|string",
            "account_holder" => "nullable|string",
            "bank_name" => "nullable|string",

            "routing_number" => "nullable|numeric",
        ]);
        $data = $request->all();
        // dd($data['front_side']);
        // dd($data['front_side']->getRealPath());
        $bank_verifcation  =  BankDetail::where('seller_id', $request->user()->id)->first();
        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        if (!$bank_verifcation) {
            $bank_verifcation = new BankDetail();

            $account = \Stripe\Account::create([
                'country' => 'US',
                'type' => 'custom',
                'email' => $request->user()->email,

                'requested_capabilities' => ['card_payments', 'transfers'],
                'tos_acceptance' => [
                    'date' => time(),
                    'ip' => $getIP
                ],
            ])->jsonSerialize();
            // dd($account['id']);
            $bank_verifcation->stripe_account = $account['id'];
            $bank_verifcation->currency_type = $account['id'];
            $bank_verifcation->bank_status = 'non-verified';
            $bank_verifcation->seller_id = $request->user()->id;
            $success = $bank_verifcation->save();
        }
        $account = \Stripe\Account::retrieve($bank_verifcation->stripe_account)->jsonSerialize();

        $first_name  = $request->user()->first_name;
        $last_name  = $request->user()->last_name;
        $getIP = \Request::getClientIp();

        dd($account);
        \Stripe\Account::update(
            $bank_verifcation->stripe_account,
            // ['metadata' => ['order_id' => '6735']]
            [
                'business_type' => 'individual',
                'default_currency' => 'USD',
                'individual' => [
                    // 'id_number' => ($account['individual']['id_number_provided'] == true) ? '000000000' : $account['individual']['id_number'],

                    'address' => [
                        "city" => $request->user()->city,
                        "country" => "US",
                        "line1" =>  $request->address_line_1,
                        "line2" => $request->address_line_1,
                        "postal_code" => $request->postal_code,
                        "state" => $request->user()->state,
                        'postal_code' => '90002'
                    ],
                    'dob' => [
                        'day' => date('d', strtotime($request->user()->dob)),
                        'month' => date('m', strtotime($request->user()->dob)),
                        'year' => date('Y', strtotime($request->user()->dob)),
                    ],
                    'email' => $request->user()->email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $request->user()->phone,
                    // 'ssn_last_4' => ($account['individual']['ssn_last_4'] == null) ? '0000' : $account['individual']['ssn_last_4'],

                ],

                'email' => $request->user()->email,
                'tos_acceptance' => [
                    'date' => time(),
                    'ip' => $getIP
                ],
                'business_profile' => [
                    'mcc' => '8071',
                    'name' => $first_name . ' ' . $last_name,
                    'support_email' => $request->user()->email,
                    'support_phone' => $request->user()->phone,
                    'support_url' => route('homepage'),
                    'url' => route('homepage'),
                ],
            ]
        );
        // if($data['front_side']){

        // \Stripe\File::create([
        //     'file' => $data['front_side']->getRealPath(),
        //     'purpose' => 'identity_document',
        // ]);
        // }

        // dd($account['external_accounts']['data']);
        /// bank accounts
        if (count($account['external_accounts']['data']) < 1) {


            $token = \Stripe\Token::create([
                'bank_account' => [
                    'country' => 'US',
                    'currency' => $request->user()->currency,
                    "account_holder_name" => $request->account_holder,
                    "account_holder_type" => $request->account_type,
                    'account_number' => $request->account_number,
                    'account_holder_type' => 'individual',
                    'routing_number' => '110000000',
                ],
            ]);
            if ($token) {
                \Stripe\Account::createExternalAccount(
                    $bank_verifcation->stripe_account,
                    [
                        'external_account' => $token,
                    ]
                );
            }
        } else {
            // dd($account);
            $bank_account = \Stripe\Account::retrieveExternalAccount(
                $bank_verifcation->stripe_account,
                $account['external_accounts']['data'][0]['id']
            );
            // dd($account['external_accounts']['data'][0]);
            // dd($account);
            if ($bank_account) {
                \Stripe\Account::updateExternalAccount(
                    $bank_verifcation->stripe_account,
                    $account['external_accounts']['data'][0]['id'],
                    [
                        "account_holder_name" => $request->account_holder,
                        "account_holder_type" => $request->account_type,
                    ]
                );
            }
        }
        dd($account);
        // \Stripe\Account::updateCapability(
        //     $bank_verifcation->stripe_account,
        //     'card_payments',
        //     // ['requested' => true],
        //     [
        //         'card_payments' => 'active',
        //         'transfers' => 'active'
        //     ]
        // );

        // 'charges_enabled' => 'true',
        // 'payouts_enabled' => 'false',
        // dd($account['external_accounts']['data']);
        dd($account);
    }

    public function stripePersonalDetail($bank_detail, $type)
    {
        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        $account = \Stripe\Account::retrieve($bank_detail->stripe_account)->jsonSerialize();

        $user_detail = User::find($bank_detail->seller_id);
        $first_name = $user_detail->first_name;
        $last_name = $user_detail->last_name;

        if ($type == 'personal_info') {
            // if($account['requirements'])
            if ($account['individual']['ssn_last_4_provided'] == false) {
                \Stripe\Account::update(
                    $bank_detail->stripe_account,
                    [
                        'individual' => [
                            'id_number' => $bank_detail->social_id_number,
                            'ssn_last_4' => substr($bank_detail->social_id_number, -4),
                        ]
                    ]
                );
            }
            return  \Stripe\Account::update(
                $bank_detail->stripe_account,
                // ['metadata' => ['order_id' => '6735']]
                [
                    'business_type' => 'individual',
                    'default_currency' => 'USD',
                    'individual' => [
                        // 'id_number' => ($account['individual']['id_number_provided'] == true) ? '000000000' : $account['individual']['id_number'],
                        'address' => [
                            "city" => $user_detail->city,
                            "country" => "US",
                            "line1" =>  $bank_detail->address_line_1,
                            "line2" => $bank_detail->address_line_1,
                            "postal_code" => $bank_detail->postal_code,
                            "state" => $user_detail->state,
                            'postal_code' => $bank_detail->postal_code
                        ],
                        'dob' => [
                            'day' => date('d', strtotime($user_detail->dob)),
                            'month' => date('m', strtotime($user_detail->dob)),
                            'year' => date('Y', strtotime($user_detail->dob)),
                        ],
                        'email' => $user_detail->email,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'phone' => $user_detail->phone,
                        // 'ssn_last_4' => ($account['individual']['ssn_last_4'] == null) ? '0000' : $account['individual']['ssn_last_4'],
                    ],
                    'email' => $user_detail->email,
                ]
            );
        } else if ($type == 'business_detail') {
            return \Stripe\Account::update(
                $bank_detail->stripe_account,
                // ['metadata' => ['order_id' => '6735']]
                [
                    'business_profile' => [
                        'mcc' => '8071',
                        'name' => $first_name . ' ' . $last_name,
                        'support_email' => $user_detail->email,
                        'support_phone' => $user_detail->phone,
                        'support_url' => ($bank_detail->support_url) ? $bank_detail->support_url : route('homepage'),
                        'url' => ($bank_detail->support_url) ? $bank_detail->support_url : route('homepage'),
                    ],
                ]
            );
        } else if ($type == 'update_document') {
            // return \Stripe\File::create([
            //     'file' => asset('/uploads/user/document/' . $bank_detail->id_front),
            //     'purpose' => 'identity_document',
            // ]);


            \Stripe\FileUpload::create(
                [
                    'purpose' => 'identity_document',
                    'file' => fopen('/path/to/a/file.jpg', 'r')
                ],
                ['stripe_account' => CONNECTED_STRIPE_ACCOUNT_ID]
            );
        } else if ($type == 'bank_detail') {
            if (count($account['external_accounts']['data']) < 1) {


                $token = \Stripe\Token::create([
                    'bank_account' => [
                        'country' => 'US',
                        'currency' => 'USD',
                        "account_holder_name" => $request->account_holder,
                        "account_holder_type" => $bank_detail->account_type,
                        'account_number' => $bank_detail->account_number,
                        'account_holder_type' => 'individual',
                        // 'routing_number' => $bank_detail->routing_number,
                    ],
                ]);
                if ($token) {
                    \Stripe\Account::createExternalAccount(
                        $bank_detail->stripe_account,
                        [
                            'external_account' => $token,
                        ]
                    );
                }
            } else {
                // dd($account);
                $bank_account = \Stripe\Account::retrieveExternalAccount(
                    $bank_detail->stripe_account,
                    $account['external_accounts']['data'][0]['id']
                );
                // dd($account['external_accounts']['data'][0]);
                // dd($account);
                if ($bank_account) {
                    \Stripe\Account::updateExternalAccount(
                        $bank_detail->stripe_account,
                        $account['external_accounts']['data'][0]['id'],
                        [
                            "account_holder_name" => $bank_detail->account_holder,
                            "account_holder_type" => $bank_detail->account_type,
                        ]
                    );
                }
            }
        }

        dd($account);
    }

    public function createStripeAccount()
    {
        $user = Auth::user();
        $bank_info  =  BankDetail::where('seller_id', $user->id)->first();

        if ($bank_info) {
            require_once public_path() . '/stripe-php/init.php';
            \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));

            if ($bank_info->stripe_account == null || $bank_info->stripe_account == '') {
                $getIP = \Request::getClientIp();
                $account = \Stripe\Account::create([
                    'country' => 'US',
                    'type' => 'custom',
                    'email' => $user->email,

                    'requested_capabilities' => ['card_payments', 'transfers'],
                    'tos_acceptance' => [
                        'date' => time(),
                        'ip' => $getIP
                    ],
                ])->jsonSerialize();
                $bank_info->stripe_account = $account['id'];
                $bank_info->currency_type = 'USD';
            }



            $success = $bank_info->save();
            $bank_info  =  BankDetail::where('seller_id', $user->id)->first();
            // $account = \Stripe\Account::retrieve($bank_info->stripe_account)->jsonSerialize();


            \Stripe\Account::update(
                $bank_info->stripe_account,
                // ['metadata' => ['order_id' => '6735']]
                ['business_type' => 'individual',]
            );
            return $success;
        }
    }







    public function updatePersonalDetail(Request $request)
    {
        $data = $request->all();
        $validation = Validator::make($data, [
            'business_type'                => 'required|in:individual',
            'first_name'                  => 'required|string',
            'last_name'                  => 'required|string',
            'dob'                          => 'required|date_format:Y-m-d',
            'phone'                        => 'required|max:20',
            'email'                       => 'required|email',
            'social_id_number'            => 'required|numeric|',
            // 'cv'					=> 'required|mimes:docx,jpg,pdf,jpeg,image|max:5000',
        ]);

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $message) {
                $errors[] = $message;
            }
            return response()->json(['status' => false, 'message' => $errors]);
        }
        $user_id = $request->user()->id;
        $user =   User::find($user_id);
        // dd($user);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->dob = $request->dob;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->email = $request->email;
        $success = $user->save();

        if ($success) {

            $bankdetail =   BankDetail::where('seller_id', $user->id)->first();
            if (!$bankdetail || $bankdetail == null) {
                $bankdetail = new BankDetail();
            }


            // dd($bankdetail);
            $bankdetail->social_id_number = $request->social_id_number;
            $bankdetail->business_type = $request->business_type;
            $bankdetail->seller_id = $request->user()->id;
            $final =  $bankdetail->save();
            if ($final) {

                $success = $this->createStripeAccount();

                if ($success) {

                    return response()->json([
                        'status' => true,
                        'message' => 'Personal information updated successfully',
                        'html' => view('myaccount.include.address-detail', compact('user', 'bankdetail'))->render()
                    ]);
                } else {
                    return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating personal detail for transaction policy.Please try again later.']]);
                }
            } else {
                return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your scocial security number.']]);
            }
        } else {
            return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your user information.']]);
        }
    }

    public function updateAddressDetail(Request $request)
    {
        $data = $request->all();

        $validation = Validator::make($data, [
            'city'                    => 'required|string',
            'state'                  => 'required|string',
            'country'                  => 'required|string',
            'address_line_1'        => 'required|string',
            'address_line_2'        => 'required|string',
            'postal_code'           => 'required|numeric',
        ]);

        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $message) {
                $errors[] = $message;
            }
            return response()->json(['status' => false, 'message' => $errors]);
        }

        $user_id = $request->user()->id;
        $user =   User::find($user_id);

        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $success = $user->save();
        if ($success) {
            $bankdetail =   BankDetail::where('seller_id', $user->id)->first();
            // dd($bankdetail);
            $bankdetail->address_line_1 = $request->address_line_1;
            $bankdetail->address_line_2 = $request->address_line_2;
            $bankdetail->postal_code = $request->postal_code;
            $final =  $bankdetail->save();
            if ($final) {
                require_once public_path() . '/stripe-php/init.php';
                \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
                $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();
                $user_detail = User::find($bankdetail->seller_id);
                // dd($account);
                $first_name = $user_detail->first_name;
                $last_name = $user_detail->last_name;


                \Stripe\Account::update(
                    $bankdetail->stripe_account,
                    [
                        'business_type' => 'individual',
                        'default_currency' => 'USD',
                        'individual' => [
                            'address' => [
                                "city" => $user_detail->city,
                                "country" => "US",
                                "line1" =>  $bankdetail->address_line_1,
                                "line2" => $bankdetail->address_line_2,
                                "postal_code" => $bankdetail->postal_code,
                                "state" => $user_detail->state,
                                'postal_code' => $bankdetail->postal_code
                            ],
                            'dob' => [
                                'day' => date('d', strtotime($user_detail->dob)),
                                'month' => date('m', strtotime($user_detail->dob)),
                                'year' => date('Y', strtotime($user_detail->dob)),
                            ],
                            'email' => $user_detail->email,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'phone' => $user_detail->phone,
                        ],
                        'email' => $user_detail->email,
                        'settings' => [
                            'payouts' => [
                                'schedule' => [
                                    // 'delay_days' => 7,
                                    'interval' => 'manual'

                                ]
                            ]

                        ]
                    ]
                );
                return response()->json([
                    'status' => true,
                    'message' => 'Personal information updated successfully',
                    'html' => view('myaccount.include.upload-document', compact('bankdetail'))->render()
                ]);
            } else {
                return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your address line 1 & address line 2. Please try again later.']]);
            }
        } else {
            return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your address information.']]);
        }
    }






    public function uploadIdproof(Request $request)
    {
        $user_id = $request->user()->id;
        $user =   User::find($user_id);
        $data = $request->all();
        // dd($data);

        $validation = Validator::make($data, [
            'document_front_side'                  => 'nullable|image|mimes:jpg,png,jpeg|max:2000',
            'document_back_side'                => 'nullable|image|mimes:jpg,png,jpeg|max:2000',
        ]);
        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $message) {
                $errors[] = $message;
            }
            return response()->json(['status' => false, 'message' => $errors]);
        }

        $bankdetail =   BankDetail::where('seller_id', $user->id)->first();
        if ($request->document_front_side) {
            $old_front_Image =  $bankdetail->id_front;
            $name = 'front_' . time() . '-' . rand(1000, 9999);
            $front_name = $this->imageProcessing($request->document_front_side, $name);
            if ($front_name) {
                $bankdetail->id_front = $front_name;
                if ($old_front_Image && file_exists(public_path() . '/uploads/user/document/' . $old_front_Image)) {
                    unlink(public_path() . "/uploads/user/document/" . $old_front_Image);
                }
            }
        }
        if ($request->document_back_side) {
            $old_back_Image =  $bankdetail->id_back;
            $back_image__mane = 'back_' . time() . '-' . rand(1000, 9999);
            $back_name = $this->imageProcessing($request->document_back_side, $back_image__mane);
            if ($back_name) {
                $bankdetail->id_back = $back_name;
                if ($old_back_Image && file_exists(public_path() . '/uploads/user/document/' . $old_back_Image)) {
                    unlink(public_path() . "/uploads/user/document/" . $old_back_Image);
                }
            }
        }
        // if ($front_name && $back_name) {
        $successs = $bankdetail->save();
        if ($successs) {
            // $personal_info  =  BankDetail::where('seller_id', $user->id)->first();
            // $this->stripePersonalDetail($personal_info, 'update_document');
            return response()->json([
                'status' => true,
                'message' => 'Personal information updated successfully',
                'html' => view('myaccount.include.business-detail', compact('bankdetail'))->render()
            ]);
        } else {
            return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while uploading  your identity document. Please try again later']]);
        }
        // } else {
        //     return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while uploading  your identity document. Please try again later']]);
        // }
    }




    public function BusinessProfile(Request $request)
    {
        $data = $request->all();
        $validation = Validator::make($data, [
            // 'mcc'                    => 'required|numeric',
            'support_email'          => 'required|email',
            'support_phone'         => 'required|string|max:20',
            'support_url'           => 'nullable|url',
            'url'                   => 'nullable|url',
        ]);
        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $message) {
                $errors[] = $message;
            }
            return response()->json(['status' => false, 'message' => $errors]);
        }
        $user_id = $request->user()->id;
        $user =   User::find($user_id);
        $bankdetail =   BankDetail::where('seller_id', $user->id)->first();
        $bankdetail->mcc = $request->mcc;
        $bankdetail->support_email = $request->support_email;
        $bankdetail->support_phone = $request->support_phone;
        $bankdetail->support_url = ($request->support_url) ? $request->support_url : route('homepage');
        $bankdetail->url = ($request->url) ? $request->url : route('homepage');

        $success =  $bankdetail->save();
        if ($success) {
            // $personal_info  =  BankDetail::where('seller_id', $user->id)->first();
            // $this->stripePersonalDetail($personal_info, 'business_detail');
            require_once public_path() . '/stripe-php/init.php';
            \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
            $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();
            $user_detail = User::find($bankdetail->seller_id);
            $first_name = $user_detail->first_name;
            $last_name = $user_detail->last_name;
            // dd($account);
            \Stripe\Account::update(
                $bankdetail->stripe_account,
                // ['metadata' => ['order_id' => '6735']]
                [
                    'business_profile' => [
                        'mcc' => '8071',
                        'name' => $first_name . ' ' . $last_name,
                        'support_email' => $user_detail->email,
                        'support_phone' => $user_detail->phone,
                        'support_url' => ($bankdetail->support_url) ? $bankdetail->support_url : route('homepage'),
                        'url' => ($bankdetail->support_url) ? $bankdetail->support_url : route('homepage'),
                    ],
                ]
            );
            if ($account['individual']['id_number_provided'] == false) {
                \Stripe\Account::update(
                    $bankdetail->stripe_account,
                    ['individual' => ['id_number' => $bankdetail->social_id_number]]
                );
            }
            if ($account['individual']['ssn_last_4_provided'] == false) {
                \Stripe\Account::update(
                    $bankdetail->stripe_account,
                    ['individual' => ['ssn_last_4' => substr($bankdetail->social_id_number, -4)]]
                );
            }

            return response()->json([
                'status' => true,
                'message' => 'Personal information updated successfully',
                'html' => view('myaccount.include.bank-detail', compact('bankdetail'))->render()
            ]);
        } else {
            return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your address line 1 & address line 2. Please try again later.']]);
        }
    }




    public function updateBankDetail(Request $request)
    {
        $data = $request->all();
        $validation = Validator::make($data, [
            // 'mcc'                    => 'required|numeric',
            'bank_name'                 => 'required|string|max:100',
            'account_number'            => 'required|string|max:20',
            'account_holder_name'       => 'required|string',
            'routing_number'            => 'nullable|string',
            'country_of_bank'           => 'required|string|in:US,GB',
            'currency_accepted_by_bank' => 'required|string|in:USD,GBP,AUD',
            'swift_code'                => 'nullable|string'
        ]);
        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $message) {
                $errors[] = $message;
            }
            return response()->json(['status' => false, 'message' => $errors]);
        }
        $user_id = $request->user()->id;
        $user =   User::find($user_id);
        $bankdetail =   BankDetail::where('seller_id', $user->id)->first();

        $bankdetail->bank_name              = $request->bank_name;
        $bankdetail->account_number         = $request->account_number;
        $bankdetail->account_holder_name    = $request->account_holder_name;
        $bankdetail->routing_number         = $request->routing_number;
        $bankdetail->country_of_bank        = $request->country_of_bank;
        $bankdetail->swift_code             = $request->swift_code;
        $bankdetail->currency_of_bank       = $request->currency_accepted_by_bank;
        $success = $bankdetail->save();
        if ($success) {
            // dd('hello');
            // dd($bankdetail);
            require_once public_path() . '/stripe-php/init.php';
            \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
            $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();
            $user_detail = User::find($bankdetail->seller_id);
            // dd($account);
            if (count($account['external_accounts']['data']) < 1) {


                $token = \Stripe\Token::create([
                    'bank_account' => [
                        'country' => ($bankdetail->country_of_bank) ? $bankdetail->country_of_bank : 'US',
                        'currency' => ($bankdetail->currency_of_bank) ? $bankdetail->currency_of_bank : 'USD',
                        "account_holder_name" => $bankdetail->account_holder_name,
                        // "bank_name" => $bankdetail->bank_name,
                        // "account_holder_type" => $bankdetail->account_type,
                        'account_number' => $bankdetail->account_number,
                        'account_holder_type' => 'individual',
                        'routing_number' => $bankdetail->routing_number,
                    ],
                ]);
                if ($token) {
                    \Stripe\Account::createExternalAccount(
                        $bankdetail->stripe_account,
                        [
                            'external_account' => $token,
                        ]
                    );
                    $bank_account = $token->jsonSerialize();
                    // dd($bank_account);

                }
            } else {
                // if ($$bankdetail->stripe_bank_account == null) {
                //     $bankdetail->stripe_bank_account = $bank_account['bank_account']['id'];
                //     $bankdetail->save();
                // }
                // dd($account);
                $bank_account = \Stripe\Account::retrieveExternalAccount(
                    $bankdetail->stripe_account,
                    $account['external_accounts']['data'][0]['id']
                );
                if ($bank_account) {
                    \Stripe\Account::updateExternalAccount(
                        $bankdetail->stripe_account,
                        $account['external_accounts']['data'][0]['id'],
                        [
                            "account_holder_name" => $bankdetail->account_holder_name,
                            // "account_holder_type" => $bankdetail->account_type,
                        ]
                    );
                }
            }
            if ($bankdetail->stripe_bank_account == null) {
                $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();
                $bankdetail->stripe_bank_account = $account['external_accounts']['data'][0]['id'];
                // $bankdetail->stripe_customer_ref = $account['external_accounts']['data'][0]['customer'];

                $bankdetail->save();
            }
            $bank_account = BankDetail::where('seller_id', $user->id)->first();
            if ($bank_account->stripe_bank_account) {
                return response()->json([
                    'status' => true,
                    'message' => 'Banking  information updated successfully',
                    'html' => view('myaccount.include.submit-all-form', compact('bankdetail'))->render()
                ]);
            } else {
                return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your bank account detail. Please try again later.']]);
            }
        } else {
            return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while updating your bank account detail. Please try again later.']]);
        }
    }


    public function submitAllBankInfo(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        // dd($data);
        $validation = Validator::make($data, [
            'agreement'                 => 'required|'
        ]);
        if ($validation->fails()) {
            foreach ($validation->messages()->getMessages() as $message) {
                $errors[] = $message;
            }
            return response()->json(['status' => false, 'message' => $errors]);
        }

        $user_id = $request->user()->id;
        $user =   User::find($user_id);
        $bankdetail =   BankDetail::where('seller_id', $user->id)->first();
        // dd($bankdetail);

        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();
        // $user_detail = User::find($bankdetail->seller_id);
        if ($account['individual']['verification']['status'] ==  'verified') {
            $bankdetail->bank_status = 'verified';
        }
        if ($request->agreement && $request->agreement == 'on') {
            $bankdetail->agreement = 'yes';
        }

        $bankdetail->save();
        if ($account['individual']['verification']['status'] ==  'verified') {
            return response()->json([
                'status' => true,
                'message' => 'All   information updated successfully',
                'html' => ''
            ]);
        } else {
            return response()->json(['status' => false, 'message' => ['Sorry!, There was problem while verifying your account.Please check your information again and submit to get  verified.']]);
        }
        // dd($account['individual']['verification']['status']);
    }




    public function deleteAccount(Request $request)
    {
        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        $account = \Stripe\Account::retrieve(
            $request->stripe_account
        );
        $success  = $account->delete();
        dd($success);
    }





    public function deleteBank(Request $request)
    {
        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        \Stripe\Account::deleteExternalAccount(
            $request->stripe_account,
            $request->bank
        );
        dd('hello');
    }





    public function updateAddress(Request $request)
    {
        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        $bank_verifcation  =  BankDetail::where('seller_id', $request->user()->id)->first();

        \Stripe\Account::update(
            $bank_verifcation->stripe_account,
            // ['metadata' => ['order_id' => '6735']]
            [
                'individual' => [
                    // 'id_number' => ($account['individual']['id_number_provided'] == true) ? '000000000' : $account['individual']['id_number'],

                    'address' => [
                        "city" => $request->user()->city,
                        "country" => "US",
                        "line1" =>  $request->address_line_1,
                        "line2" => $request->address_line_1,
                        "postal_code" => $request->postal_code,
                        "state" => $request->user()->state,
                        'postal_code' => '90002'
                    ],
                    'dob' => [
                        'day' => date('d', strtotime($request->user()->dob)),
                        'month' => date('m', strtotime($request->user()->dob)),
                        'year' => date('Y', strtotime($request->user()->dob)),
                    ],
                    'email' => $request->user()->email,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'phone' => $request->user()->phone,
                    // 'ssn_last_4' => ($account['individual']['ssn_last_4'] == null) ? '0000' : $account['individual']['ssn_last_4'],

                ],

                'email' => $request->user()->email,
                'tos_acceptance' => [
                    'date' => time(),
                    'ip' => $getIP
                ],
                'business_profile' => [
                    'mcc' => '8071',
                    'name' => $first_name . ' ' . $last_name,
                    'support_email' => $request->user()->email,
                    'support_phone' => $request->user()->phone,
                    'support_url' => route('homepage'),
                    'url' => route('homepage'),
                ],
            ]
        );
    }


    public function withdrawcash(Request $request)
    {
        $user = Auth::user();
        $bankdetail  =  BankDetail::where('seller_id', $user->id)->first();
        if (isset($bankdetail) && $bankdetail->bank_status == 'verified') {
            $data = $request->all();
            $validation = Validator::make($data, [
                'withdraw_amount'          => 'required|numeric',
            ]);
            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $message) {
                    $errors[] = $message;
                }
                return response()->json(['status' => false, 'message' => $errors]);
            }

            $withdrawn_cash = Withdrawn::select(DB::raw('sum(amount) as old_cash_outh'))
                ->where('user_id', $user->id)
                ->first();

            // dd($withdrawn_cash);

            $stripe_transfered = Bookings::select(DB::raw('sum(total_amount - (((price*booked_duration)/100)*tax_percent * ((price*booked_duration)/100)*service_fee_percent)) as balance'))
                ->where('seller_id', $user->id)
                // ->where('rental_status', 'Delivered')
                ->where('rental_status', '!=', 'Rejected')
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=', 'Pending')
                // ->where('created_at', '<', date('Y-m-d H:i:s', strtotime('-7 days')))
                ->where('amount_transfered', 'no')
                ->where('refunded', '!=', 'yes')
                ->first();
            // dd($stripe_transfered);

            $balence = Bookings::select(DB::raw('sum(total_amount - (((price*booked_duration)/100)*tax_percent * ((price*booked_duration)/100)*service_fee_percent)) as balance'))
                ->where('seller_id', $user->id)
                ->where('rental_status', '!=', 'Rejected')
                ->where('rental_status', '!=', 'Canceled')
                ->where('rental_status', '!=', 'Pending')
                ->where('refunded', '!=', 'yes')
                ->first();

            $possible_withdraw = $balence['balance'] - $withdrawn_cash['old_cash_outh'] - $stripe_transfered['balance'];
            if ($request->withdraw_amount >= $possible_withdraw) {
                return response()->json(['status' => false, 'message' => ['withdraw amount is invalid.']]);
            }

            require_once public_path() . '/stripe-php/init.php';
            \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
            // $account = \Stripe\Account::retrieve($bankdetail->stripe_account)->jsonSerialize();

            // dd($request->withdraw_amount);
            $stripe_balance = \Stripe\Balance::retrieve(['stripe_account' => $bankdetail->stripe_account])->jsonSerialize();
            // dd($stripe_balance);
            // dd($bankdetail->stripe_account);
            if ($stripe_balance['available'][0]['amount'] < $request->withdraw_amount) {
                return response()->json(['status' => false, 'message' => ['Insufficient Available Balance.']]);
            }
            $amount = (int) ($request->withdraw_amount * 100);
            // dd($amount);

            $payout = \Stripe\Payout::create(
                [
                    'amount' => $amount,
                    'currency' => 'usd',
                    // 'method' => 'instant',
                    // 'destination' => $bankdetail->stripe_bank_account,
                    // 'source_type' => 'bank_account',
                ],
                ['stripe_account' => $bankdetail->stripe_account]
            )->jsonSerialize();
            if ($payout) {
                $withdraw_info = new Withdrawn();
                $withdraw_info->user_id  = $user->id;
                $withdraw_info->amount  = $amount / 100;

                $withdraw_info->cashout_id  = $payout['id'];
                $withdraw_info->balance_transaction  = $payout['balance_transaction'];
                $withdraw_info->arrival_date  = $payout['arrival_date'];
                $withdraw_info->stripe_bank_id  = $payout['destination'];
                $withdraw_info->method  = $payout['method'];
                $withdraw_info->source_type  = $payout['source_type'];
                $withdraw_info->requested_date  = $payout['created'];
                $withdraw_info->currency  = $payout['currency'];
                $withdraw_info->type  = $payout['type'];
                $withdraw_info->withdraw_status  = 'Paid';
                // dd($payout);
                $success = $withdraw_info->save();
                return response()->json(
                    [
                        'status' => true,
                        'message' => 'Fund transfer successfully to your account.',
                        'html' => view('myaccount.include.payout-detail', compact('withdraw_info'))->render()
                    ]
                );
            } else {
                return response()->json(['status' => false, 'message' => 'Opps!, There was problem while sending amount to your bank account. Please try again later.']);
            }
        } else {
            return response()->json(['status' => false, 'message' => ['Your bank account is not updated completely. Update your Banking Detail completely to get verified.']]);
        }


        // dd($request->all());



    }




    public function addBankDetail(Request $request)
    {
        $this->validate($request, [

            //person info 

            "business_type"     => "required|in:individual",
            "address_line_1"    => "required|string",
            "address_line_2"    => "required|string",
            "account_number"    => "required|string",
            "account_holder"    => "required|string",
            "bank_name"         => "required|string",

            "routing_number"    => "nullable|numeric",

        ]);
        $bank_verifcation  =  BankDetail::where('seller_id', $request->user()->id)->first();
        require_once public_path() . '/stripe-php/init.php';
        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
        if (!$bank_verifcation) {
            $bank_verifcation = new BankDetail();

            $account = \Stripe\Account::create([
                'country' => 'US',
                'type' => 'custom',
                'email' => $request->user()->email,

                'requested_capabilities' => ['card_payments', 'transfers'],
                'tos_acceptance' => [
                    'date' => time(),
                    'ip' => $getIP
                ],
            ])->jsonSerialize();

            $bank_verifcation->stripe_account = $account['id'];
            $bank_verifcation->currency_type = $account['id'];
            $bank_verifcation->bank_status = 'non-verified';
            $bank_verifcation->seller_id = $request->user()->id;
            $success = $bank_verifcation->save();
        }
        $bank_verifcation  =  BankDetail::where('seller_id', $request->user()->id)->first();

        $first_name  = $request->user()->first_name;
        $last_name  = $request->user()->last_name;
        $getIP = \Request::getClientIp();


        if ($bank_verifcation && $bank_verifcation->bank_status == 'non-verified') {
            \Stripe\Account::update(
                $bank_verifcation->stripe_account,
                [
                    'business_type' => 'individual',
                    'default_currency' => 'USD',
                    'individual' => [
                        'id_number' => $request->social_id_number,
                        'address' => [
                            "city" => $request->user()->city,
                            "country" => "US",
                            "line1" =>  $request->address_line_1,
                            "line2" => $request->address_line_1,
                            "postal_code" => $request->postal_code,
                            "state" => $request->user()->state,
                            'postal_code' => $request->postal_code,
                        ],
                        'dob' => [
                            'day' => date('d', strtotime($request->user()->dob)),
                            'month' => date('m', strtotime($request->user()->dob)),
                            'year' => date('Y', strtotime($request->user()->dob)),
                        ],
                        'email' => $request->user()->email,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'phone' => $request->user()->phone,
                        'ssn_last_4' => ($account['individual']['ssn_last_4'] == null) ? '0000' : $account['individual']['ssn_last_4'],

                    ],

                    'email' => $request->user()->email,
                    'tos_acceptance' => [
                        'date' => time(),
                        'ip' => $getIP
                    ],
                    'business_profile' => [
                        'mcc' => '8071',
                        'name' => $first_name . ' ' . $last_name,
                        'support_email' => $request->user()->email,
                        'support_phone' => $request->user()->phone,
                        'support_url' => route('homepage'),
                        'url' => route('homepage'),
                    ],
                ]
            );





            if (count($account['external_accounts']['data']) < 1) {


                $token = \Stripe\Token::create([
                    'bank_account' => [
                        'country' => 'US',
                        'currency' => $request->user()->currency,
                        "account_holder_name" => $request->account_holder,
                        "account_holder_type" => $request->account_type,
                        'account_number' => $request->account_number,
                        'account_holder_type' => 'individual',
                        'routing_number' => '110000000',
                    ],
                ]);
                if ($token) {
                    \Stripe\Account::createExternalAccount(
                        $bank_verifcation->stripe_account,
                        [
                            'external_account' => $token,
                        ]
                    );
                }
            } else {
                // dd($account);
                $bank_account = \Stripe\Account::retrieveExternalAccount(
                    $bank_verifcation->stripe_account,
                    $account['external_accounts']['data'][0]['id']
                );
                // dd($account['external_accounts']['data'][0]);
                // dd($account);
                if ($bank_account) {
                    \Stripe\Account::updateExternalAccount(
                        $bank_verifcation->stripe_account,
                        $account['external_accounts']['data'][0]['id'],
                        [
                            "account_holder_name" => $request->account_holder,
                            "account_holder_type" => $request->account_type,
                        ]
                    );
                }
            }
        } else {
        }
        return redirect()->route('updateBankDetail');
    }









    public function imageProcessing($image,  $image_title, $resulation = null)
    {
        $image_ext = $image->getClientOriginalExtension();
        $image_name = $image_title . '.' . $image_ext;


        // $original_path = public_path().'/uploads/courses/';
        $original_path = public_path() . "/uploads/user/document";
        if (!File::exists($original_path)) {
            File::makeDirectory($original_path, 0777, true, true);
        }

        $keep_original = Image::make($image->getRealPath());
        $keep_original->save($original_path . '/' . $image_name);
        if ($resulation != null) {
            $this->resize_image($image_name, $original_path, $resulation, $image_ext);
        }

        return $image_name;
    }




    public function resize_image($filename, $upload_path = null, $max_resolution, $extension)
    {
        $image_path = $upload_path . '/' . $filename;
        if (strtolower($extension) === "png") {
            $original_image  = imagecreatefrompng($image_path);
        }
        if ((strtolower($extension) === "jpg") || (strtolower($extension) === "jpeg")) {
            $original_image  = imagecreatefromjpeg($image_path);
        }
        $image_detail = getimagesize($image_path);
        // dd($image_detail);

        $original_width = imagesx($original_image);
        $original_height = imagesy($original_image);

        $ratio  = $max_resolution / $original_width;
        $new_width = $max_resolution;
        $new_height = $original_height * $ratio;
        // if that didn't work
        if ($new_height > $max_resolution) {
            $ratio = $max_resolution / $original_height;
            $new_height = $max_resolution;
            $new_width = $original_width * $ratio;
        }
        if ($original_image) {
            $new_image = imagecreatetruecolor($new_width, $new_height);
            imagecopyresampled($new_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
            return imagejpeg($new_image, $image_path, 90);
        }
    }
}
