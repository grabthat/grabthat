<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\Rentals;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Orders;

class BookingController extends Controller
{
    public function __construct(Bookings $booking, Rentals $rental, Orders $order, User $user)
    {
        $this->booking = $booking;
        $this->rental = $rental;
        $this->order = $order;
        $this->user = $user;
    }

    public function listBooking(Request $request,  $slug)
    {
        $user = Auth::user();
        $rental_info = $this->rental->where(['slug' => $slug, 'user_id' => $user->id])->first();
        if (!$rental_info) {
            $request->session()->flash('error', 'Rental Information not found.');
            return redirect()->back();
        }
        // $all_booking = $this->booking->where(['rental_id' => $rental_info->id, 'user_id' => $user->id])->get();
        $all_booking = $this->booking->where(['rental_id' => $rental_info->id])->paginate(3);
        return view('rental/booking/list', compact('all_booking', 'rental_info'));
    }
    public function transactionDetail(Request $request)
    {

        $booking_info = $this->booking->find($request->booking_id);
        if (!$booking_info) {
            return response()->json(['status' => false, 'message' => ['Invalid Transaction Information.']]);
        }
        return response()->json(
            [
                'status' => true,
                'message' => 'Transcation found.',
                'data' => view('myaccount.transaction.transaction-modal', compact('booking_info'))->render(),
            ]
        );
    }






    protected function cancelRentalOrder(Request $request)
    {
        $user = Auth::user();

        $booking_info = $this->booking
            ->where('id', $request->id)
            ->where('user_id', $user->id)->first();
        if (!$booking_info) {
            return response()->json(['status' => false, 'message' => ['Invalid Order Information.']]);
        }

        $now = time();
        $day = 24 * 60 * 60;

        if (($booking_info->rental_status == 'Cancelling') && ($booking_info->refund_request == 'yes') && ($booking_info->refunded  == 'no')) {
            return response()->json(['status' => false, 'message' => ['You already have sent request for rental cancel. Wait till next update.']]);
        } else if (($booking_info->rental_status == 'Canceled') && ($booking_info->refund_request == 'yes') && ($booking_info->refunded  == 'yes')) {
            return response()->json(['status' => false, 'message' => ['Your rental cancel request has already reviewed and refunded amount to your account.']]);
        } else if ($booking_info->start_date - $now <= $day) {
            return response()->json(['status' => false, 'message' => ['Rental can not be canceled due to booking is starting within less than 24 hours.']]);
        } else if ($booking_info->rental_status  == 'Delivered') {
            return response()->json(['status' => false, 'message' => ['After Service Delivered rental can not be canceled.']]);
        }
        if (($booking_info->rental_status == 'Confirmed') && (($booking_info->start_date - $now) >= $day)) {
            $card_info = $this->order->where(['reference_id' => $booking_info->reference_id])->first();
            if ($card_info) {
                $card = $card_info->card_number;
                $exp_month = $card_info->card_exp_month;
                $exp_year = $card_info->card_exp_year;
                $price = $booking_info->price;

                $tax_amount = (($price * $booking_info->booked_duration) / 100) * $booking_info->tax_percent;
                $fee = (($price * $booking_info->booked_duration) / 100) * $booking_info->service_fee_percent;


                $refundable_amount = $booking_info->total_amount - $tax_amount - $fee;
                require_once public_path() . '/stripe-php/init.php';
                \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
                // checking for previous refund 
                $stripe_info = \Stripe\Charge::retrieve(
                    $card_info->charge_id
                )->jsonSerialize();

                // dd($stripe_info);
                if ($stripe_info['amount_refunded'] < 1 && (($stripe_info['amount'] / 100) >= $booking_info->total_amount)) {
                    $refund_query = \Stripe\Refund::create([
                        'charge' => $card_info->charge_id,
                        'amount' => (int) $refundable_amount * 100,

                    ]);
                    $success = $refund_query->jsonSerialize();
                    // dd($success);
                    if ($success['status'] == 'succeeded') {

                        $booking_info->rental_status = 'Canceled';
                        $booking_info->refunded_at = time();
                        $booking_info->refunded = 'yes';
                        $booking_info->save();
                        // updateing order table 
                        $card_info->payment_status = 'Refunded';
                        $card_info->save();
                        return response()->json(['status' => true, 'message' => 'Order cancellation proceeded successfully and payment refunded to your account.', 'data' => $success]);
                    } else {

                        return response()->json(['status' => false, 'message' => ['Sorry there was problem while canceling rental order and refunding the amount. Please Try again later or contact to admin.']]);
                    }
                } else {
                    $booking_info->rental_status = 'Canceled';
                    $booking_info->refunded_at = time();
                    $booking_info->refunded = 'yes';
                    $booking_info->save();
                    // updateing order table 
                    $card_info->payment_status = 'Refunded';
                    $card_info->save();
                    return response()->json(['status' => false, 'message' => ['Amount Already refunded and Booking order has been canceled.']]);
                }
            } else {
                $booking_info->rental_status = 'Canceled';
                $booking_info->refund_request = 'yes';
                $booking_info->refunded = 'yes';
                $success = $booking_info->save();
                if ($success) {
                    return response()->json(['status' => true, 'message' => 'Booking canceled successfully.']);
                } else {
                    return response()->json(['status' => false, 'message' => ['Opps! There was problem while requesting to cancel your rental order.Please try again later..']]);
                }
            }
        } else {
            $booking_info->rental_status = 'Cancelling';
            $booking_info->refund_request = 'yes';
            $booking_info->refunded = 'Pending';
            $success = $booking_info->save();
            if ($success) {
                return response()->json(['status' => true, 'message' => 'Your Rental cancel request has been sent. Once it reviewed your request will submited to further process.']);
            } else {
                return response()->json(['status' => false, 'message' => ['Opps! There was problem while requesting to cancel your rental order.Please try again later..']]);
            }
        }



    }






    protected function confirmRental(Request $request)
    {
        $booking_info = $this->booking->find($request->id);
        if (!$booking_info) {
            return response()->json(['status' => false, 'message' => ['Invalid Order Information.']]);
        }
        // dd($request->all());


        if (($booking_info->rental_status == 'Cancelling') && ($booking_info->refund_request == 'yes') && ($booking_info->refunded  == 'no')) {
            return response()->json(['status' => false, 'message' => ['Customer requested for cancel the rental.Please take break.']]);
        } else if (($booking_info->rental_status == 'Canceled') && ($booking_info->refund_request == 'yes') && ($booking_info->refunded  == 'yes')) {
            return response()->json(['status' => false, 'message' => ['Customer requested for cancel the rental.Please take break.']]);
        } else if ($booking_info->rental_status  == 'Delivered') {
            return response()->json(['status' => false, 'message' => ['This booking has been already delivered.']]);
        } else if ($booking_info->rental_status == 'Confirmed') {
            return response()->json(['status' => false, 'message' => ['You already confirmed the customer for rental.']]);
        }
        $booking_info->rental_status = 'Confirmed';
        $success = $booking_info->save();
        if ($success) {
            return response()->json(['status' => true, 'message' => 'Rental confirmed to the customer.']);
        } else {
            return response()->json(['status' => false, 'message' => ['Opps! There was problem while making confirm  booking  order.Please try again later..']]);
        }
    }

    public function DeliveredRental(Request $request){
        $booking_info = $this->booking->find($request->id);
        if (!$booking_info) {
            return response()->json(['status' => false, 'message' => ['Invalid Order Information.']]);
        }
        // dd($request->all());
        if(($booking_info->rental_status == 'Confirmed') && time() > $booking_info->start_date  ){
            $booking_info->rental_status = 'Delivered';
            $success = $booking_info->save();
            if ($success) {
                return response()->json(['status' => true, 'message' => 'Rental marked as Delivered.']);
            } else {
                return response()->json(['status' => false, 'message' => ['Opps! There was problem while updating  Delivered  booking  order.Please try again later..']]);
            }
        }else {
            return response()->json(['status' => false, 'message' => ['Opps! Request can not be proccess at this time. Please Try again later.']]);

        }


         
    }





    public function buyerdetail(Request $request)
    {
        $booking_info  = $this->booking->find($request->id);
        if (!$booking_info) {
            return response()->json(['status' => false, 'message' => ['Invalid Order Information.']]);
        }
        $user_info = $this->user->find($booking_info->user_id);
        if (!$user_info) {
            return response()->json(['status' => false, 'message' => ['User Information not found.']]);
        }
        $item_info = $this->rental->find($booking_info->rental_id);
        $payment_info = $this->order
            ->select('id', 'reference_id')
            // ->where('rental_id', $booking_info->rental_id)
            ->where('reference_id', $booking_info->reference_id)
            ->first();
        if (!$payment_info) {
            return response()->json(['status' => false, 'message' => ['Payment information not found.']]);
        }
        return response()->json([
            'status' => true,
            'message' => 'information found.',
            'data' => view('rental/booking/modal', compact('booking_info', 'user_info', 'payment_info', 'item_info'))->render()
        ]);
    }


    // this method is for cancelling the booking order  for my rental service from customer
    public function CancelCustomerBooking(Request $request)
    {
        $booking_info = $this->booking->find($request->id);
        if (!$booking_info) {
            return response()->json(['status' => false, 'message' => ['Invalid Booking Information.']]);
        }
        // dd($booking_info);
        $now = time();
        $day = 24 * 60 * 60;
        if ($booking_info->start_date < time()) {
            return response()->json(['status' => false, 'message' => ['Expired rental booking information.']]);
        }
        $date_diff = $booking_info->start_date - $now;
        if ($date_diff <= $day) {
            return response()->json(['status' => false, 'message' => ['Booked rental can not be canceled due to booking is starting within less than 24 hours.']]);
        }
        // if($booking_info->rental_status == 'Canceled' || $booking_info->rental_status == 'Canceled'){
        //     return response()->json(['status' => false, 'message' => ['Booking Already Canceled or rejected.']]);
        // }
        $card_info = $this->order->where(['reference_id' => $booking_info->reference_id])->first();
        if ($card_info) {
            $card = $card_info->card_number;
            $exp_month = $card_info->card_exp_month;
            $exp_year = $card_info->card_exp_year;
            $price = $booking_info->price;

            $tax_amount = (($price * $booking_info->booked_duration) / 100) * $booking_info->tax_percent;
            $fee = (($price * $booking_info->booked_duration) / 100) * $booking_info->service_fee_percent;


            $refundable_amount = $booking_info->total_amount - $tax_amount - $fee;

            require_once public_path() . '/stripe-php/init.php';
            \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
            // checking for previous refund 
            $stripe_info = \Stripe\Charge::retrieve(
                $card_info->charge_id
            )->jsonSerialize();

            // dd($stripe_info);
            if ($stripe_info['amount_refunded'] < 1 && (($stripe_info['amount'] / 100) >= $booking_info->total_amount)) {
                $refund_query = \Stripe\Refund::create([
                    'charge' => $card_info->charge_id,
                    'amount' => (int) $refundable_amount * 100,

                ]);
                $success = $refund_query->jsonSerialize();
                // dd($success);
                if ($success['status'] == 'succeeded') {

                    $booking_info->rental_status = 'Canceled';
                    $booking_info->refunded_at = time();
                    $booking_info->refunded = 'yes';
                    $booking_info->save();
                    // updateing order table 
                    $card_info->payment_status = 'Refunded';
                    $card_info->save();
                    return response()->json(['status' => true, 'message' => 'Order cancellation proceeded successfully and payment refunded to the customer account.', 'data' => $success]);
                } else {
                    // $booking_info->rental_status = 'Canceled';
                    // $booking_info->refunded_at = time();
                    // $booking_info->refunded = 'yes';
                    // $booking_info->save();
                    // // updateing order table 
                    // $card_info->payment_status = 'Refunded';
                    // $card_info->save();
                    return response()->json(['status' => false, 'message' => ['Sorry there was problem while canceling rental order and refunding the amount. Please Try again later or contact to admin.']]);
                }
            } else {
                $booking_info->rental_status = 'Canceled';
                $booking_info->refunded_at = time();
                $booking_info->refunded = 'yes';
                $booking_info->save();
                // updateing order table 
                $card_info->payment_status = 'Refunded';
                $card_info->save();
                return response()->json(['status' => false, 'message' => ['Amount Already refunded and Booking order has been canceled.']]);
            }
        } else {
            $booking_info->rental_status = 'Canceled';
            $booking_info->refunded_at = time();
            $booking_info->refunded = 'yes';
            $success = $booking_info->save();
            if ($success) {
                return response()->json(['status' => true, 'message' => 'Booking order has been canceled.']);
            } else {
                return response()->json(['status' => false, 'message' => ['Opps! There was problem while cancelling the booking order. Please try again later.']]);
            }
        }
    }

    protected function removeRental(Request $request)
    {
        $user = Auth::user();
        $rental_info  = $this->rental->find($request->id);
        // dd($rental_info);
        if (!$rental_info) {
            return response()->json(['status' => false, 'message' => ['Service Not found.']]);
        }
        // dd(time());
        $booking_list = $this->booking
            ->where('seller_id', $user->id)
            ->where('rental_id', $rental_info->id)
            ->where('start_date', '>=', time())
            ->where('rental_status', '!=', 'Canceled')
            ->where('rental_status', '!=', 'Rejected')
            ->get();

        // dd($booking_list);
        $rental_info->status = 0;
        $rental_info->draft = 1;
        $rental_info->availability = 0;


        if ($booking_list && $booking_list->count()) {
            $success_refund = 0;
            foreach ($booking_list as $booking_data) {
                $card_info = $this->order->where(['reference_id' => $booking_data->reference_id])->first();


                if ($card_info) {
                    if ($card_info->payment_status == 'succeeded') {
                        $card = $card_info->card_number;
                        $exp_month = $card_info->card_exp_month;
                        $exp_year = $card_info->card_exp_year;
                        $price = $booking_data->price;

                        $tax_amount = (($price * $booking_data->booked_duration) / 100) * $booking_data->tax_percent;
                        $fee = (($price * $booking_data->booked_duration) / 100) * $booking_data->service_fee_percent;
                        // dd($tax_amount);

                        $refundable_amount = $booking_data->total_amount - $tax_amount - $fee;


                        require_once public_path() . '/stripe-php/init.php';
                        \Stripe\Stripe::setApiKey(env('STRIPE_API_KEY'));
                        // checking for previous refund 
                        $stripe_info = \Stripe\Charge::retrieve(
                            $card_info->charge_id
                        )->jsonSerialize();


                        if ($stripe_info['amount_refunded'] < 1 && (($stripe_info['amount'] / 100) >= $booking_data->total_amount)) {
                            // dd($refundable_amount);
                            // return response()->json(['status' => false, 'message' => ['Amount already refunded.']]);
                            $refund_query = \Stripe\Refund::create([
                                'charge' => $card_info->charge_id,
                                'amount' => (int) $refundable_amount * 100
                            ]);

                            $success = $refund_query->jsonSerialize();
                            if ($success['status'] == 'succeeded') {
                                $success_refund = $success_refund + 1;
                                $booking_data->rental_status = 'Canceled';
                                $booking_data->refunded_at = time();
                                $booking_data->refunded = 'yes';
                                $booking_data->save();

                                $card_info->payment_status = 'Refunded';
                                $card_info->save();
                            }
                        } else {
                            $success_refund = $success_refund + 1;

                            $booking_data->rental_status = 'Canceled';
                            $booking_data->refunded_at = time();
                            $booking_data->refunded = 'yes';
                            $booking_data->save();

                            $card_info->payment_status = 'Refunded';
                            $card_info->save();
                        }
                    } else {
                        // card exists but status refunded 
                        $success_refund = $success_refund + 1;
                        $booking_data->rental_status = 'Canceled';
                        $booking_data->refunded_at = time();
                        $booking_data->refunded = 'yes';
                        $booking_data->save();
                    }
                } else {
                    // change status of booking directly 
                    $success_refund = $success_refund + 1;
                    $booking_data->rental_status = 'Canceled';
                    $booking_data->refunded_at = time();
                    $booking_data->refunded = 'yes';
                    $booking_data->save();

                    $card_info->payment_status = 'Refunded';
                    $card_info->save();
                }
            }
            if ($success_refund == $booking_list->count()) {

                $done = $rental_info->save();
                if ($done) {
                    return response()->json(['status' => true, 'message' => 'Your rental has been updated status to canceled and now this rental will no longer shown in the rental.']);
                } else {
                    return response()->json(['status' => false, 'message' => ['Opp! there was problem while canceling your rental. please Try again later.']]);
                }
            } else {
                return response()->json(['status' => false, 'message' => ['Opps!, There was problem while refunding  booked rental. please try again.']]);
            }
        }
        $done = $rental_info->save();
        if ($done) {
            return response()->json(['status' => true, 'message' => 'Your rental has been updated status to canceled and now this rental will no longer shown in the rental.']);
        } else {
            return response()->json(['status' => false, 'message' => ['Opp! there was problem while canceling your rental. please Try again later.']]);
        }
    }
}
