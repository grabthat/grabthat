<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Rentals;
use Illuminate\Http\Request;
use  Session;

class FrontEndController extends Controller
{

    public function homepage(Request $request)
    {
        // dd($request);
        if ($request->user()) {
            // return redirect()->route('homepage');
            // dd($request->user());
            $getIP = \Request::getClientIp();
            $location = $this->GetUserLocation($getIP);
            $city = (!empty($location) && isset($location['city'])) ? $location['city'] : 'Delhi';

            $categories = Categories::select('name', 'slug', 'image')
                ->where('status', 1)->where('on_homepage', 1)->where('parent', '=', NULL)
                ->orderBy('name', 'ASC')->get();

            foreach ($categories as $k => $v) {
                $categories[$k]->image = GetCategoryImage($categories[$k]->image, 'large');
            }

            $rentalsObj = new Rentals();

            $rentals_popular = $rentalsObj->FetchRentals('popular', $city);
            $rentals_today = $rentalsObj->FetchRentals('today', $city);
            // dd($rentals_today[0]->rentReview);
            $rentals_tomorrow = $rentalsObj->FetchRentals('tomorrow', $city);

            $data = array();
            $data['city'] = $city;
            $data['categories'] = $categories;

            $data['popular'] = $rentals_popular;
            $data['today'] = $rentals_today;
            $data['tomorrow'] = $rentals_tomorrow;
        } else {
            $getIP = \Request::getClientIp();
            $user_loc = $this->GetUserLocation($getIP);

            if (!empty(Session::get('loc'))) {
                $location = Session::get('loc');
            } else {
                $location = @$user_loc['city'];
            }


            $city = (!empty($location) && isset($location)) ? $location : 'Delhi';

            $categories = Categories::select('name', 'slug', 'image')
                ->where('status', 1)->where('on_homepage', 1)->where('parent', '=', NULL)
                ->orderBy('name', 'ASC')->get();

            foreach ($categories as $k => $v) {
                $categories[$k]->image = GetCategoryImage($categories[$k]->image, 'large');
            }

            $rentals = Rentals::select('rentals.id', 'rentals.name', 'rentals.slug', 'rentals.duration', 'rentals.price', 'rentals.image', 'rentals.city', 'users.display_name', 'categories.name as category')
                ->leftJoin('categories', 'categories.id', '=', 'rentals.category')
                ->leftJoin('users', 'users.id', '=', 'rentals.user_id')
                ->where('rentals.status', 1)->where('draft', 0)
                ->where('rentals.city', 'LIKE', $city . '%')
                ->OrWhere('rentals.state', 'LIKE', $city . '%')
                ->limit(HOME_CAT_LIMIT)
                ->orderBy('rentals.id', 'desc')
                ->get();


            foreach ($rentals as $k => $v) {
                $price = GetRenalPrice($rentals[$k]->duration, $rentals[$k]->price);
                $rating = \App\Reviews::where('rental_id', $v->id)->pluck('rating')->toArray();
                $review = \App\Reviews::where('rental_id', $v->id)->count();
                if ($price == '') {
                    unset($rentals[$k]);
                    continue;
                }

                $rentals[$k]->price = $price;
                $rentals[$k]->rating = $rating;
                $rentals[$k]->review = $review;
                $rentals[$k]->image = GetRentalImage($rentals[$k]->image, $rentals[$k]->id, 'large');
            }
            $child_categories = Categories::select('categories.id', 'categories.name', 'categories.slug', 'categories.image')
                ->selectRaw('count(rentals.id) as rental')
                ->leftJoin('rentals', 'rentals.category', '=', 'categories.id')
                ->where('categories.status', 1)->where('on_homepage', 1)->where('parent', '>', 0)
                ->orderBy('categories.name', 'ASC')->groupBy('categories.id')
                ->having('rental', '>', 0)->get();

            foreach ($child_categories as $k => $v) {
                $child_categories[$k]->image = GetCategoryImage($child_categories[$k]->image, 'large');

                $child_rentals = Rentals::select('rentals.id', 'rentals.name', 'rentals.slug', 'rentals.duration', 'rentals.price', 'rentals.image', 'rentals.city', 'users.display_name')
                    ->leftJoin('users', 'users.id', '=', 'rentals.user_id')

                    ->where('rentals.status', 1)->where('draft', 0)
                    ->where('rentals.category', $child_categories[$k]->id)
                    ->limit(HOME_RENTAL_LIMIT)->get();



                foreach ($child_rentals as $k1 => $v1) {
                    $price = GetRenalPrice($child_rentals[$k1]->duration, $child_rentals[$k1]->price);
                    if ($price == '') {
                        unset($child_rentals[$k1]);
                        continue;
                    }

                    $child_rentals[$k1]->price = $price;
                    $child_rentals[$k1]->image = GetRentalImage($child_rentals[$k1]->image, $child_rentals[$k1]->id, 'large');
                }

                $child_categories[$k]->rentals = $child_rentals;
            }
            $data = array();
            $data['city'] = $city;
            $data['categories'] = $categories;
            $data['rentals'] = $rentals;
            $data['child_categories'] = $child_categories;
        }
        return view('front/index', $data);
    }

    public function customer(Request $request)
    {
        return redirect()->route('homepage');
    }
}
