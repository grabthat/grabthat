<?php

namespace App\Providers;

// use Illuminate\View\View;
use Illuminate\Support\ServiceProvider;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // View::composer('makeWithdrawl,myBankDetail, UserMypayment', 'App\ViewComposer\ViewComposer');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
