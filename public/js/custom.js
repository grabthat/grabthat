$(document).ready(function() {
    $(".details-page-nav li span").click(function() {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
        var get_id = "."+$(this).attr('id');
        console.log(get_id);
        $('html, body').animate({
            scrollTop: $(get_id).offset().top - 164
        }, 2000);
    });
    $('.how-map-btn').click(function(){
        $('.search-content').toggleClass('open');
        if($(this).attr('class')=='how-map-btn') {
            $('.search-map').fadeIn();
            $(this).addClass('open');
        }
        else {
            $('.search-map').fadeOut();
            $(this).removeClass('open');
        }
        
    });
    $('.login-profile').click(function() {
        $('.dropdown-tooltip').fadeIn(500);
        $('.transparent-box').show();
    });
    $('.transparent-box').click(function() {
        $('.dropdown-tooltip').fadeOut(500);
        $(this).hide();
    });
    $('.filter-icon').click(function() {
        $('.filter-box').animate({'left':0});
    });
    $('.filter-close').click(function() {
        $('.filter-box').animate({'left':'-320px'});
    });
    $('.next-btn').click(function() {
       $(this).parents('.add-rental-row').hide().next().fadeIn(); 
    });
    $('.rental-backBtn').click(function() {
       $(this).parents('.add-rental-row').hide().prev().fadeIn(); 
    });
    $('.my-rental-btn input').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
        var count = $(this).index();
        $('.my-rental-content').children().eq(count).fadeIn().siblings().hide();
    });
    $('.acc-update-btn input').click(function(){
        console.log($(this).val());
        if($(this).val()=="Update") {
            $(this).val('Submit');
            $(this).parents('li').find('.acc-pwd-field').show();
        }                           
        else {
            $(this).val('Update');
            $(this).parents('li').find('.acc-pwd-field').hide();
        }
     });
    $('.head-location').click(function() {
        $('.location-box').show().animate({top: '0'},400);
        $('.dark-bg').fadeIn();
    });
    $('.popup-close').click(function() {
        $('.location-box, .calender-popup, .payment-popup').animate({top: '-100px'},400, function(){
            $(this).hide();
            $('.dark-bg').fadeOut();
        });
	});
    $('.change-date').click(function() {
        $('.calender-popup').show().animate({top: '0'},400);
        $('.dark-bg').fadeIn();
    });
    $('.payment-details').click(function() {
        $('.payment-popup').show().animate({top: '0'},400);
        $('.dark-bg').fadeIn();
    });
});
$(window).scroll(function(){
    var scroll_top = $(window).scrollTop();
    if(scroll_top>0) {
        $('.top-head').addClass('fixed');
    }
    else {
        $('.top-head').removeClass('fixed');
    }
})