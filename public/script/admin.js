jQuery(document).ready(function() {
    var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');

    $('.rt_nav_header.horizontal-layout .nav-bottom .page-navigation .nav-item').each(function() {
        var $this = $(this);
        if (current === "") {
            if ($this.find(".nav-link").attr('href').indexOf("index.html") !== -1) {
                $(this).find(".nav-link").parents('.nav-item').last().addClass('active');
                $(this).addClass("active");
            }
        } else {
            if ($this.find(".nav-link").attr('href').indexOf(current) !== -1) {
                $(this).find(".nav-link").parents('.nav-item').last().addClass('active');
                $(this).addClass("active");
            }
        }
    });

    $(".rt_nav_header.horizontal-layout .nav_wrapper_main .navbar-toggler").on("click", function() {
        $(".rt_nav_header.horizontal-layout .nav-bottom").toggleClass("header-toggled");
    });

    var navItemClicked = $('.page-navigation >.nav-item');
    navItemClicked.on("click", function(event) {
        if (window.matchMedia('(max-width: 991px)').matches) {
            if (!($(this).hasClass('show-submenu'))) {
                navItemClicked.removeClass('show-submenu');
            }
            $(this).toggleClass('show-submenu');
        }
    });

    $(window).scroll(function() {
        if (window.matchMedia('(min-width: 992px)').matches) {
            var header = '.rt_nav_header.horizontal-layout';
            if ($(window).scrollTop() >= 50) {
                $(header).addClass('fixed-on-scroll');
            } else {
                $(header).removeClass('fixed-on-scroll');
            }
        }
    });

    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop(),
            mainHeader = $('#sticky-header'),
            mainHeaderHeight = mainHeader.innerHeight();

        if (scroll > 1) {
            $("#sticky-header").addClass("sticky-menu");
        } else {
            $("#sticky-header").removeClass("sticky-menu");
        }
    });

    var e = function() {
        var e = (window.innerHeight > 0 ? window.innerHeight : this.screen.height) - 5;
        (e -= 67) < 1 && (e = 1), e > 67 && $(".main-content").css("min-height", e + "px")
    };

    $(window).ready(e), $(window).on("resize", e);

    //$('[data-toggle="popover"]').popover();

    $('form').each(function() {
        $(this).validate();
    });

    $('input.date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true //, startDate: '0d'
    });

    if ($('textarea.editor').length) {
        var _height = $(this).data('height') || 200;

        $('textarea.editor').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                //['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize'],
                ['color'],
                ['para', ['ul', 'ol', 'paragraph']],
                //['height', ['height']]
            ],
            height: _height
        });
    }

    $('select[name="rental_type"]').change(function() {
        var _val = $(this).val();

        $('.price_slab, .hour_duration').addClass('d-none');

        if (_val == 'hourly') {
            $('.price-hourly, .hour_duration').removeClass('d-none');
        }

        if (_val == 'daily') {
            $('.price-daily').removeClass('d-none');
        }
    });


    $('select[name=availability]').change(function() {
        var _val = $(this).val();

        if (_val == 1) {
            $('#availability_dates').removeClass('d-none');
        } else {
            $('#availability_dates').addClass('d-none');
            $('#availability_dates input').val('');
        }
    });

    $('a.deleted').click(function(e) {
        e.preventDefault();

        if (confirm('Are you sure want t delete?')) {
            window.location.href = $(this).attr('href');
        }
    });

    $('input[name=username]').blur(function() {
        var _elm = $(this);
        var _parent = $(this).closest('div');

        var _val = $.trim(_elm.val());
        if (!_val) return;

        var _id = $('input[name=id]').val();

        //showLoader();

        $('small.error', _parent).addClass('d-none');

        $.ajax({
            type: 'GET',
            url: '/ajax/unique',
            data: 'type=username&value=' + _val + '&id=' + _id,
            success: function(response) {
                if (response == 0) {
                    $('small.error', _parent).removeClass('d-none');
                }
            },
            complete: function() {
                //hideLoader();
            },
            error: function(errorThrown) {
                alert('Something went wrong. Please try again');
            }
        });
    });

    $('input[name=email]').blur(function() {
        var _elm = $(this);
        var _parent = $(this).closest('div');

        var _val = $.trim(_elm.val());
        if (!_val) return;

        var _id = $('input[name=id]').val();

        //showLoader();

        $('small.error', _parent).addClass('d-none');

        $.ajax({
            type: 'GET',
            url: '/ajax/unique',
            data: 'type=email&value=' + _val + '&id=' + _id,
            success: function(response) {
                if (response == 0) {
                    $('small.error', _parent).removeClass('d-none');
                }
            },
            complete: function() {
                //hideLoader();
            },
            error: function(errorThrown) {
                alert('Something went wrong. Please try again');
            }
        });
    });

});