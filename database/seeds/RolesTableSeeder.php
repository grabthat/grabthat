<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' 		=> 'Admin',
                'description' => 'Super admin',
            ],
            [
                'name' 		=> 'Guest',
                'description' => 'Registered Users',
            ],
        ]; 
        DB::table('roles')->insert($roles); 
    }
}
