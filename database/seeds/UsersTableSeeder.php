<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username' 		=> 'Super Admin',
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'email' 	=>'superadmin@localhost.com',
                'password' 	=>Hash::make('admin@123'),
                'group' => 99999,
                'verified' => 1
               
            ],
            [
                'username' 		=> 'Admin localhost',
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'email' 	=>'admin@localhost.com',
                'password' 	=>Hash::make('admin@123'),
                'group' => 99999,
                'verified' => 1
            ],
        ]; 
        DB::table('users')->insert($users); 
    }
}
