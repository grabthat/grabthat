<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $done = $this->call(UsersTableSeeder::class);
        // if($done){
        //     $success =  $this->call(RolesTableSeeder::class);
        //     if($success){
        //         $this->call(UsersRolesTableSeeder::class);
        //     }
        // }
    }
}
