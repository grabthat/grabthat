<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('username')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();

            $table->integer('group')->nullable();


            $table->string('phone')->nullable();
            $table->date('dob')->nullable();
            
            
            $table->tinyInteger('gender')->default('1');
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('zip')->nullable();

            $table->tinyInteger('company_role')->default('1');
            $table->string('company')->nullable();
            $table->string('designation')->nullable();
            $table->string('image')->nullable();
            $table->string('display_name')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            
            $table->tinyInteger('verified')->default('0');
            $table->enum('status',['Active', 'Inactive', 'Deleted', 'Freezed'])->default('Inactive');
            $table->string('activation_link')->nullable();
            $table->timestamp('password_reset_token')->nullable();
            $table->string('password_reset_link')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
        DB::update("ALTER TABLE users AUTO_INCREMENT = 9001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
