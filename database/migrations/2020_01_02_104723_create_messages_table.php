<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('from')->nullable();
            $table->unsignedBigInteger('to')->nullable();
            $table->text('text');
            $table->enum('status', ['seen', 'unseen'])->default('unseen');
            $table->bigInteger('seen_at')->nullable();
            $table->timestamps();
            $table->foreign('from')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('to')->references('id')->on('users')->onDelete('SET NULL');
        });
        DB::update("ALTER TABLE messages AUTO_INCREMENT = 100001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
