<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_id')->nullable();
            $table->string('booked_duration')->nullable();
            $table->unsignedBigInteger('rental_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('seller_id')->nullable();
            $table->string('rental_name')->nullable();
            $table->bigInteger('start_date')->nullable();
            $table->bigInteger('end_date')->nullable();
            $table->bigInteger('from_date')->nullable();
            $table->bigInteger('to_date')->nullable();
            $table->enum('refund_request', ['yes', 'no'])->default('no');
            $table->enum('refunded', ['yes', 'no', 'pending'])->default('no');
            $table->bigInteger('refunded_at')->nullable();
            $table->double('total_amount', 15,2)->nullable();
            $table->double('price', 15, 2)->nullable();
            $table->float('tax_percent', 3,2)->nullable();
            $table->double('tax_amount', 15,2)->nullable();
            $table->float('service_fee_percent', 3,2)->nullable();
            $table->double('fee_amount', 15,2)->nullable();
            $table->string('currency_type')->nullable();
            $table->enum('amount_transfered', ['yes', 'no'])->default('no');

            $table->enum('rental_type', ['hourly', 'daily'])->default('hourly');
            $table->enum('rental_status', ['Pending', 'Confirmed','Canceled', 'Rejected' , 'Cancelling', 'Delivered'])->default('Pending');
            $table->enum('booking_status', ['Active', 'Deleted'])->default('Active');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('seller_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('rental_id')->references('id')->on('rentals')->onDelete('SET NULL');
        });
        DB::update("ALTER TABLE bookings AUTO_INCREMENT = 100001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
