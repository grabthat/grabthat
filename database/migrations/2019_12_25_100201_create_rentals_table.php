<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->unique();
            $table->unsignedBigInteger('category')->nullable();
            $table->integer('duration')->nullable();
            $table->enum('rental_type', ['daily', 'hourly'])->default('daily');
            // $table->tinyInteger('hours', 4)->nullable();
            $table->double('price', 8, 2)->nullable();

            $table->longText('description')->nullable();
            $table->longText('including')->nullable();
            $table->longText('excluding')->nullable();

            $table->tinyInteger('confirmation')->nullable();
            $table->tinyInteger('availability')->nullable();
            
            $table->bigInteger('available_start_date')->nullable();
            $table->bigInteger('available_end_date')->nullable();
            // $table->time('start_hour')->nullable();
            // $table->time('end_hour')->nullable();


            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('zip')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('rental_tips')->nullable();

            $table->longText('guest_requirements')->nullable();
            $table->longText('cancellation_policy')->nullable();
            $table->string('image')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->string('meta_description')->nullable();
          
            $table->tinyInteger('draft')->default('0');
            $table->tinyInteger('status')->default('0');
            
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('category')->references('id')->on('categories')->onDelete('SET NULL');
        });
        DB::update("ALTER TABLE rentals AUTO_INCREMENT = 100001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals'); 
    }
}
