<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawns', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->unsignedBigInteger('user_id')->nullable();
            $table->double('amount', 15,2)->nullable();

            $table->string('cashout_id')->nullable();
            $table->string('balance_transaction')->nullable();
            $table->string('stripe_bank_id')->nullable();
            $table->string('method')->nullable();
            $table->string('source_type')->nullable();
            $table->string('currency')->nullable();
            $table->string('type')->nullable();
            
            
            
            $table->bigInteger('arrival_date')->nullable();
            $table->bigInteger('requested_date')->nullable();
            $table->bigInteger('completed_date')->nullable();
            $table->string('withdraw_status')->nullable('pending');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawns');
    }
}
