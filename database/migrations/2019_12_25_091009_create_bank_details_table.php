<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('seller_id')->nullable();
            $table->string('social_id_number')->nullable();
            $table->string('business_type')->default('individual');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->mediumText('address_line_1')->nullable();
            $table->mediumText('address_line_2')->nullable();
            $table->string('postal_code')->nullable();


            // image file 
            $table->string('id_front')->nullable();
            $table->string('id_back')->nullable();

            // $table->string('account_no')->nullable();
            $table->string('currency_type')->nullable();
            // $table->string('country')->nullable();

            $table->string('mcc')->nullable();
            $table->string('support_email')->nullable();
            $table->string('support_phone')->nullable();
            $table->string('support_url')->nullable();
            $table->string('url')->nullable();


            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_holder_name')->nullable();
            $table->string('routing_number')->nullable();
            $table->string('country_of_bank')->nullable();
            $table->string('currency_of_bank')->nullable();
            $table->string('swift_code')->nullable();



            $table->enum('agreement' , ['yes', 'no'])->default('no');

            $table->string('stripe_account')->nullable();
            $table->string('stripe_bank_account')->nullable();
            $table->string('stripe_customer_ref')->nullable();

            $table->enum('bank_status', ['verified', 'non-verified'])->default('non-verified');
            $table->timestamps();
            $table->foreign('seller_id')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_details');
    }
}
