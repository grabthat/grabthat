<?php

Route::post('home', 'MyaccountController@index');
// Route::get('home', 'MyaccountController@index');
Route::get('/seller/{usernae}', 'MyaccountController@profile')->name('sellerDetail');
// Route::match(['get', 'post'], 'user/{username}', );
Route::match(['get', 'post'], 'rental', 'RentalController@index');

Route::match(['get', 'post'], 'rental/{slug}', 'RentalController@rental')->name('rentalDetail');
// Route::get('/rental/booking/{slug}', 'RentalController@booking')->name('bookNow');
Route::match(['get', 'post'], 'rental/booking/{slug}', 'RentalController@booking')->name('bookNow');

Route::match(['get', 'post'], 'search', 'RentalController@search');


Route::match(['get', 'post'], 'category/{slug}', 'CategoryController@view');


Route::get('terms-condition', 'HomeController@frontTermsCondition')->name('frontTermsCondition');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('FrontPrivacyPolicy');
Route::get('/check-availability', 'RentalController@checkavailability')->name('checkavailability');

Route::match(['get', 'post'], 'ajax/unique', 'AjaxController@unique');
Route::post('ajax/set/nearby', 'AjaxController@updatelocation');