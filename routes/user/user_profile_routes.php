<?php

// Route::match(['get', 'post'], 'profile', 'MyaccountController@profile');
Route::get('/profile', 'MyaccountController@FrontUserProfile')->name('FrontUserProfile');
Route::get('/edit-profile/', 'MyaccountController@editProfile')->name('editProfile');
Route::post('/edit-profile/', 'MyaccountController@updateProfile');
Route::group(['prefix' => 'ajax'], function(){
    Route::post('/update-personal-detail', 'BankDetailController@updatePersonalDetail')->name('updatePersonalDetail');
    Route::post('/update-address-detail', 'BankDetailController@updateAddressDetail')->name('updateAddressDetail');
    Route::post('/upload-verification-document', 'BankDetailController@uploadIdproof')->name('uploadIdproof');
    Route::post('/update-business-detail/', 'BankDetailController@BusinessProfile')->name('updateBusindessDetail');
    Route::post('/update-bank-detail/', 'BankDetailController@updateBankDetail')->name('updateBankDetail');
    Route::post('/submit-all-detail-with-agreement', 'BankDetailController@submitAllBankInfo')->name('submitAllBankInfo');
});
// Route::group(['prefix' => 'profile'], function(){
//     // Route::post('/', 'MyaccountController@UpdateFrontUserProfile') ;
//     // Route::match(['get', 'post'], 'edit-profile', 'MyaccountController@editprofile');
// });