<?php

Route::get('/checkout', 'CheckoutController@index');
Route::post('/checkout', 'CheckoutController@checkout')->name('checkout');
Route::match(['get', 'post'], 'payment', 'PaymentController@index');
Route::post('/payment', 'PaymentController@index')->name('payNow');