<?php
// Route::match(['get', 'post'], 'my-payments', 'MyaccountController@mypayments');
Route::get('/my-payments', 'MyaccountController@mypayments')->name('UserMypayment');
// Route::match(['get', 'post'], 'withdraw/funds', 'MyaccountController@myWithdrawFunds');
Route::get('/withdraw/funds', 'MyaccountController@myWithdrawFunds')->name('makeWithdrawl');
Route::POST('/withdraw-cash/', 'BankDetailController@withdrawcash')->name('withdrawcash');

Route::get('/bank-details', 'MyaccountController@myBankDetails')->name('myBankDetail');
Route::post('/update-bank-detail', 'BankDetailController@updateBankDetail')->name('updateBankDetail');
// Route::match(['get', 'post'], 'bank-details', 'MyaccountController@myBankDetails');