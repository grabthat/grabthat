<?php
Route::get('/my-rentals', 'MyaccountController@listMyAllRentals')->name('FrontUserRentals');
// Route::match(['get', 'post'], 'my-rentals', 'MyaccountController@myrentals');
Route::match(['get', 'post'], 'add-rental', 'MyaccountController@addrental');
Route::get('/add-rental', 'MyaccountController@addrental')->name('frontAddRental');
Route::post('/add-rental', 'MyaccountController@UpdateRental');
Route::get('rental/review/{slug}', 'RentalController@review')->name('writeReview');
Route::post('rental/review/{slug}', 'RentalController@review');

Route::post('/cancel-ordered-rental', 'BookingController@cancelRentalOrder')->name('cancelRentalOrder');
Route::post('/remove-rental/', 'BookingController@removeRental')->name('removeRental');
Route::get('/view-all-booking/{slug}', 'BookingController@listBooking')->name('listBooking');
Route::post('/confirm-rental', 'BookingController@confirmRental')->name('confirmRental');
Route::post('/delivered-rental', 'BookingController@DeliveredRental')->name('DeliveredRental');
Route::post('/cancel-booking-order', 'BookingController@CancelCustomerBooking')->name('CancelCustomerBooking');
Route::get('/transaction-detail-of-buyer', 'BookingController@transactionDetail')->name('transactionDetail');
Route::get('/my-rentals/{id}', 'MyaccountController@myRentalsDetail')->name('myRentalsDetail');
Route::get('/message', 'MyaccountController@mymessage')->name('mymessage');
Route::get('/get-buyer-detail', 'BookingController@buyerdetail')->name('buyerdetail');

