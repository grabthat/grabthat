<?php
Route::group(['prefix'=> 'rentals'], function(){
    Route::get('/', 'AdmincpController@allRentalList')->name('allRentalList');
    Route::get('/search', 'AdmincpController@AdminrentalSearch')->name('AdminrentalSearch');
    // Route::get('/', 'AdmincpController@rentals');
    // Route::match(['get', 'post'], 'admincp/rentals', 'AdmincpController@rentals');
});
Route::group(['prefix'=> 'rental'], function(){
    Route::get('/', 'AdmincpController@addrental')->name('adminAddRental');
    Route::post('/', 'AdmincpController@updateRental');
    Route::get('/edit/{id}', 'AdmincpController@editRental')->name('AdminEditRental');
    Route::post('/edit/{id}', 'AdmincpController@updateRental');
    Route::get('/remove-rental/{id}', 'AdmincpController@AdminremoveRental')->name('AdminremoveRental');
    // Route::post('/', 'AdmincpController@rental')->name('UpdateadminAddRental');
    // Route::match(['get', 'post'], 'admincp/rental', 'AdmincpController@rental');
});
