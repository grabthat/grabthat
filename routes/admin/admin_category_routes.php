<?php

Route::group(['prefix' => 'categories'], function () {
    Route::get('/', 'CategoryController@allCategoryList')->name('allCategoryList');
    Route::get('/search', 'CategoryController@CategorySearch')->name('CategorySearch');
});
Route::group(['prefix' => 'category'], function () {
    Route::get('/add-category', 'CategoryController@addCategory')->name('addCategory');
    Route::post('/add-category', 'CategoryController@StoreCategory')->name('StoreCategory');
    Route::get('/edit-category/{id}', 'CategoryController@editCategory')->name('editCategory');
    Route::post('/edit-category/{id}', 'CategoryController@StoreCategory');
    Route::get('/remove-category/{id}', 'CategoryController@removeCategory')->name('removeCategory');
    // Route::get('/add-category', 'AdmincpController@category')->name('addCategory');
    // Route::match(['get', 'post'], 'admincp/category', 'AdmincpController@category');
    // Route::match(['get', 'post'], 'admincp/categories', 'AdmincpController@categories');
});
