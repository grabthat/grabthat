<?php
Route::get('/my-profile', 'AdmincpController@profile')->name('adminProfile');
Route::post('/my-profile', 'AdmincpController@updateProfile')->name('updateProfile');
