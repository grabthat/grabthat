<?php

Route::group(['prefix' => 'users'], function () {
    Route::get('all-users-list', 'AdmincpController@alluserlist')->name('alluserlist');
    Route::get('all-user/search', 'AdmincpController@searchUser')->name('searchUser');
    Route::get('remove-user/{id}', 'AdmincpController@RemoveUser')->name('RemoveUser');

    // Route::match(['get', 'post'], 'admincp/users', 'AdmincpController@users');
    // Route::match(['get', 'post'], 'admincp/user', 'AdmincpController@user');
    Route::get('/add-user', 'AdmincpController@addNewUser')->name('addNewUser');
    Route::post('/add-user', 'AdmincpController@storeUser');
    Route::get('/edit/{id}', 'AdmincpController@editUser')->name('editUser');
    Route::post('/edit/{id}', 'AdmincpController@storeUser');
    Route::get('/profile/{id}', 'AdmincpController@UserProfie')->name('UserProfie');
});
