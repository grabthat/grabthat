<?php
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');