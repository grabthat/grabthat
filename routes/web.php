<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once('reset_routes.php');

// Route::match(['get', 'post'], 'redirect', 'HomeController@redirect');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// dd(Auth::user() );
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'AdmincpController@index')->name('admin');
    include_once('admin/admin_profile_routes.php');
    include_once('admin/admin_users_routes.php');
    include_once('admin/admin_category_routes.php');
    include_once('admin/admin_setting_routes.php');
    include_once('admin/admin_rental_routes.php');
    include_once('admin/pages_routes.php');
});
// routes for logged in users 
Route::get('/', 'FrontEndController@homepage')->name('homepage');
Route::group(['prefix' => 'user', 'middleware' => ['auth', 'customer']], function () {
    Route::get('/', 'FrontEndController@customer')->name('customer');
     
    include_once('user/user_profile_routes.php');
    include_once('user/user_banking_routes.php');
    include_once('user/user_rental_routes.php'); 
    include_once('user/payment_routes.php'); 
});

include_once('front_routes.php');
include_once('auth_routes.php');
// Route::get('/delete-stripe/{stripe_account}', 'BankDetailController@deleteAccount')->name('deleteAccount');
// Route::get('/delete-bank/{stripe_account}/{bank}', 'BankDetailController@deleteBank')->name('deleteBank');
 
 



